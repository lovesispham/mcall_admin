// import './App.css';
import { Navigate, Route, Routes } from 'react-router-dom';
import ProtectedRoutes from './router/protectedRoutes';

import Admin from './layout/admin/admin';
import Auth from './page/login/auth';
import { router, user } from './router/constants';
import Layout from './layout/admin/layout';
import ProtectedAuth from './router/protecdAuth';
import { useDispatch, useSelector } from 'react-redux';
import { Fragment, useEffect, useState } from 'react';
import NotFound from './page/admin/notFound/NotFound';
import ErrServer from './page/admin/notFound/errServer';
import Lock from './page/admin/notFound/Lock';
import { selectAuthActive } from './features/auth/authSlice';
import { getProfile, selectProfile } from './features/user/userSlice';
import MainRoutes from './Routes';


function App() {
  const dispatch = useDispatch();

  const [module, setModule] = useState([]);
  const activeAuth = useSelector(selectAuthActive);
  const token = localStorage.getItem('access_token');
  const profile1 = useSelector(selectProfile);
  const [profile, setProfile] = useState(useSelector(selectProfile));

  useEffect(() => {
    setProfile(profile1);
  }, [profile1]);
  useEffect(() => {
    token && CallApi();
  }, [token, activeAuth]);

  const CallApi = async () => {
    // await dispatch(getNotify());
    await dispatch(getProfile())
      .unwrap()
      .then((res) => {
        setProfile(res?.data?.user_obj);
      }, []);

  };

  return (
    <>
    
      <MainRoutes/>
    </>
  );
}

export default App;
