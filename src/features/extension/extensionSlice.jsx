import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosClient from "../../api/axiosClient";

const initialState = {
  loading: false,
};

const ACTION = {
  GET_EXTENSION: "extension/getExtension",
  
POST_EXTENSION: "extension/postExtension",

};

export const getExtension = createAsyncThunk(ACTION.GET_EXTENSION, async (body) => {
  return axiosClient.get("/pbx/extension/", {params:body});
});

export const postExtension = createAsyncThunk(ACTION.POST_EXTENSION, async (body) => {
    return axiosClient.post("/pbx/extension/", body);
  });



const extensionSlice = createSlice({
  name: "extension",
  initialState: initialState,

  extraReducers: (builder) => {
    builder
    .addCase(getExtension.pending, (state) => {
      state.loading = true;
    })
    .addCase(getExtension.rejected, (state) => {
      state.loading = false;
    })
    .addCase(getExtension.fulfilled, (state, action) => {
      state.loading = false;
      
      state.extensionList = action.payload.data;
    })
  },
});

// Reducer
const { reducer: extensionReducer } = extensionSlice;
export default extensionReducer;
