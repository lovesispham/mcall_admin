import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axiosClient from '../../api/axiosClient';

const initialState = {
  loading: false,
  profile: {},
  userList: {},
  listUser: [],
};

const ACTION = {
  GET_PROFILE: 'user/getProfile',
  EDIT_PASSWORD: 'user/editPassword',

  EDIT_USER: 'user/editUser',

  GET_USER: 'user/getUser',

  GET_USER_DASHBOARD: 'user/getUserDashboard',
  CREATE_USER_DASHBOARD: 'user/createUserDashboard',
  GET_USER_DASHBOARD_CODE: 'user/getUserDashboardCode',
  EDIT_USER_DASHBOARD_CODE: 'user/editUserDashboardCode',
};

export const getUserDashboardCode = createAsyncThunk(ACTION.GET_USER_DASHBOARD_CODE, async (body) => {
  return axiosClient.get(`/dashboard/users/${body.code}/`);
});

export const editUserDashboardCode = createAsyncThunk(ACTION.EDIT_USER_DASHBOARD_CODE, async ( {code, body, rejectWithValue }) => {
  
    try {
      const response = await axiosClient.put(`/dashboard/users/${code}/`,body);

      return response.data;
    } catch (error) {
      return rejectWithValue(error.response);
    }
  
});


export const getUserDashboard = createAsyncThunk(ACTION.GET_USER_DASHBOARD, async (body) => {
  return axiosClient.get('/dashboard/users/', {
    params: body,
  });
});

export const createUserDashboard = createAsyncThunk(ACTION.CREATE_USER_DASHBOARD, async (body, { rejectWithValue }) => {
  try {
    const response = await axiosClient.post('/dashboard/users/', body);

    return response.data;
  } catch (error) {
    return rejectWithValue(error.response);
  }
});

export const getProfile = createAsyncThunk(ACTION.GET_PROFILE, async (body) => {
  return axiosClient.get('/users/profile/', {
    params: body,
  });
});

export const editPassword = createAsyncThunk(ACTION.EDIT_PASSWORD, async (body, { rejectWithValue }) => {
  try {
    const response = await axiosClient.post('/users/change-password/?', body);

    return response.data;
  } catch (error) {
    return rejectWithValue(error.response);
  }
});

export const getUser = createAsyncThunk(ACTION.GET_USER, async (body) => {
  return axiosClient.get('/auth/users/', {
    params: body,
  });
});

export const editUser = createAsyncThunk(ACTION.EDIT_USER, async ({ id, body }) => {
  return axiosClient.put(`/users/${id}/`, body);
});

const userSlice = createSlice({
  name: 'user',
  initialState: initialState,
  reducers: {
    getUserList: (state, action) => {
      state.listUser = action.payload.map((e) => ({
        idPhone: e?.id,

        label: e?.extension,
      }));
    },
    resetProfile: (state) => {
      state.profile = null;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getProfile.pending, (state) => {
        state.loading = true;
      })
      .addCase(getProfile.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getProfile.fulfilled, (state, action) => {
        state.loading = false;
        state.success = false;
        state.profile = action.payload?.data?.user_obj;
      })
      .addCase(editUser.pending, (state) => {})
      .addCase(editUser.rejected, (state) => {
        state.success = false;
      })
      .addCase(editUser.fulfilled, (state, action) => {
        state.success = true;
      })
      .addCase(getUser.pending, (state) => {
        state.loading = true;
      })
      .addCase(getUser.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getUser.fulfilled, (state, action) => {
        state.userList = action.payload.data;
        state.loading = false;
      })
      .addCase(getUserDashboard.pending, (state) => {
        state.loading = true;
      })
      .addCase(getUserDashboard.rejected, (state) => {
        state.loading = false;
        state.userList = null
      })
      .addCase(getUserDashboard.fulfilled, (state, action) => {
        state.loading = false;
        state.success = false;
        state.userList = action.payload.data
      })
      .addCase(getUserDashboardCode.pending, (state) => {
        state.loading = true;
      })
      .addCase(getUserDashboardCode.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getUserDashboardCode.fulfilled, (state, action) => {
        state.userDetail = action.payload.data;
        state.success = false;
        state.loading = false;
      })
      .addCase(editUserDashboardCode.pending, (state) => {
      })
      .addCase(editUserDashboardCode.rejected, (state) => {
        state.success = false;
      })
      .addCase(editUserDashboardCode.fulfilled, (state, action) => {
        state.success = true
      })
      .addCase(createUserDashboard.pending, (state) => {
      })
      .addCase(createUserDashboard.rejected, (state) => {
        state.success = false;
      })
      .addCase(createUserDashboard.fulfilled, (state, action) => {
        state.success = true
      });;
  },
});

// Selector
export const { getUserList, resetProfile } = userSlice.actions;

export const selectProfile = (state) => state.user.profile;
export const selectUser = (state) => state.user.userList;
export const selectMessage = (state) => state.user.success;
export const profileLoadding = (state) => state.user.loading;
export const selectListUser = (state) => state.user.listUser;

// Reducer
const { reducer: userReducer } = userSlice;
export default userReducer;
