import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosClient from "../../api/axiosClient";

const initialState = {
  loading: false,
};

const ACTION = {
  GET_DEPARTMENT:'org/getDepartment',
  GET_POSITION:'org/getPosition',

  GET_ORG: "org/getOrgApi",
  CREATE_ORG_ADMIN: "org/postorgAdminApi",
  EDIT_ORG_ADMIN: "org/putOrgapi",

  CREATE_ORG: "org/postOrgApi",

  GET_ORG_DASHBOARD: "org/getOrgDashboard",
  CREATE_ORG_DASHBOARD: "org/createOrgDashboard",

  GET_ORG_DASHBOARD_CODE: "org/getOrgDashboardCode",
  EDIT_ORG_DASHBOARD_CODE: "org/editOrgDashboardCode",

  ORG_ASYNC: "org/orgAsync"
};

export const getDepartment = createAsyncThunk(ACTION.GET_DEPARTMENT, async (body) => {
  
  return axiosClient.get("/org/departments/", {params:body});

 
});

export const getPosition = createAsyncThunk(ACTION.GET_DEPARTMENT, async (body) => {
  
  return axiosClient.get(`/org/departments/${body.id}/`);

 
});


export const getOrgDashboard = createAsyncThunk(ACTION.GET_ORG_DASHBOARD, async (body) => {
  
    return axiosClient.get("/dashboard/org/", {params:body});

   
});

export const createOrgDashboard = createAsyncThunk(
  ACTION.CREATE_ORG_DASHBOARD,
  async (body, { rejectWithValue }) => {
    try {
      const response = await axiosClient.post(`/dashboard/org/`, body);

      return response.data;
    } catch (error) {
      console.log(error)
      return rejectWithValue(error.response);
    }
  }
);

export const getOrgDashboardCode = createAsyncThunk(ACTION.GET_ORG_DASHBOARD_CODE, async (body) => {
  
  return axiosClient.get(`/dashboard/org/${body.code}/`);

 
});


export const editOrgDashboardCode = createAsyncThunk(
  ACTION.EDIT_ORG_DASHBOARD_CODE,
  async ( {code, body, rejectWithValue }) => {
    try {
      const response = await axiosClient.put(`/dashboard/org/${code}/`,body);

      return response;
    } catch (error) {
      console.log(error)
      return rejectWithValue(error.response);
    }
  }
);

export const postorgAdminApi = createAsyncThunk(
  ACTION.CREATE_ORG_ADMIN,
  async (body, { rejectWithValue }) => {
    try {
      const response = await axiosClient.post(`/org/${body.id}/`, body);

      return response.data;
    } catch (error) {
      console.log(error)
      return rejectWithValue(error.response);
    }
  }
);

export const postOrgApi = createAsyncThunk(
  ACTION.CREATE_ORG,
  async (body, { rejectWithValue }) => {
    try {
      const response = await axiosClient.post(`/org/`, body);

      return response.data;
    } catch (error) {
      return rejectWithValue(error.response);
    }
  }
);

export const putOrgapi = createAsyncThunk(
  ACTION.EDIT_ORG_ADMIN,
  async (body, { rejectWithValue }) => {
    try {
      const response = await axiosClient.put(`/org/${body.id}/`, body);

      return response.data;
    } catch (error) {
      return rejectWithValue(error.response);
    }
  }
);

export const getOrgApi = createAsyncThunk(ACTION.GET_ORG, async (body) => {
  try {
    const response = await axiosClient.get("/org/", body);

    return response.data;
  } catch (error) {
    return rejectWithValue(error.response);
  }
});


export const orgAsync = createAsyncThunk(
  ACTION.ORG_ASYNC,
  async (body, { rejectWithValue }) => {
    try {
      const response = await axiosClient.post('/dashboard/org-async/', body);

      return response.data;
    } catch (error) {
      return rejectWithValue(error.response);
    }
  }
);

const orgSlice = createSlice({
  name: "org",
  initialState: initialState,

  extraReducers: (builder) => {
    builder
    .addCase(getOrgDashboard.pending, (state) => {
      state.loading = true;
    })
    .addCase(getOrgDashboard.rejected, (state) => {
      state.loading = false;
    })
    .addCase(getOrgDashboard.fulfilled, (state, action) => {
      state.loading = false;
      state.success = false;
      state.orgAdminList = action.payload.data;
    })
    .addCase(editOrgDashboardCode.pending, (state) => {
    })
    .addCase(editOrgDashboardCode.rejected, (state) => {
      state.success = false;
    })
    .addCase(editOrgDashboardCode.fulfilled, (state, action) => {
      state.success = true;
    })
    .addCase(createOrgDashboard.pending, (state) => {})
      .addCase(createOrgDashboard.rejected, (state) => {
        state.success = false;
      })
      .addCase(createOrgDashboard.fulfilled, (state, action) => {
        state.success = true;
      })

      .addCase(getDepartment.pending, (state) => {
        state.loading = true;
      })
      .addCase(getDepartment.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getDepartment.fulfilled, (state, action) => {
        state.loading = false;
        state.departmentList = action.payload.data;
      })
      .addCase(getOrgDashboardCode.pending, (state) => {
        state.loading = true;
      })
      .addCase(getOrgDashboardCode.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getOrgDashboardCode.fulfilled, (state, action) => {
        state.loading = false;
        state.success = false;
        state.orgDetail = action.payload.data;
      })
  },
});

// Reducer
const { reducer: orgReducer } = orgSlice;
export default orgReducer;
