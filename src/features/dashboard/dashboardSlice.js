import { createSlice } from '@reduxjs/toolkit';
import {
  getAllDashboardApi,
  getOrderReportApi,
  getPaymentReportThunk,
  getStatusUser,
  getSummary,
  getTopProductApi,
} from './dashboardThunk';

const initialState = {
  loading: false,
};

const dashboardSlice = createSlice({
  name: 'dashboard',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getAllDashboardApi.pending, (state) => {
        state.loading = false;
      })
      .addCase(getAllDashboardApi.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getAllDashboardApi.fulfilled, (state, action) => {
        state.loading = false;
      })
      .addCase(getTopProductApi.pending, (state) => {
        state.loading = false;
      })
      .addCase(getTopProductApi.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getTopProductApi.fulfilled, (state, action) => {
        state.loading = false;
      })
      .addCase(getOrderReportApi.pending, (state) => {
        state.loading = false;
      })
      .addCase(getOrderReportApi.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getOrderReportApi.fulfilled, (state, action) => {
        state.loading = false;
      })
      .addCase(getSummary.pending, (state) => {
        state.loading = true;
      })
      .addCase(getSummary.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getSummary.fulfilled, (state, action) => {
        state.loading = false;
      })
      .addCase(getPaymentReportThunk.pending, (state) => {
        state.loading = false;
      })
      .addCase(getPaymentReportThunk.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getPaymentReportThunk.fulfilled, (state, action) => {
        state.loading = false;
      });
    // .addCase(getStatusUser.pending, (state) => {
    //   state.loading = false;
    // })
    // .addCase(getStatusUser.rejected, (state) => {
    //   state.loading = false;
    // })
    // .addCase(getStatusUser.fulfilled, (state, action) => {
    //   state.products = action.payload.data;
    //   state.loading = false;
    // });
  },
});

// Actions
export const dashboardActions = dashboardSlice.actions;

// Selector
export const selectAccessToken = (state) => state.dashboard.accessToken;
export const selectDashboardloading = (state) => state.dashboard.loading;

// Reducer
const dashboardReducer = dashboardSlice.reducer;
export default dashboardReducer;
