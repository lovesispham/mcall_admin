import { createAsyncThunk } from '@reduxjs/toolkit';

import DashboardApi from '../../api/dashboardApi';

export const getAllDashboardApi = createAsyncThunk('dashboard', async (payload, { rejectWithValue }) => {
  const controller = new AbortController();
  try {
    const response = await DashboardApi.getAllDashboards(payload);
    // localStorage.setItem('access_token', response.data.access);

    return response.data;
  } catch (error) {
    return rejectWithValue(error.response);
  }
});

export const getOrgAndAgent = createAsyncThunk('OrgAndAgent', async (payload, { rejectWithValue }) => {
  try {
    const response = await DashboardApi.getOrgAgent(payload);
    // localStorage.setItem('access_token', response.data.access);

    return response.data;
  } catch (error) {
    return rejectWithValue(error.response);
  }
});
export const getTopProductApi = createAsyncThunk('TopProduct', async (payload, { rejectWithValue }) => {
  try {
    const response = await DashboardApi.getTopproduct(payload);
    // localStorage.setItem('access_token', response.data.access);

    return response.data;
  } catch (error) {
    return rejectWithValue(error.response);
  }
});

export const getOrderReportApi = createAsyncThunk('OrderReport', async (payload, { rejectWithValue }) => {
  try {
    const response = await DashboardApi.getOrderReport(payload);
    // localStorage.setItem('access_token', response.data.access);

    return response.data;
  } catch (error) {
    return rejectWithValue(error.response);
  }
});

export const getPayment = createAsyncThunk('Payment', async (payload, { rejectWithValue }) => {
  try {
    const response = await DashboardApi.getPayment(payload);
    // localStorage.setItem('access_token', response.data.access);

    return response.data;
  } catch (error) {
    return rejectWithValue(error.response);
  }
});

export const getPaymentReportThunk = createAsyncThunk('getPaymentReport', async (payload, { rejectWithValue }) => {
  try {
    const response = await DashboardApi.getPaymentReport(payload);
    // localStorage.setItem('access_token', response.data.access);

    return response.data;
  } catch (error) {
    return rejectWithValue(error.response);
  }
});

export const getSummary = createAsyncThunk('getSummary', async (payload, { rejectWithValue }) => {
  try {
    const response = await DashboardApi.getSummary(payload);
    // localStorage.setItem('access_token', response.data.access);

    return response.data;
  } catch (error) {
    return rejectWithValue(error.response);
  }
});

export const getStatusUser = createAsyncThunk('getStatusUser', async (payload, { rejectWithValue }) => {
  try {
    const response = await DashboardApi.getStatusUser(payload);
    // localStorage.setItem('access_token', response.data.access);

    return response.data;
  } catch (error) {
    return rejectWithValue(error.response);
  }
});
