import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosClient from "../../api/axiosClient";

const initialState = {
  loading: false,
};

const ACTION = {
  GET_MODULE: "module/getModules",
  
  GET_MODULE_DASHBOARD: "module/getModuleDashboard",
};

export const getModules = createAsyncThunk(ACTION.GET_MODULE, async (body) => {
  try {
    const response = await axiosClient.get("/modules/", body);

    return response.data;
  } catch (error) {
    return rejectWithValue(error.response);
  }
});

export const getModuleDashboard = createAsyncThunk(ACTION.GET_MODULE_DASHBOARD, async (body) => {
  try {
    const response = await axiosClient.get("/dashboard/modules/", {params:body});

    return response;
  } catch (error) {
    return rejectWithValue(error.response);
  }
});

const moduleSlice = createSlice({
  name: "module",
  initialState: initialState,

  extraReducers: (builder) => {
    builder
    .addCase(getModuleDashboard.pending, (state) => {
      state.loading = true;
    })
    .addCase(getModuleDashboard.rejected, (state) => {
      state.loading = false;
    })
    .addCase(getModuleDashboard.fulfilled, (state, action) => {
      state.loading = false;
      
      state.modulesDashboard = action.payload.data;
    })
  },
});

// Reducer
const { reducer: moduleReducer } = moduleSlice;
export default moduleReducer;
