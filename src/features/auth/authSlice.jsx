import axiosClient from '../../api/axiosClient';
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const initialState = {
    loading: false,
    accessToken: null,
    refreshToken: null,
    active: false,
  };
  

const ACTION = {
    AUTH_LOGIN:'auth/login'
}

  export const LoginApi = createAsyncThunk(ACTION.AUTH_LOGIN, async (body, { rejectWithValue }) => {
    try {
      const response = await axiosClient.post('/auth/login/',body);
      localStorage.setItem('access_token', response.data.access);
      localStorage.setItem('refresh_token', response.data.refresh);
  
      return response.data;
    } catch (error) {
      return rejectWithValue(error.response);
    }
  });

  const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
      login: (state, payload) => {
        state.active = true;
      },
    },
    extraReducers: (builder) => {
      builder
        .addCase(LoginApi.pending, (state) => {
          state.loading = true;
        })
        .addCase(LoginApi.rejected, (state) => {
          state.loading = false;
        })
        .addCase(LoginApi.fulfilled, (state, action) => {
          state.loading = false;
        });
    },
  });
  
  // Actions
  export const { login } = authSlice.actions;
  
  // Selector
  export const selectAccessToken = (state) => state.auth.accessToken;
  export const selectRefreshToken = (state) => state.auth.refreshToken;
  export const loadingLogin = (state) => state.auth.loading;
  export const selectAuthActive = (state) => state.auth.active;
  // Reducer
  const authReducer = authSlice.reducer;
  export default authReducer;