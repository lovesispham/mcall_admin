import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  toggleSidebar: false
};

const sidebarSlice = createSlice({
  name: 'sidebar',
  initialState: initialState,
  reducers: {
    toggleSidebar: (state, action) => {
      state.toggleSidebar = action.payload;
    }
  },
});

export const { toggleSidebar } = sidebarSlice.actions;

// Selector
export const isOpenSidebar = (state) => state.sidebar.toggleSidebar;


const { reducer: sidebarReducer } = sidebarSlice;
export default sidebarReducer;