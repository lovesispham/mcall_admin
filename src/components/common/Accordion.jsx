import { useState, useEffect } from "react";
import { FaAngleDown } from "react-icons/fa";
const Accordion = ({state, items }) => {
  const [activeIndex, setActiveIndex] = useState(null);

   useEffect(() => {
     if (state.success === true) {
      setActiveIndex(null)
     }
   }, [state.success]);

  return (
    <div>
      {items?.map((v, idx) => {
        return (
          <div className='accordion'>
            <h2
              className="title accordion__title"
              onClick={() => {
                setActiveIndex(activeIndex === idx ? null : idx) 
              }}
            >
              <span>{v.title}</span>
              <div className="accordion__icon">
                <FaAngleDown />
              </div>
            </h2>
            {idx === activeIndex && (
              <div className="accordion__content">{v.content}</div>
            )}
          </div>
        );
      })}
    </div>
  );
};
export default Accordion;