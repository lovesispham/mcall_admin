import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { resetState } from '../../features/alert/alert-message';
import { SuccessIcon } from '../icon/SuccessIcon';
import { CloseIcon } from '../icon/CloseIcon';
import { IoIosWarning } from 'react-icons/io';
const Toast = () => {
  const dispatch = useDispatch();
  const alert = useSelector((state) => state.alert);
  useEffect(() => {
    setTimeout(() => {
      dispatch(resetState());
    }, 5000);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [alert.showStatus]);

  const bgStatus = (type) => {
    if (alert.type === 'success') {
      return `bg-success`;
    }
    if (alert.type === 'error') {
      return `bg-error`;
    }
    if (alert.type === 'warning') {
      return `bg-warning`;
    }
  };

  return (
    <>
      {alert.showStatus && (
        <div className={`notifi custom-toast bottom-left ${bgStatus(alert.type)}`} style={{ zIndex: '5000' }}>
          <div className='notification-image'>
            {alert.type === 'success' && <SuccessIcon />}
            {alert.type === 'error' && <CloseIcon />}
            {alert.type === 'warning' && <IoIosWarning />}
          </div>
          <div>
            <p className='notification-title'>{alert.type === 'success' ? 'Thành công' : 'Lỗi'}</p>
            <p className='notification-message'>{alert.message}</p>
          </div>
        </div>
      )}
    </>
  );
};

export default Toast;
