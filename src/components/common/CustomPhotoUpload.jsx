import React, { forwardRef } from 'react';
import { FaPencilAlt } from 'react-icons/fa';

const CustomPhotoUpload = forwardRef(
  ({ className, onBlur, handleChange, error, label, type, required, ...otherProps }, ref) => {
    return (
      <div className={`form-input-group ${className}`} style={otherProps.style}>
        {label ? (
          <p>
            {label}
            <span className='text-red-400 font-bold text-lg'>{required && '*'}</span>
          </p>
        ) : null}
        <div className='photo'>
        
        <img
              src={otherProps.src}
              alt='avatar'
              onError={otherProps.onError}
            />
            <span className='edit'>
              <FaPencilAlt />
            </span>
        <input
              accept='image/png, image/jpeg'
              type='file'
              onChange={handleChange}
              {...otherProps}
              />
        </div>
      </div>
    );
  }
);

export default CustomPhotoUpload;
