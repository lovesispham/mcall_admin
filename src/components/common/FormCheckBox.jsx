import React from 'react';
import PropTypes from 'prop-types';
import { FaCheck } from 'react-icons/fa';
const FormCheckBox = (props) => {
  const inputRef = React.useRef(null);

  return (
    <label className='custom-checkbox' style={props.style}>
      {props.pos === 'left' && (
        <>
          <input disabled={props.disabled} name={props.name} type='checkbox' ref={inputRef} onChange={props.onChange} checked={props.checked} />
          <span className='custom-checkbox__checkmark'>{props.checked && <FaCheck className='bx bx-check' />}</span>
          <span>{props.label}</span>
        </>
      )}

      {props.pos === 'right' && (
        <>
          <span>{props.label}</span>
          <input disabled={props.disabled} name={props.name} type='checkbox' ref={inputRef} onChange={props.onChange} checked={props.checked} />
          <span className='custom-checkbox__checkmark'>{props.checked && <FaCheck className='bx bx-check' />}</span>
        </>
      )}
    </label>
  );
};

FormCheckBox.propTypes = {
  label: PropTypes.string.isRequired,
  checked: PropTypes.bool,
};

export default FormCheckBox;
