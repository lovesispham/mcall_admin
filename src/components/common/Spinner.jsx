import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
// import { selectLoadingClient } from '../../features/client/clientSlice';
// import { selectDashboardloading } from '../../features/dashboard/dashboardSlice';
// import { loaddingFeed } from '../../features/feed/feedSlice';
// import { loaddingLogin } from '../../features/login/authSlice';
// import { selectLoading } from '../../features/order/orderSlice';

// import { selectproductLoadding } from '../../features/product/productSlice';
// import { selectloadingReceipt } from '../../features/receipt/receiptSlice';
// import { profileLoadding } from '../../features/user/userSlice';

const Spinner = ({ heading }) => {
  // const [loading, setLoadding] = useState('false');
  // const loaddingProduct = useSelector(selectproductLoadding);
  // const loginLoadding = useSelector(loaddingLogin);
  // const loaddingProfile = useSelector(profileLoadding);
  // const feedLoadding = useSelector(loaddingFeed);
  // const loaddingClient = useSelector(selectLoadingClient);
  // const loaddingOrder = useSelector(selectLoading);
  // const loaddingRecei = useSelector(selectloadingReceipt);
  // const dashboardLoadding = useSelector(selectDashboardloading);
  // useEffect(() => {
  //   function checkUserData() {
  //     const loading123 = localStorage.getItem('loadding');

  //     if (loading123) {
  //       setLoadding(loading123);
  //     }
  //   }

  //   window.addEventListener('storage', checkUserData);

  //   return () => {
  //     window.removeEventListener('storage', checkUserData);
  //   };
  // }, []);

  const loading =
    loaddingProduct ||
    loginLoadding ||
    loaddingProfile ||
    feedLoadding ||
    loaddingClient ||
    loaddingOrder ||
    loaddingRecei ||
    dashboardLoadding;
  return (
    <div
      id='upload-loading'
      className='loading'
      style={{
        display: `${loading === false ? 'none' : 'flex'}`,
        top: '50vh',
        backgroundColor: 'gray',
        zIndex: '20000',
        opacity: '0.5',
        position: 'fixed',
      }}
    >
      <div className='box-text-loading'>
        {heading && (
          <h3 className='title text-center' style={{ fontSize: 28 }}>
            {heading}
          </h3>
        )}
        <i style={{ marginTop: 50 }} className='spinning-loader'></i>
      </div>
    </div>
  );
};

export default Spinner;
