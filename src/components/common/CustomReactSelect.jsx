import React, { forwardRef } from 'react';
import Select from 'react-select';
const CustomReactSelect = forwardRef(
  (
    {
      className,
      isGroup,
      styleInput,
      style,

      onChange,
      label,
      allowSelectAll,
      required,
      ...otherProps
    },
    ref
  ) => {
    
    if (isGroup === false) {
      return <Select className={`form-control ${otherProps.classSelect}`} onChange={onChange} {...otherProps} ref={ref} />;
    } else {
      return (
        <div className={`form-input-group ${className}`} style={{ position: 'relative', ...style }}>
          {label ? (
            <p>
              {label}
              <span className='text-red-400 font-bold text-lg'>{required && ' *'}</span>
            </p>
          ) : null}

          {
            allowSelectAll ? (
              <Select 
               {...otherProps}
              options={[otherProps.allOption, ...otherProps.options]}
              onChange={selected => {
                if (
                  selected !== null &&
                  selected.length > 0 &&
                  selected[selected.length - 1].value === otherProps.allOption.value
                ) {
                  return onChange(otherProps.options);
                }
                return onChange(selected);
              }}
              className={`form-control ${otherProps.classSelect}`} styles={{ ...styleInput }} ref={ref} />
            ) : (
              <Select className={`form-control ${otherProps.classSelect}`} styles={{ ...styleInput }} onChange={onChange} {...otherProps} ref={ref} />
            )
          }
        </div>
      );
    }
  }
);

export default CustomReactSelect;
