import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { toggleSidebar } from '../../features/sidebar/sidebarSlice';
import { isOpenSidebar } from '../../features/sidebar/sidebarSlice';
import { MenuIcon } from '../../components/icon/MenuIcon';
const Bookmark = () => {
    const dispatch = useDispatch();
    const isOpen = useSelector(isOpenSidebar);
    const handleClickMenu = () =>{
        dispatch(toggleSidebar(!isOpen))
    }
    
  return (
    <>
        <span className="mobile-menu" onClick={handleClickMenu}>
          <MenuIcon />
        </span>
    </>
  )
}

export default Bookmark