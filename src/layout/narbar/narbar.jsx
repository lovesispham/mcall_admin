import React, { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import { narBar } from "../../util/constan";
import Heading from "../../components/common/Heading";

//icons react-icons
import { BsChevronRight } from "react-icons/bs";
import { BsChevronLeft } from "react-icons/bs";

import { useSelector } from "react-redux";

import { useDispatch } from "react-redux";

import { useOnOutsideClick } from "../../hooks/use-outside";
import { selectProfile } from "../../features/user/userSlice";
import logo from "../../assets/images/logoVntel.png";
import MenuItem from "./MenuItem";
import { toggleSidebar } from "../../features/sidebar/sidebarSlice";
function Navbar(props) {
  const location = useLocation();
  const [hide, setHide] = useState(true);
  const profile = useSelector(selectProfile);
  const { setMenu, menu, setDocumentPhone } = props;
  const [currentlyOpen, setCurrentlyOpen] = useState(null);
  const activeRoute = (routeName) => {
    if (routeName) {
      return location.pathname.includes(routeName);
    }
    return false;
  };

  const [isSubMenuShow, setIsSubMenuShow] = useState(undefined);
  const [toggle, setToggle] = useState([]);
  const [openSub, setOpenSub] = useState(false);
  const [module, setModule] = useState([]);
  const dispatch = useDispatch();
  const hideNarBar = () => {
    // setHide(!hide);
    // setMenu(!menu);
    dispatch(toggleSidebar(!props.isOpen));
  };

  useEffect(() => {
    if (profile?.role === "USER") {
      setModule(
        profile?.org?.has_access
          .filter((v) => v.code !== "CONF")
          .map((n) => n.code)
      );
    } else {
      setModule(profile?.org?.has_access.map((v) => v.code));
    }
  }, [profile]);

  const [inactive, setInactive] = useState(false);

  useEffect(() => {
    props.onCollapse(inactive);
  }, [inactive]);

  // useEffect(() => {
  //   let menuItems = document.querySelectorAll(".menu-item");
  //   menuItems.forEach((el) => {
  //     el.addEventListener("click", (e) => {
  //       const next = el.nextElementSibling;

  //       removeActiveClassFromSubMenu();
  //       menuItems.forEach((el) => el.classList.remove("active"));
  //       el.classList.toggle("active");

  //       if (next !== null) {
  //         next.classList.toggle("active");
  //       }
  //     });
  //   });
  // }, []);

  // //just an improvment and it is not recorded in video :(
  // const removeActiveClassFromSubMenu = () => {
  //   document.querySelectorAll(".sub-menu").forEach((el) => {
  //     el.classList.remove("active");
  //   });
  // };

  return (
    <>
      <div
        className="site-logo"
        style={{
          display: "flex",
          justifyContent: "space-between ",
          flexDirection: !hide && "column-reverse",

          alignItems: "center",
          padding: !hide && "10px",
        }}
      >
        <div className="sitebar-logo" style={{ maxWidth: 70 }}>
          <img
            src={profile?.org?.logo ? `${profile?.org?.logo}` : logo}
            alt="avatar"
            onError={({ currentTarget }) => {
              currentTarget.onerror = null; // prevents looping
              currentTarget.src = logo;
            }}
          />
        </div>
        <div onClick={hideNarBar} className="btn-hide-nav">
          {" "}
          {hide ? <BsChevronLeft /> : <BsChevronRight />}
        </div>
      </div>
      <ul
        className="menu-main"
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "flex-start",
        }}
      >
        {narBar.map((menuItem, index) => {
          return (
            <MenuItem
              activeRoute={activeRoute(menuItem.path)}
              name={menuItem.name}
              exact={menuItem.exact}
              to={menuItem.path}
              icon={menuItem.svg}
              iconSub={menuItem.iconSub}
              subMenus={menuItem.submenu || []}
              isHideMenu={props.isOpen}
              item={menuItem}
              key={index}
              isOpen={index === currentlyOpen}
              currentlyOpen={currentlyOpen}
              setCurrentlyOpen={setCurrentlyOpen}
              handleClick={() =>
                setCurrentlyOpen(
                  index === currentlyOpen && !activeRoute(menuItem.path)
                    ? null
                    : index
                )
              }
            />
          );
        })}
      </ul>
    </>
  );
}

export default Navbar;
