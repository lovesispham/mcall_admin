import React, { useState, useEffect } from "react";
import { Link, NavLink } from "react-router-dom";

const MenuItem = (props) => {
  const { name, subMenus, icon, handleClick, to, exact, activeRoute, iconSub, isOpen } =
    props;
  const [open, setOpen] = useState(false);

  const showSubnav = () => setOpen(!open);
  
  useEffect(() => {
    if(isOpen){
      setOpen(true)
    } else{
      setOpen(false)
    }
  }, [isOpen])
  

  return (
    <div
      className={`menu-item menu-single ${activeRoute ? "active" : ""} `}
      onClick={subMenus && handleClick}
    >
      
        <Link
          exact={exact}
          to={to}
          className="menu-toggle"
        
        >
          <span
            className={`inline-flex items-center ${
              !props.isHideMenu && "ml-4"
            }`}
          >
            {icon}
            
             
                <span
                  style={{
                    marginLeft: "15px",
                    fontSize: "16px",
                  }}
                >
                  {name}
                </span>
                <span
                  className="icon-arrow"
                  onClick={showSubnav}
                >
                  {iconSub}
                </span>
           
          
          </span>
          </Link>
        {open && (
          <ul className={`sub-menu`}>
            {subMenus.map((menu, index) => (
              <li key={index} className="menu-item menu-single">
                <NavLink className={"menu-toggle"} to={menu.path}>
                  {menu.name}
                </NavLink>
              </li>
            ))}
          </ul>
          ) } 
         
    </div>
  );
};

export default MenuItem;
