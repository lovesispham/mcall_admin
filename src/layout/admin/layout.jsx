import React from 'react';

import { Outlet } from 'react-router-dom';
import Spinner from '../../components/common/Spinner';
import Toast from '../../components/common/Toast';

function Layout() {
  return (
    <>
      <Outlet />
      <Toast />
      {/* <Spinner /> */}
    </>
  );
}

export default Layout;
