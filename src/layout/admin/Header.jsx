import React, { useRef, useEffect } from 'react';
import UserProfile from '../user/UserProfile';
import MobileMenu from './MobileMenu';

const Header = ({ menu }) => {
  const headerRef = useRef(null);

  return (
    <div className='header-container'>
      <header className='header navbar'>
        <MobileMenu style={{fontSize:20}} />
        <div style={{ marginLeft: 'auto', display: 'flex' }}>
         
           <UserProfile />
        </div>
      </header>
    </div>
  );
};

export default Header;
