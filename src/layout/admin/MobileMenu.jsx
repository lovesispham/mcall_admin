import React from 'react'
import { useDispatch, useSelector } from 'react-redux'

import {CgMenu} from 'react-icons/cg'
import { isOpenSidebar, toggleSidebar } from '../../features/sidebar/sidebarSlice';
const MobileMenu = (props) => {
  const dispatch = useDispatch();
    const isOpen = useSelector(isOpenSidebar);
    const handleClickMenu = () =>{
        dispatch(toggleSidebar(!isOpen))
    }
  return (
    <div className='mobile-menu' onClick={handleClickMenu}>
        <CgMenu style={props.style} />
    </div>
  )
}

export default MobileMenu