import React, { useState, useEffect } from "react";
import { Outlet } from "react-router-dom";

import Navbar from "../narbar/narbar";
import Header from "./Header";
import Toast from "../../components/common/Toast";
import "./admin.css";
import { useDispatch, useSelector } from "react-redux";
import {
  isOpenSidebar,
  toggleSidebar,
} from "../../features/sidebar/sidebarSlice";
function Admin() {
  const dispatch = useDispatch();
  const [menu, setMenu] = useState(true);
  const isOpen = useSelector(isOpenSidebar);
  const [inactive, setInactive] = useState(false);

  useEffect(() => {
    window.addEventListener("resize", () => {
      if (window.innerWidth < 991) {
        setMenu(false);
      } else {
        setMenu(true);
      }
    });
  }, []);



  return (
    <>
      <Toast />
      <div className={`layout-page ${isOpen && "menu-collapse"}`}>
        <div  className={`menu-vertical ${isOpen && "menu-no-animation"} `}>
          <Navbar
            setMenu={setMenu}
            menu={menu}
            isOpen={isOpen}
            onCollapse={(inactive) => {
              setInactive(inactive);
            }}
          />
        </div>
        <Header menu={menu} />
        <div className={`main-content ${isOpen ? "" : ""}`}>
          <div className="container">
            {" "}
            <Outlet />
          </div>
        </div>
        <div onClick={()=>{
          dispatch(toggleSidebar(!isOpen))
        }} class="layout-overlay layout-menu-toggle"></div>
      </div>
    </>
  );
}

export default Admin;
