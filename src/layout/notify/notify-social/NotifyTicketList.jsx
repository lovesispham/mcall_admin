import React from 'react';
import Heading from '../../../components/common/Heading';
import ticket_icon from '../../../assets/images/ticket.png';
import { FaCheckDouble } from 'react-icons/fa';
import { delNotify } from '../../../features/notify/notify';
import { useDispatch } from 'react-redux';
import { showMessageError, showMessageSuccess } from '../../../features/alert/alert-message';
const NotifyTicketList = ({ dataNotifyTicket }) => {
  const dispatch = useDispatch();

  const handleDel = async (id) => {
    dispatch(delNotify({ id: id }))
      .unwrap()
      .then(() => {
        dispatch(showMessageSuccess());
      })
      .catch(() => {
        dispatch(showMessageError());
      });
  };
  return (
    <>
      {dataNotifyTicket && dataNotifyTicket.length > 0 ? (
        <div className='notify-list'>
          <Heading type='h2' style={{ fontWeight: 'bold' }}>
            Thông báo ticket
          </Heading>

          {dataNotifyTicket &&
            dataNotifyTicket.map((item, index) => (
              <div className='item' key={index}>
                <div className='photo'>
                  <img src={ticket_icon} alt='' />
                </div>
                <div className='info'>
                  <div className='source'>{item.source}</div>
                  <div className='username'>{item.recipient}</div>

                  <div className='text-msg'>{item.content}</div>
                  <span className='btn-del' onClick={() => handleDel(item.id)}>
                    <i className={`fas ${item.status === 'false' ? '' : 'color-gray'}`}>
                      <FaCheckDouble />
                    </i>
                  </span>
                </div>
              </div>
            ))}
        </div>
      ) : (
        <Heading type='h2' style={{ fontWeight: 'bold', fontSize: '1rem' }}>
          Không có thông báo.
        </Heading>
      )}
    </>
  );
};

export default NotifyTicketList;
