import React, { useState, useRef, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FaRegBell, FaClone } from 'react-icons/fa';
import NotifyList from './NotifyList';
import NotifyTicketList from './NotifyTicketList';
import { selectProfile } from '../../../features/user/userSlice';
import { getMessage, getMessageZalo, getNotify } from '../../../features/notify/notify';
import { showMessageSuccess } from '../../../features/alert/alert-message';
import { useOnOutsideClick } from '../../../hooks/use-outside';
const Notify = () => {
  const dispatch = useDispatch();
  const notifyList = useSelector((state) => state.notify.list);
  const status = useSelector((state) => state.notify);
  const [active, setActive] = useState(false);
  const [activeTicket, setActiveTicket] = useState(false);
  //websocket
  const webSocket = useRef(null);
  const [msgSocket, setMsgSocket] = useState([]);
  const [countNotify, setCountNotify] = useState('0');
  const [dataNotify, setDataNotify] = useState([]);

  const [countNotifyTicket, setCountNotifyTicket] = useState('0');
  const [dataNotifyTicket, setDataNotifyTicket] = useState([]);


  const [messageFb, setMessageFb] = useState([]);
  const [messageZalo, setMessageZalo] = useState([]);

  const [dataNotifySum, setDataNotifySum] = useState([]);

  const profile = useSelector(selectProfile);

  var orgCode = profile?.org?.unique_id;
  var msgLoadApi = msgSocket && msgSocket.filter((x) => x.message !== `event_${orgCode}`);

  var url = `${process.env.REACT_APP_API_WSS}/ws/socket-server/?org_Name=${orgCode}`;
  var orgId = profile?.id;
  
  useEffect(() => {
    if (orgCode !== undefined) {
      webSocket.current = new WebSocket(url);

      webSocket.current.onmessage = (message) => {
        let msg = JSON.parse(message.data);
        if (msg?.source === 'FACEBOOK') {
          setMessageFb([...messageFb, msg]);
        }
        if (msg?.source === 'ZALO') {
          setMessageZalo([...messageZalo, msg]);
        }
        setMsgSocket((prev) => [...prev, JSON.parse(message.data)]);
      };
    }
  }, [url, orgCode]);

  useEffect(() => {
    if (messageFb) {
      dispatch(getMessage(messageFb));
    }
  }, [messageFb]);


  useEffect(() => {
    if (messageZalo) {
      dispatch(getMessageZalo(messageZalo));
    }
  }, [messageZalo]);



  useEffect(() => {
    if (status.isSuccess === true) {
      dispatch(getNotify());
      dispatch(showMessageSuccess());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status.isSuccess]);

  useEffect(() => {
    if (msgLoadApi && msgLoadApi.length > 0) {
      dispatch(getNotify());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [msgLoadApi.length]);

  useEffect(() => {
    if (notifyList) {
      const resNotify = notifyList && notifyList.notify && notifyList.notify.filter((x) => x.source !== 'Ticket');
      const resNotifyTicket =
        notifyList &&
        notifyList.notify &&
        notifyList.notify.filter((x) => x.source === 'Ticket' && x.send_to === orgId);

      if (resNotify && resNotify.length > 0) {
        setDataNotify(resNotify);
      }
      if (resNotifyTicket && resNotifyTicket.length > 0) {
        setDataNotifyTicket(resNotifyTicket);
        setCountNotifyTicket(resNotifyTicket.length);
      }
    }
  }, [notifyList, orgId]);

  useEffect(() => {
    if (!dataNotifySum?.find((v) => notifyList?.notify?.map((v) => v?.id)?.includes(v?.id))) {
      setDataNotifySum(dataNotifySum?.concat(notifyList?.notify));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [notifyList]);

  const { innerBorderRef } = useOnOutsideClick(() => {
    setActiveTicket(false);
    setActive(false);
  });
  useEffect(() => {
    const countNotify = dataNotifySum?.filter((x) => x?.source !== 'Ticket' && x?.readed === false);
    setCountNotify(countNotify?.length);
  }, [dataNotifySum]);

  return (
    <>
      <div className='notify-drp' ref={innerBorderRef}>
        <div className='notify-icon' onClick={() => setActive(!active)}>
          <FaRegBell />
          <div className='badget'>{countNotify > 99 ? '99+' : countNotify}</div>
        </div>

        <div className={`dropdown-menu ${active ? 'active' : ''}`}>
          <NotifyList dataNotify={dataNotify} notifyList={notifyList} />
        </div>
      </div>
      {/* 
      <div className='notify-drp' ref={innerBorderRef}>
        <div className='notify-icon' onClick={() => setActiveTicket(!activeTicket)}>
          <FaClone />
          <div className='badget'>{countNotifyTicket > 99 ? '99+' : countNotifyTicket}</div>
        </div>

        <div className={`dropdown-menu ${activeTicket ? 'active' : ''}`}>
          <NotifyTicketList dataNotifyTicket={dataNotifyTicket} />
        </div>
      </div> */}
    </>
  );
};

export default Notify;
