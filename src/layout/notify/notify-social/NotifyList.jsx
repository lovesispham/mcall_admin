import React, { Fragment, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import Heading from '../../../components/common/Heading';
import Linkify from 'linkify-react';
import { useNavigate } from 'react-router';
import { TbMail, TbMailOpened } from 'react-icons/tb';
import { getDetail, getNotify, updateNotify } from '../../../features/notify/notify';
import InfiniteScroll from '../../../hooks/InfiniteScroll';
import { useState } from 'react';

const NotifyList = ({ dataNotify }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [listNotify, setListNotify] = useState([]);
  const [page, setPage] = useState(0);
  const [total, setTotal] = useState();
  const handleGetInfoMsg = (item) => {
    dispatch(getDetail(item));
  };

  useEffect(() => {
    dispatch(
      getNotify({
        limit: 10,
        offset: (page - 1 + 1) * 10,
      })
    )
      .unwrap()
      .then((res) => {
        setListNotify(listNotify.concat(res?.data?.notify));
        setTotal(res?.data?.count);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  return (
    <>
      {dataNotify && dataNotify.length > 0 ? (
        <div className='notify-list'>
          <Heading type='h2' style={{ fontWeight: 'bold' }}>
            Thông báo
          </Heading>

          <InfiniteScroll
            loader={<NotifyList />}
            className='msg-content'
            fetchMore={() => {
              setPage((prev) => prev + 1);
            }}
            hasMore={listNotify.length < total}
            endMessage={<p></p>}
          >
            {listNotify.map((item, index) => {
              const isFacebook = item.source === 'FACEBOOK';
              return isFacebook ? (
                <Fragment key={index}>
                  <div
                    className={`item ${item.readed && 'readed'}`}
                    onClick={() => {
                      if (item.readed) {
                        handleGetInfoMsg(item);
                        navigate('/admin/marketing/facebook/fb-chat');
                      } else {
                        dispatch(
                          updateNotify({
                            id: item.id,
                            readed: 'true',
                          })
                        ).then(() => {
                          handleGetInfoMsg(item);
                          navigate('/admin/marketing/facebook/fb-chat');
                        });
                      }
                    }}
                  >
                    <div className='photo'>{item.readed ? <TbMailOpened /> : <TbMail />}</div>
                    <div className='info'>
                      <div className='source'>{item.source}</div>
                      <div className='username'>{item.name}</div>
                      <Linkify className='text-msg' tagName='p'>
                        {item.content}
                      </Linkify>

                      {/* <span
                      className='btn-del'
                      // onClick={() => dataContext.handleDel(item.id)}
                    >
                      <i className={`fas ${item.status === 'false' ? '' : 'color-gray'}`}>
                        <FaCheckDouble />
                      </i>
                    </span> */}
                      {item.status === 'false' ? <div className='status'></div> : null}
                    </div>
                  </div>
                </Fragment>
              ) : (
                <Fragment key={index}>
                  <div
                    className={`item ${item.readed && 'readed'}`}
                    onClick={() => {
                      if (item.readed) {
                        dispatch(
                          updateNotify({
                            id: item.id,
                            readed: 'true',
                          })
                        );
                      }
                    }}
                  >
                    <div className='photo'>{item.readed ? <TbMailOpened /> : <TbMail />}</div>
                    <div className='info'>
                      <div className='source'>{item.source}</div>
                      <div className='username'>{item.name}</div>
                      <Linkify className='text-msg' tagName='p'>
                        {item.content}
                      </Linkify>

                      {/* <span
                      className='btn-del'
                      // onClick={() => dataContext.handleDel(item.id)}
                    >
                      <i className={`fas ${item.status === 'false' ? '' : 'color-gray'}`}>
                        <FaCheckDouble />
                      </i>
                    </span> */}
                      {item.status === 'false' ? <div className='status'></div> : null}
                    </div>
                  </div>
                </Fragment>
              );
            })}
          </InfiniteScroll>
        </div>
      ) : (
        <Heading type='h2' style={{ fontWeight: 'bold', fontSize: '1rem' }}>
          Không có thông báo.
        </Heading>
      )}
    </>
  );
};

export default NotifyList;
