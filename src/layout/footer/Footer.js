import React, { useEffect } from 'react';
import { getProfile } from '../../features/user/userSlice';
import { selectProfile } from '../../features/user/userSlice';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

const Footer = () => {
  var CurrentYear = new Date().getFullYear();

  const profile = useSelector(selectProfile);

  return (
    <footer className='footer'>
      <div className='copyright'>
        Copyright © <span id='year'>{CurrentYear} </span>
        <Link className='font-semibold' to='http://vntel.vn' target='_blank' rel='noreferrer'>
          {profile?.user_obj?.org?.name}
        </Link>
        . All rights reserved.
      </div>
      <div className='hotline'>
        Hotline hỗ trợ: <Link to='callto:+84439928886'>{profile?.org?.phone}</Link>
      </div>
    </footer>
  );
};

export default Footer;
