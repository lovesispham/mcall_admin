import React, { useState } from 'react';
import logo from '../../assets/images/logo.png';
import { userProfile } from '../../util/constan';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { selectProfile, resetProfile } from '../../features/user/userSlice';
import { LogOutIcon } from '../../components/icon/LogOutIcon';
// import ReactTooltip from 'react-tooltip';
import { useOnOutsideClick } from '../../hooks/use-outside';
import { useDispatch } from 'react-redux';
const UserProfile = () => {
  const [active, setActive] = useState(false);
  const { innerBorderRef } = useOnOutsideClick(() => {
    setActive(false);
  });
  const dispatch = useDispatch();
  const logout = async () => {
    await localStorage.clear();
    await dispatch(resetProfile());
    window.location.reload()
  };

   const profile = useSelector(selectProfile);

  const name = profile?.first_name && profile?.last_name && `${profile?.last_name} ${profile?.first_name}` || null;
  const email = profile?.email || null;
  const username = profile?.username || null;
  return (
    <div className='user-profile-drp' ref={innerBorderRef}>
      <div style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center', gap: '10px' }}>
        <div
          style={{ maxWidth: '100px', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}
          data-tip={name || email || username}
        >
          {name || email || username}
        </div>
        {/* <ReactTooltip /> */}
        <div className='avatar' onClick={() => setActive(!active)}>
          <img
            src={profile?.profile_pic ? profile?.profile_pic : logo}
            alt='avatar'
            onError={({ currentTarget }) => {
              currentTarget.onerror = null; // prevents looping
              currentTarget.src = logo;
            }}
          />
        </div>
      </div>
      <div className={`dropdown-menu ${active ? 'active' : ''}`}>
        <div className='user-profile-info'>
          <div className='user-name'>{name || email || username}</div>
          <div className='user-role'>{profile?.role}</div>
        </div>
        <ul>
          {userProfile.map((item, indx) => (
            <li className='dropdown-item' key={indx}>
              <Link to={item.path}>
                <span dangerouslySetInnerHTML={{ __html: item.svg }}></span>
                <span>{item.name}</span>
              </Link>
            </li>
          ))}

          <li className='dropdown-item' onClick={() => logout()}>
            <Link>
              <LogOutIcon />
              <span>Đăng xuất</span>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default UserProfile;
