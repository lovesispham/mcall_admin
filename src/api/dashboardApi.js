import axiosClient from './axiosClient';
const controller = new AbortController();
const DashboardApi = {
  getAllDashboards(params) {
    const url = `/report/dashboard/`;
    return axiosClient.get(url, { params }, { signal: controller.signal });
  },
  getTopproduct(params) {
    const url = '/products/top-product/';
    return axiosClient.get(url, { params });
  },
  getOrgAgent(params) {
    const url = '/dashboard/org-report/';
    return axiosClient.get(url, { params });
  },
  getOrderReport(params) {
    const url = '/leads/order-report/';
    return axiosClient.get(url, { params });
  },
  getStatusUser() {
    const url = '/report/connected-device/';
    return axiosClient.get(url);
  },
  getPayment(params) {
    const url = '/leads/payment-dashboard/';
    return axiosClient.get(url, { params });
  },
  getPaymentReport(params) {
    const url = '/leads/payment-report/';
    return axiosClient.get(url, { params });
  },
  getSummary(params) {
    const url = '/report/cdr-summary/';
    return axiosClient.get(url, { params });
  },
};
export default DashboardApi;
