import axios from 'axios';
const axiosClient = axios.create({
  baseURL: `${import.meta.env.VITE_REACT_APP_API_URL}api/`,
  timeout: 10000,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});

// Add a request interceptor
axiosClient.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    localStorage.setItem('loadding', true);
    const token = localStorage.getItem('access_token');
    // const token = process.env.TOKEN;
    config.headers.Authorization = token ? 'JWT ' + localStorage.getItem('access_token') : '';
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

axiosClient.interceptors.response.use(
  (response) => {
    localStorage.setItem('loadding', false);
    return response;
  },
  (error) => {
    const originalRequest = error.config;
    // Prevent infinite loops
    // if(){
    // const navRef = useRef(useNavigate());

    // // }
    // navRef.current('/login');
    console.log(error.response.data)
    if (
      error.response.status === 401 &&
      error.response.data.detail === 'Token is invalid or expired'
      // error.response.data.detail === 'User not found'
    ) {
      localStorage.clear();
      window.location.href = '/login';
      return Promise.reject(error);
    }
    if (
      error.response.status === 401 &&
      (error.response.data.detail === 'Authentication credentials were not provided.' ||
      error.response.data.detail === 'User is inactive' ||
      error.response.data.detail === 'User not found' 
      )
      // error.response.data.detail === 'User not found'
    ) {
      localStorage.clear();
      window.location.href = '/login';
      return Promise.reject(error);
    }

    // if (
    //   error.response.status === 401 &&

    //   // error.response.data.detail === 'User not found'
    // ) {
    //   localStorage.clear();
    //   window.location.href = '/login';
    //   return Promise.reject(error);
    // }

    // if (error.code === 'ERR_NETWORK') {
    //   localStorage.clear();
    //   window.location.href = '/500';
    //   return Promise.reject(error);
    // }

    // if (error?.code === 'ERR_BAD_REQUEST') {
    //   localStorage.clear();
    //   window.location.href = '/505';
    //   return Promise.reject(error);
    // }
    // console.log('huu thin123');

    if (error.response.status === 401 && !originalRequest._retry) {
      const refreshToken = localStorage.getItem('refresh_token');

      if (refreshToken) {
        const tokenParts = JSON.parse(atob(refreshToken.split('.')[1]));

        // exp date in token is expressed in seconds, while now() returns milliseconds:
        const now = Math.ceil(Date.now() / 1000);

        if (tokenParts.exp > now) {
          return axiosClient
            .post('auth/refresh/', { refresh: refreshToken })
            .then((response) => {
              localStorage.setItem('access_token', response.data.access);
              localStorage.setItem('refresh_token', response.data.refresh);

              axiosClient.defaults.headers['Authorization'] = 'JWT ' + response.data.access;
              originalRequest.headers['Authorization'] = 'JWT ' + response.data.access;

              return axiosClient(originalRequest);
            })
            .catch((err) => {});
        } else {
          window.location.href = '/login/';
        }
      } else {
      }
    }

    // specific error handling done elsewherenp
    localStorage.setItem('loadding', false);
    return Promise.reject(error);
  }
);

export default axiosClient;
