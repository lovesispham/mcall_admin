import { AiOutlineDashboard } from "react-icons/ai";
import { BiChevronDown } from "react-icons/bi";
import { BsBuilding } from "react-icons/bs";
import { HiUserGroup } from "react-icons/hi";

export const userProfile = [
  {
    name: "Thông tin cá nhân",
    path: "/profile",
    svg: ` <svg
      xmlns='http://www.w3.org/2000/svg'
      width='24'
      height='24'
      viewBox='0 0 24 24'
      fill='none'
      stroke='currentColor'
      strokeWidth='2'
      strokeLinecap='round'
      stroke-linejoin='round'
    >
    <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
      <circle cx="12" cy="7" r="4"></circle></svg>`,
  },
];

export const narBar = [
  {
    name: "Báo cáo",
    role: ["ADMIN", "SUPERADMIN"],
    path: "/dashboard",
    module: "DASHBOARD",
    svg: <AiOutlineDashboard style={{ width: "20px", height: "20px" }} />,
  },
  {
    name: "Đại lý",
    role: ["ADMIN", "SUPERADMIN"],
    path: "agent",
    module: "CONF",
    svg: <HiUserGroup style={{ width: "20px", height: "20px" }} />,
    iconSub: <BiChevronDown />,
    submenu: [
      {
        name: "Thông tin đại lý",
        path: 'agent/org',
        role: ["ADMIN", "SUPERADMIN"],
      },
      {
        name: "Các đại lý",
        path: "agent/all",
        role: ["ADMIN", "SUPERADMIN"],
      },
    ],
  },
  {
    name: "Công ty",
    role: ["ADMIN", "SUPERADMIN"],
    path: "company2",
    module: "CONF",
    svg: <BsBuilding style={{ width: "20px", height: "20px" }} />,
    iconSub: <BiChevronDown />,
    submenu: [
      {
        name: "Danh sách công ty",
        path: "company2/company",
        role: ["ADMIN", "SUPERADMIN"],
      },
      {
        name: "Danh sách tài khoản",
        path: "company2/user",
        role: ["ADMIN", "SUPERADMIN"],
      },
    ],
  },
];

export const gender = [
  { label: "Chưa xác định", value: 0, key: "gender" },
  { label: "Nam", value: 1, key: "gender" },
  { label: "Nữ", value: 2, key: "gender" },
];

export const role = [
  { label: "Admin", value: "ADMIN", key: "role" },
  { label: "User", value: "USER", key: "role" },
];
