export const groupBy = (x,f)=>x.reduce((a,b,i)=>((a[f(b,i,x)]||=[]).push(b),a),{});


export const isPositiveInteger = (str) => {
    const num = Number(str);
  
    if (Number.isInteger(num) && Math.sign(num) === 1) {
      return true;
    }
  
    return false;
  }