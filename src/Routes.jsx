import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import PermissionDenied from "./router/PermissionDenied";
import ProtectedRoutes from "./router/protectedRoutes";
import PublicRoutes from "./router/PublicRoutes";
import Auth from "./page/login/auth";
import NotFound from "./page/admin/notFound/NotFound";
import Dashboard from "./page/admin/DashBoard/Dashboard";
import Admin from "./layout/admin/admin";
import Org from "./page/admin/configuration/org/org";
import User from "./page/admin/configuration/user/User";
import Company from "./page/admin/configuration/org/Company";
import UserDetail from "./page/admin/configuration/user/UserDetail";
import CompanyAgent from "./page/admin/configuration/component/CompanyAgent";
import Profile from "./page/admin/User/Profile";
import Agent from "./page/admin/configuration/org/Agent";
import Detail from "./page/admin/configuration/org/Detail";
import Layout from "./layout/admin/layout";
import DetailCompany from "./page/admin/configuration/org/DetailCompany";
import AgentOrg from "./page/admin/configuration/org/AgentOrg";
const MainRoutes = () => {
  const is_superuser = localStorage.getItem("is_superuser");
  const role = localStorage.getItem("is_superuser");
  return (
    <Routes>
      {/** Protected Routes */}
      {/** Wrap all Route under ProtectedRoutes element */}
      <Route exact path="/" element={<Layout />}>
        <Route
          path="/"
          element={
            <ProtectedRoutes
              isSuper={is_superuser}
              role={role}
              roleRequired={["SUPERADMIN"]}
            />
          }
        >
          <Route path="/" element={<Admin />}>
            <Route path="/" element={<Navigate replace to="dashboard" />} />
            <Route path="dashboard" element={<Dashboard />} />
            <Route path="profile" element={<Profile />} />
            <Route path="company" element={<CompanyAgent />} />
            <Route path="company2">
              <Route
                path="/company2"
                element={<Navigate replace to="company" />}
              />
              <Route path="company" element={<Company />} />
              <Route path="company/:id" element={<DetailCompany />} />
              <Route path="company/:id/:id" element={<UserDetail />} />

              <Route path="agent" element={<Agent />} />
              <Route path="user" element={<User />} />
              <Route path="user/:id" element={<UserDetail />} />
            </Route>
            <Route path="agent">
              <Route path="/agent" element={<Navigate replace to="org" />} />
              <Route path="org" element={<AgentOrg />} />
              <Route path="all" element={<Agent />} />
              <Route path="all/:id" element={<Detail />} />
              <Route path="org/:id" element={<Detail />} />
            </Route>
          </Route>
        </Route>

        {/** Public Routes */}
        {/** Wrap all Route under PublicRoutes element */}
        <Route path="login" element={<PublicRoutes />}>
          <Route path="/login" element={<Auth />} />
        </Route>
        <Route 
               path="/denied" element={<PermissionDenied roleRequired={["ADMIN","USER"]} />} />
        {/** Permission denied route */}

        <Route path="*" element={<NotFound />} />
      </Route>
    </Routes>
  );
};

export default MainRoutes;
