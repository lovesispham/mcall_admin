import React from 'react'
import { useSelector } from 'react-redux';
import { selectProfile } from './features/user/userSlice';

const RoleWrapper = ({ children, roles }) => {
    const user = useSelector(selectProfile)
    // Handles initial mount if user object is fetched
    if (user === undefined) {
      return null; // or loading indicator/spinner/etc
    }
  
    const canAccess = roles.some(
      (role) => user?.org?.has_access.map((v) => v.code).includes(role),
    );
    return canAccess
      ? children
      : <Navigate to="/dashboard" replace />; // or any safe route
  };

export default RoleWrapper