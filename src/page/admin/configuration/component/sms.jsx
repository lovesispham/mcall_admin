import React, { useState } from 'react';
import { BsPlusSquareFill } from 'react-icons/bs';
import Heading from '../../../../components/common/Heading';
import { Section } from '../../../../components/common/Section';
import ConfigurationSMS from './componentSms/configurationSMS';
import NodeSMS from './componentSms/nodeSMS';

const Sms = ({ module }) => {
  const [key, setKey] = useState('all');

  const SettingAll = [
    { key: 'all', name: 'Cấu Hình Tin Nhắn' },
    { key: 'department', name: 'Mẫu Tin Nhắn' },
  ];

  const listdetail = {
    all: <ConfigurationSMS />,
    department: <NodeSMS />,
  };

  return (
    <Section className='box-general'>
      <Heading className='heading' type='h2'>
        Cấu hình tin nhắn
      </Heading>
      <div className='box-tabs'>
        <div className='tabs-list' style={{ margin: 0 }}>
          {SettingAll.map((v, i) => (
            <div
              key={i}
              className={`tab-item ${v.key === key ? 'active' : ''}`}
              onClick={() => {
                setKey(v.key);
              }}
            >
              <p>{v.name.toUpperCase()}</p>
            </div>
          ))}
        </div>
      </div>

      <div className='box-form-general'>{listdetail[key]}</div>
    </Section>
  );
};

export default Sms;
