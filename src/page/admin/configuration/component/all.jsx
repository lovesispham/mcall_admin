import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import Heading from '../../../../components/common/Heading';
import { Section } from '../../../../components/common/Section';
import { selectProfile } from '../../../../features/user/userSlice';
import DataStatus from '../../DataList/Tabs/DataStatus';
import DataTag from '../../DataList/Tabs/DataTag';
import Decentralization from './componentAll/decentralization';
import Department from './componentAll/department';
import InfomationAll from './componentAll/infomationAll';
import Staff from './componentAll/staff';

const All = ({ module }) => {
  const [key, setKey] = useState('all');
  const profile = useSelector(selectProfile);
  const SettingAll = [
    { key: 'all', name: 'Thông tin chung', role: ['ADMIN', 'SUPERADMIN', 'USER'] },
    { key: 'department', name: 'Phòng ban', role: ['ADMIN', 'SUPERADMIN'] },
    { key: 'staff', name: 'Nhân viên', role: ['ADMIN', 'SUPERADMIN', 'USER'] },
    { key: 'extension', name: 'Phân quyền', role: ['ADMIN', 'SUPERADMIN'] },
    { key: 'tag', name: 'Tag', role: ['ADMIN', 'SUPERADMIN'] },
    { key: 'status', name: 'Trạng thái', role: ['ADMIN', 'SUPERADMIN'] },
  ];

  const listdetail = {
    all: <InfomationAll />,
    department: <Department />,
    staff: <Staff module={module} />,
    extension: <Decentralization />,
    tag: <DataTag />,
    status: <DataStatus />,
  };
  return (
    <Section className='box-general'>
      <Heading className='heading' type='h2'>
        Cấu hình chung
      </Heading>
      <div className='box-tabs'>
        <div className='tabs-list' style={{ margin: 0 }}>
          {SettingAll.filter((item) => item?.role?.includes(profile?.role)).map((v, i) => {
            return (
              <div
                key={i}
                className={`tab-item ${v.key === key ? 'active' : ''}`}
                onClick={() => {
                  setKey(v.key);
                }}
              >
                <p>{v.name.toUpperCase()}</p>
              </div>
            );
          })}
        </div>
      </div>

      <div className='box-form-general'>{listdetail[key]}</div>
    </Section>
  );
};

export default All;
