import React, { useState } from 'react';
import { AiOutlineClose } from 'react-icons/ai';
import { useDispatch, useSelector } from 'react-redux';
import Button from '../../../../components/common/Button';
import Col from '../../../../components/common/Col';
import FormCheckBox from '../../../../components/common/FormCheckBox';
import FormInput from '../../../../components/common/FormInput';
import Heading from '../../../../components/common/Heading';
import { Section } from '../../../../components/common/Section';
import { showMessageError, showMessageSuccess } from '../../../../features/alert/alert-message';
import { postExtension, putOrgapi } from '../../../../features/common/commonThunk';
import { getProfile, selectProfile } from '../../../../features/user/userSlice';
import { CssButton, cssInput } from '../../../../scss/cssCustoms';
import Decentralization from './componentAll/decentralization';

const Switchboard = ({ module }) => {
  const dispatch = useDispatch();
  const profile = useSelector(selectProfile);
  const [data, setData] = useState({});
  const [mes, setMes] = useState({});
  const [active, setActive] = useState(data?.hide_number || profile?.org?.hide_number);
  const [activeEye, setActiveEye] = useState(false);
  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleCreate = () => {
    if (!data.name && !profile?.org?.name) {
      setMes({ name: 'Không Được Để Trống Tên Doanh Nghiệm' });
      return;
    }
    setMes({});
    dispatch(putOrgapi({ ...data, id: profile?.org?.id }))
      .unwrap()
      .then((res) => {
        dispatch(getProfile());
        dispatch(showMessageSuccess('Cập Nhật Thông Tin Thành Công'));
      })
      .catch((err) => {
        dispatch(showMessageError('Cập Nhật Thông Tin Thất Bại'));
      });
  };
  const synchronized = () => {
    dispatch(postExtension())
      .unwrap()
      .then((res) => {
        dispatch(showMessageSuccess('Đồng bộ thành công'));
      })
      .catch((err) => {
        dispatch(showMessageError('Đồng bộ thất bại'));
      });
  };

  return (
    <Section className='general-info form-board'>
      <Heading className='heading' type='h2'>
        Thông tin tổng đài
      </Heading>
      <div className='row'>
        <Col className={3}>
          <FormInput
            type='text'
            label={'Tên doanh nghiệp'}
            value={data?.name === undefined ? profile?.org?.name || '' : data?.name}
            name='name'
            onChange={handleChange}
            required
          />
        </Col>
        <Col className={3}>
          <FormInput
            type='text'
            label={'Số điện thoại'}
            name='phone'
            value={data?.phone === undefined ? profile?.org?.phone || '' : data?.phone}
            onChange={handleChange}
            required
          />
        </Col>
        <Col className={3}>
          <FormInput
            type='text'
            label={'Địa chỉ'}
            value={data?.address === undefined ? profile?.org?.address || '' : data?.address}
            name='address'
            onChange={handleChange}
            required
          />
        </Col>
        <Col className={6}>
          <FormInput
            type='text'
            label={'Tên đăng nhập'}
            onChange={handleChange}
            name='pbx_username'
            value={data?.pbx_username === undefined ? profile?.org?.pbx_username || '' : data?.pbx_username}
            required
          />
        </Col>
        <Col className={6}>
          <FormInput
            label={'Mật khẩu tổng đài'}
            type='password'
            value={data?.pbx_password === undefined ? profile?.org?.pbx_password || '' : data?.pbx_password}
            name='pbx_password'
            onChange={handleChange}
            active={activeEye}
            setActive={setActiveEye}
            required
          />
        </Col>
        <Col className={12}>
          <FormCheckBox
            pos={'left'}
            label={'Hiển thị số điện thoại thách hàng'}
            name='hide_number'
            onChange={(e) => setActive(e.currentTarget.checked)}
            checked={active}
          />
        </Col>
        <div className='text-right'>
          <Button className='bg-theme' onClick={() => synchronized()} style={{ marginRight: '5px' }}>
            Đồng bộ
          </Button>
          <Button className='bg-theme' onClick={() => handleCreate()}>
            Cập nhập
          </Button>
        </div>
      </div>
      <div style={{ marginTop: '10px' }}>
        <Heading className='heading' type='h2'>
          Danh sách Tài khoản
        </Heading>
        <Decentralization />
      </div>
    </Section>
  );
};

export default Switchboard;

export const cssBlock = {
  backgroundColor: 'white',
  height: '500px',
  width: '500px',
  marginTop: '20px',
  padding: '10px',
};
