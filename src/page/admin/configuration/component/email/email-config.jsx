import React, { useEffect, useState } from 'react';
import { FaCheck, FaPen, FaTimes } from 'react-icons/fa';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import Button from '../../../../../components/common/Button';
import Empty from '../../../../../components/common/Empty';
import Modal, { ModalContent } from '../../../../../components/common/Modal';
import { getConfigEmail } from '../../../../../features/config/email';
import { selectProfile } from '../../../../../features/user/userSlice';
import { CheckPermission } from '../../../../../util/help';
import DialogConfigEmail from './DialogConfigEmail';

const EmailConfig = ({ module }) => {
  const [showModal, setShowModal] = useState(false);
  const [data, setData] = useState('');
  const emailList = useSelector((state) => state.config.emailList);
  const status = useSelector((state) => state.config);
  const profile = useSelector(selectProfile);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getConfigEmail());
  }, []);

  useEffect(() => {
    if (status.success === true) {
      dispatch(getConfigEmail());
    }
  }, [status.success]);
  return (
    <>
      <Modal active={showModal}>
        <ModalContent
          heading={!data ? 'Cấu hình' : 'Sửa cấu hình'}
          onClose={() => {
            setShowModal(false);
          }}
        >
          <DialogConfigEmail setData={setData} setUDO={setShowModal} dataEdit={data} />
        </ModalContent>
      </Modal>
      {CheckPermission(module, 'POST') && profile.role !== 'USER' && (
        <Button style={{ marginBottom: 10 }} className='bg-theme' onClick={() => setShowModal(true)}>
          + Tạo cấu hình
        </Button>
      )}
      <div className='table-wrapper'>
        <table className='table mb-0'>
          <thead className='bg-white'>
            <tr>
              <th scope='col' className='border-0 text-center'>
                STT
              </th>

              <th scope='col' className='border-0'>
                Tên
              </th>

              <th scope='col' className='border-0'>
                Host
              </th>
              <th scope='col' className='border-0'>
                Port
              </th>
              <th scope='col' className='border-0 text-center'>
                use_tls
              </th>
              <th scope='col' className='border-0'>
                Tài khoản
              </th>
              <th scope='col' className='border-0'>
                Hành động
              </th>
            </tr>
          </thead>

          <tbody>
            {emailList?.email_cfg?.map((item, idx) => {
              return (
                <tr key={idx}>
                  <td>{idx + 1}</td>
                  <td>{item.name}</td>
                  <td>{item.host}</td>
                  <td>{item.port}</td>
                  <td className='text-center'>
                    <div className={`status ${item.use_tls ? 'is_success' : 'is_error'}`}>
                      {item.use_tls ? <FaCheck /> : <FaTimes />}
                    </div>
                  </td>
                  <td>{item?.username}</td>
                  <td className='flex'>
                    <Button
                      className='btn-edit'
                      onClick={() => {
                        setData(item);
                        setShowModal(true);
                      }}
                    >
                      <FaPen />
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        {emailList?.email_cfg?.length === 0 && <Empty description='Không có dữ liệu'></Empty>}
      </div>
    </>
  );
};

export default EmailConfig;
