import React, { useState, useEffect } from 'react';
import { FaCheck, FaPen, FaTimes } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import Button from '../../../../../components/common/Button';
import Empty from '../../../../../components/common/Empty';
import Modal, { ModalContent } from '../../../../../components/common/Modal';
import { Pagination } from '../../../../../components/common/Pagination';
import { getEmailTemp } from '../../../../../features/config/email';
import { usePaginationState } from '../../../../../hooks/use-pagination';
import { CheckPermission } from '../../../../../util/help';
import DialogTemplateEmail from './DialogTemplateEmail';

const EmailTemplates = ({ module }) => {
  const dispatch = useDispatch();
  const [showModal, setShowModal] = useState(false);
  const [data, setData] = useState('');
  const emailList = useSelector((state) => state.config.emailTempList);
  const status = useSelector((state) => state.config);

  const pagination = usePaginationState();

  useEffect(() => {
    dispatch(
      getEmailTemp({
        limit: pagination.perPage,
        offset: pagination.perPage * pagination.page - pagination.perPage,
      })
    );
  }, [pagination]);

  useEffect(() => {
    if (status.success === true) {
      dispatch(getEmailTemp());
    }
  }, [status.success]);
  return (
    <>
      <Modal active={showModal}>
        <ModalContent
          className='pu-email-template'
          heading={!data ? 'Cấu hình' : 'Sửa mẫu'}
          onClose={() => {
            setShowModal(false);
          }}
        >
          <DialogTemplateEmail setData={setData} setUDO={setShowModal} dataEdit={data} />
        </ModalContent>
      </Modal>
      {CheckPermission(module, 'POST') && (
        <Button style={{ marginBottom: 10 }} className='bg-theme' onClick={() => setShowModal(true)}>
          + Tạo mẫu
        </Button>
      )}
      <div className='table-wrapper'>
        <table className='table mb-0'>
          <thead className='bg-white'>
            <tr>
              <th scope='col' className='border-0 text-center'>
                STT
              </th>

              <th scope='col' className='border-0'>
                Tên mẫu
              </th>

              <th scope='col' className='border-0'>
                Tiêu đề
              </th>
              <th scope='col' className='border-0'>
                Nội dung
              </th>
              <th scope='col' className='border-0 text-center'>
                Hoạt động
              </th>
              <th scope='col' className='border-0 text-center'>
                Trạng thái
              </th>

              <th scope='col' className='border-0'>
                Hành động
              </th>
            </tr>
          </thead>

          <tbody>
            {emailList?.results?.map((item, idx) => {
              return (
                <tr key={idx}>
                  <td>{idx + 1}</td>
                  <td>{item.title}</td>
                  <td>{item.subject}</td>
                  <td>
                    {
                      <div
                        dangerouslySetInnerHTML={{
                          __html: `</br>` + item?.content + `</br>`,
                        }}
                      />
                    }
                  </td>
                  <td className='text-center'>
                    <div className={`status ${item.activate ? 'is_success' : 'is_error'}`}>
                      {item.activate ? <FaCheck /> : <FaTimes />}
                    </div>
                  </td>
                  <td className='text-center'>
                    <div className={`status ${item.status ? 'is_success' : 'is_error'}`}>
                      {item.status ? <FaCheck /> : <FaTimes />}
                    </div>
                  </td>
                  <td className='flex'>
                    <Button
                      className='btn-edit'
                      onClick={() => {
                        setData(item);
                        setShowModal(true);
                      }}
                    >
                      <FaPen />
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <Pagination
          currentPage={pagination.page}
          pageSize={pagination.perPage}
          lastPage={Math.min(
            Math.ceil((emailList?.results ? emailList?.count : 0) / pagination.perPage),
            Math.ceil((emailList?.results ? emailList?.count : 0) / pagination.perPage)
          )}
          onChangePage={pagination.setPage}
          onChangePageSize={pagination.setPerPage}
          onGoToLast={() =>
            pagination.setPage(Math.ceil((emailList?.results ? emailList?.count : 0) / pagination.perPage))
          }
        />
        {emailList?.results?.length === 0 && <Empty description='Không có dữ liệu'></Empty>}
      </div>
    </>
  );
};

export default EmailTemplates;
