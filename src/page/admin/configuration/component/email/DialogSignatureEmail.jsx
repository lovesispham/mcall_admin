import React from 'react';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useDispatch } from 'react-redux';
import Col from '../../../../../components/common/Col';
import FormInput from '../../../../../components/common/FormInput';
import { HelpText } from '../../../../../components/common/HelpText';
import Button from '../../../../../components/common/Button';
import { createEmailSignature, editEmailSignature } from '../../../../../features/config/email';
import { TextEditor } from '../../../Feed/DraftEditor';
const DialogSignatureEmail = ({ setUDO, dataEdit = null, setData }) => {
  const dispatch = useDispatch();

  const validationSchema = yup.object(
    !dataEdit && {
      signature: yup.string().required('Trường này không được bỏ trống'),
    }
  );

  const submitForm = async (values, { resetForm }) => {
    if (!dataEdit) {
      dispatch(createEmailSignature(values));
    } else {
      const newValue = {
        ...values,
        id: dataEdit.id,
      };
      dispatch(editEmailSignature(newValue));
    }
    setUDO(false);
  };
  const { values, handleSubmit, handleChange, setFieldValue, handleBlur, errors, touched, resetForm } = useFormik({
    initialValues: {
      signature: dataEdit?.signature || '',
    },
    validationSchema: validationSchema,
    onSubmit: submitForm,
  });
  return (
    <div className='content box-form'>
      <Col className={12}>
        <TextEditor setFieldValue={(val) => setFieldValue('signature', val)} value={values.signature} />
        <HelpText status='error'>{touched.signature && errors.signature ? errors.signature : ''}</HelpText>
      </Col>

      <div className='text-right'>
        <Button className='bg-theme' onClick={handleSubmit} type={'submit'} style={{ marginTop: 10 }}>
          {!dataEdit ? 'Tạo' : 'Sửa'}
        </Button>
      </div>
    </div>
  );
};

export default DialogSignatureEmail;
