import React, { useState } from 'react';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useDispatch } from 'react-redux';
import Col from '../../../../../components/common/Col';
import FormInput from '../../../../../components/common/FormInput';
import { HelpText } from '../../../../../components/common/HelpText';
import Button from '../../../../../components/common/Button';
import { createConfigEmail, editConfigEmail } from '../../../../../features/config/email';
import { showMessageSuccess } from '../../../../../features/alert/alert-message';
const DialogConfigEmail = ({ setUDO, dataEdit = null, setData }) => {
  const dispatch = useDispatch();
  const [activeEye, setActiveEye] = useState(false);
  const validationSchema = yup.object(
    !dataEdit && {
      name: yup.string().required('Trường này không được bỏ trống'),
    }
  );

  const submitForm = async (values, { resetForm }) => {
    if (dataEdit) {
      const submitValues = {
        ...values,
        unique_id: dataEdit.unique_id,
      };
      dispatch(editConfigEmail(submitValues))
        .unwrap()
        .then(() => {
          dispatch(showMessageSuccess());
        })
        .catch((err) => {});
    } else {
      dispatch(createConfigEmail(values))
        .unwrap()
        .then(() => {
          dispatch(showMessageSuccess());
        })
        .catch((err) => {});
    }
    setUDO(false);
    setData('');
  };
  const { values, handleSubmit, handleChange, setFieldValue, handleBlur, errors, touched, resetForm } = useFormik({
    initialValues: {
      name: dataEdit?.name || '',
      host: dataEdit?.host || '',
      port: dataEdit?.port || '',
      username: dataEdit?.username || '',
      password: dataEdit?.password || '',
    },
    validationSchema: validationSchema,
    onSubmit: submitForm,
  });
  return (
    <div className='content box-form'>
      <Col className={12}>
        <FormInput
          onChange={handleChange}
          value={values.name}
          name='name'
          type='text'
          label='Tên'
          placeholder='Tên'
          required
        />
        <HelpText status='error'>{touched.name && errors.name ? errors.name : ''}</HelpText>
      </Col>
      <Col className={6}>
        <FormInput
          onChange={handleChange}
          value={values.host}
          name='host'
          type='text'
          label='Host'
          placeholder='smtp.gmail.com'
        />
      </Col>
      <Col className={6}>
        <FormInput
          onChange={handleChange}
          value={values.port}
          name='port'
          type='text'
          label='Port'
          placeholder='port'
        />
      </Col>
      <Col className={6}>
        <FormInput
          onChange={handleChange}
          value={values.username}
          name='username'
          type='text'
          label='Tên đăng nhập'
          placeholder='a@gmail.com'
        />
      </Col>
      <Col className={6}>
        <FormInput
          onChange={handleChange}
          value={values.password}
          name='password'
          type='password'
          label='Mật khẩu'
          placeholder='Mật khẩu'
          active={activeEye}
          setActive={setActiveEye}
        />
      </Col>

      <div className='text-right'>
        <Button className='bg-theme' onClick={handleSubmit} type={'submit'}>
          {!dataEdit ? 'Tạo' : 'Sửa'}
        </Button>
      </div>
    </div>
  );
};

export default DialogConfigEmail;
