import React, { useEffect, useState } from 'react';
import { FaCheck, FaPen, FaTimes } from 'react-icons/fa';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import Button from '../../../../../components/common/Button';
import Empty from '../../../../../components/common/Empty';
import Modal, { ModalContent } from '../../../../../components/common/Modal';
import { getEmailSignature } from '../../../../../features/config/email';
import { CheckPermission } from '../../../../../util/help';
import DialogSignatureEmail from './DialogSignatureEmail';

const EmailSignature = ({ module }) => {
  const [showModal, setShowModal] = useState(false);
  const [data, setData] = useState('');
  const emailList = useSelector((state) => state.config.emailSignatureList);
  const status = useSelector((state) => state.config);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getEmailSignature());
  }, []);

  useEffect(() => {
    if (status.success === true) {
      dispatch(getEmailSignature());
    }
  }, [status.success]);
  return (
    <>
      <Modal active={showModal}>
        <ModalContent
          heading={!data ? 'Tạo' : 'Sửa'}
          onClose={() => {
            setShowModal(false);
          }}
        >
          <DialogSignatureEmail setData={setData} setUDO={setShowModal} dataEdit={data} />
        </ModalContent>
      </Modal>
      {CheckPermission(module, 'POST') && (
        <Button
          style={{ marginBottom: 10 }}
          className='bg-theme'
          onClick={() => setShowModal(true)}
          disabled={emailList.length > 0}
        >
          + Tạo chữ ký
        </Button>
      )}
      <div className='table-wrapper'>
        <table className='table mb-0'>
          <thead className='bg-white'>
            <tr>
              <th scope='col' className='border-0'>
                STT
              </th>

              <th scope='col' className='border-0'>
                Chữ ký
              </th>
              <th scope='col' className='border-0 text-center'>
                Hoạt động
              </th>
              <th scope='col' className='border-0'>
                Hành động
              </th>
            </tr>
          </thead>

          <tbody>
            {emailList?.map((item, idx) => {
              return (
                <tr key={idx}>
                  <td>{idx + 1}</td>
                  <td>
                    <div
                      dangerouslySetInnerHTML={{
                        __html: item?.signature,
                      }}
                    />
                  </td>

                  <td className='text-center'>
                    <div className={`status ${item.activate ? 'is_success' : 'is_error'}`}>
                      {item.activate ? <FaCheck /> : <FaTimes />}
                    </div>
                  </td>
                  <td className='flex'>
                    <Button
                      className='btn-edit'
                      onClick={() => {
                        setData(item);
                        setShowModal(true);
                      }}
                    >
                      <FaPen />
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        {emailList?.length === 0 && <Empty description='Không có dữ liệu'></Empty>}
      </div>
    </>
  );
};

export default EmailSignature;
