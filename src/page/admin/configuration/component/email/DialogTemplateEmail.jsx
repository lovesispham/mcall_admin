import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useDispatch } from 'react-redux';
import Col from '../../../../../components/common/Col';
import FormInput from '../../../../../components/common/FormInput';
import { HelpText } from '../../../../../components/common/HelpText';
import Button from '../../../../../components/common/Button';
import { createEmailTemp, editEmailTemp, getEmailSignature } from '../../../../../features/config/email';
import { TextEditor } from '../../../Feed/DraftEditor';
import FormLabel from '../../../../../components/common/FormLabel';
import { showMessageError, showMessageSuccess } from '../../../../../features/alert/alert-message';
import { useSelector } from 'react-redux';
const DialogTemplateEmail = ({ setUDO, dataEdit = null, setData }) => {
  const dispatch = useDispatch();
  const emailList = useSelector((state) => state.config.emailSignatureList);
  const [dataSig, setDataSig] = useState();

  const validationSchema = yup.object(
    !dataEdit && {
      title: yup.string().required('Trường này không được bỏ trống'),
    }
  );

  useEffect(() => {
    if (!emailList.length) {
      dispatch(getEmailSignature());
    }
  }, []);

  const submitForm = async (values, { resetForm }) => {
    if (dataEdit) {
      const submitValues = {
        ...values,
        id: dataEdit.id,
      };
      dispatch(editEmailTemp(submitValues))
        .then(() => {
          dispatch(showMessageSuccess());
        })
        .catch(() => {
          dispatch(showMessageError());
        });
      setData('');
    } else {
      dispatch(createEmailTemp(values))
        .then(() => {
          dispatch(showMessageSuccess());
        })
        .catch(() => {
          dispatch(showMessageError());
        });
    }
    setUDO(false);
  };
  const { values, handleSubmit, handleChange, setFieldValue, handleBlur, errors, touched, resetForm } = useFormik({
    initialValues: {
      title: dataEdit?.title || '',
      subject: dataEdit?.subject || '',

      content: dataEdit?.content || '',
    },
    enableReinitialize: true,
    validationSchema: validationSchema,
    onSubmit: submitForm,
  });

  return (
    <div className='content box-form'>
      <Col className={12}>
        <FormInput
          onChange={handleChange}
          value={values.title}
          name='title'
          type='text'
          label='Tên mẫu'
          placeholder='Tên mẫu'
          required
        />
        <HelpText status='error'>{touched.name && errors.name ? errors.name : ''}</HelpText>
      </Col>
      <Col className={12}>
        <FormInput
          onChange={handleChange}
          value={values.subject}
          name='subject'
          type='text'
          label='Tiêu đề'
          placeholder='Tiêu đề'
        />
      </Col>

      <Col className={12}>
        <FormLabel label={'Nội dung'} required />
        <TextEditor setFieldValue={(val) => setFieldValue('content', val)} value={values.content} signature={dataSig} />
      </Col>
      <Col className={12} style={{ margin: '10px 0' }}>
        <Button
          className='bg-theme'
          disabled={dataSig && dataSig.length}
          onClick={() => {
            setDataSig(emailList[0]?.signature);
          }}
        >
          Sử dụng chữ ký
        </Button>
      </Col>

      <div className='text-right'>
        <Button className='bg-theme' onClick={handleSubmit} type={'submit'}>
          {!dataEdit ? 'Tạo' : 'Sửa'}
        </Button>
      </div>
    </div>
  );
};

export default DialogTemplateEmail;
