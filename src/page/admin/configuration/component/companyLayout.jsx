import React, { useEffect ,useState} from "react";
import { Outlet, useParams } from "react-router-dom";

import { Link, useLocation } from "react-router-dom";
import Button from '../../../../components/common/Button';
import DialogAddCompany from './company/diglogAddCompary'
import DiglogAddAdmin from './company/diglogAddAdmin';
const CompanyLayout = () => {
  const { pathname } = useLocation();
  const { outletComponentId } = useParams();
  const [activeCompany, setActiveCompany] = useState(false);
  const [activeAddAdmin, setActiveAddAdmin] = useState(false);
  const checkMatch = (pathname) => {
    if ((pathname && pathname.split("/")[3]) === "configuration") {
      return CompanyTabs;
    }
  };

  const activeNav = checkMatch(pathname)?.findIndex((e) => e.path === pathname);
  useEffect(() => {
    checkMatch(pathname);
  }, [pathname]);
  return (
    <>
      {checkMatch(pathname) && (
        <div style={{ paddingTop: '20px' }}>
      {activeCompany && <DialogAddCompany setActiveCompany={setActiveCompany} editCompany={editCompany} />}
      {activeAddAdmin && (
        <DiglogAddAdmin setActiveAddAdmin={setActiveAddAdmin} addSuperAdmin={addSuperAdmin} idCompany={idCompany} />
      )}
  
      <div style={{ paddingRight: '20px' }}>
        <div style={{ display: 'flex', gap: '2px' }}>
          <Button
            style={{ marginBottom: 10 }}
            className='bg-theme'
            onClick={() => {
              setActiveCompany(true);
              setEditCompany();
            }}
          >
            + Tạo Công Ty
          </Button>
        </div>
      </div>
          <div className="box-tabs">
            <div className="tabs-list" style={{ margin: 0 }}>
              {checkMatch(pathname).map((v, i) => (
                <Link
                  key={i}
                  to={v.path}
                  className={`tab-item ${i === activeNav ? "active" : ""}`}
                >
                  <span>{v?.name?.toUpperCase()}</span>
                </Link>
              ))}
            </div>
          </div>

          <div className="box-form-general">
            <Outlet />
          </div>
        </div>
      )}
    </>
  );
};

export default CompanyLayout;

export const CompanyTabs = [
  {
    name: "DANH SÁCH CÔNG TY",
    key: 1,
    path: "/admin/configuration/company",
  },
  {
    name: "DANH SÁCH ĐẠI LÝ",
    key: 2,
    path: "/admin/configuration/authorize",
  },
];
