import React, { useState } from 'react';
import Heading from '../../../../components/common/Heading';
import { Section } from '../../../../components/common/Section';
import EmailConfig from './email/email-config';
import EmailSignature from './email/email-signature';
import EmailTemplates from './email/email-template';

const Email = ({ module }) => {
  const [activeTab, setActiveTab] = useState(0);
  const emailConfigTabs = [
    {
      name: 'Cấu hình email',
      key: 0,
    },
    {
      name: 'Chữ ký',
      key: 1,
    },
    {
      name: 'Mẫu email',
      key: 2,
    },
  ];
  const dataTab = [
    <EmailConfig module={module} />,
    <EmailSignature module={module} />,
    <EmailTemplates module={module} />,
  ];
  return (
    <Section className='box-general'>
      <Heading className='heading' type='h2'>
        Email
      </Heading>
      <div className='box-tabs'>
        <div className='tabs-list' style={{ margin: 0 }}>
          {emailConfigTabs.map((v, i) => (
            <div
              key={i}
              className={`tab-item ${activeTab === v.key ? 'active' : ''}`}
              onClick={() => {
                setActiveTab(v.key);
              }}
            >
              <p>{v.name.toUpperCase()}</p>
            </div>
          ))}
        </div>
      </div>

      <div className='box-form-general'>{dataTab[activeTab]}</div>
    </Section>
  );
};

export default Email;
