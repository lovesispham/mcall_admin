import React, { useState } from "react";
import { AiOutlineClose } from "react-icons/ai";
import { useDispatch } from "react-redux";
import Button from "../../../../../components/common/Button";
import Col from "../../../../../components/common/Col";
import FormInput from "../../../../../components/common/FormInput";
import {
  showMessageError,
  showMessageSuccess,
} from "../../../../../features/alert/alert-message";
import { postorgAdminApi } from "../../../../../features/org/orgSlice";

const DiglogAddAdmin = ({ setActiveAddAdmin, idCompany, addSuperAdmin, dataEdit }) => {
  const dispatch = useDispatch();
  const [activeEye, setActiveEye] = useState(false);
  const [data, setData] = useState({});
  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleCreate = () => {
   
    dispatch(postorgAdminApi({ ...data, id: idCompany || dataEdit?.id, username: data?.email }))
      .unwrap()
      .then((res) => {
        dispatch(showMessageSuccess("Tạo Tài khoản Thành Công"));
      })
      .catch((err) => {
        dispatch(showMessageError(err?.data?.errors?.email[0]));
      });
    setActiveAddAdmin(false);
  };
  return (
    <div className="content">
      <Col className={6}>
        <FormInput
          type="text"
          placeholder="Email"
          label={"Email"}
          name="email"
          onChange={handleChange}
        />
      </Col>

      <Col className={6}>
        <FormInput
          label={"Mật Khẩu"}
          placeholder="Mật Khẩu"
          name="password"
          type="password"
          onChange={handleChange}
          active={activeEye}
                setActive={setActiveEye}
        />
      </Col>
      <Col className={6}>
        <FormInput
          type="text"
          label={"Họ"}
          placeholder={"Họ"}
          name="first_name"
          onChange={handleChange}
        />
      </Col>
      <Col className={6}>
        <FormInput
          label={"Tên"}
          placeholder={"Tên"}
          type="text"
          onChange={handleChange}
          name="last_name"
        />
      </Col>
      <Col className={12}>
        <div className="text-right">
          <Button className="bg-theme" onClick={handleCreate}>
            Lưu
          </Button>
        </div>
      </Col>
    </div>
  );
};

export default DiglogAddAdmin;
