import React, { useEffect, useState } from "react";

import { useDispatch, useSelector } from "react-redux";

import { getModules } from "../../../../../features/module/moduleSlice";
import { postOrgApi, putOrgapi } from "../../../../../features/org/orgSlice";
import { selectProfile } from "../../../../../features/user/userSlice";
import {
  showMessageError,
  showMessageSuccess,
} from "../../../../../features/alert/alert-message";
import Button from "../../../../../components/common/Button";
import FormInput from "../../../../../components/common/FormInput";
import TextArea from "../../../../../components/common/TextArea";
import FormCheckBox from "../../../../../components/common/FormCheckBox";
import Col from "../../../../../components/common/Col";
import FormLabel from "../../../../../components/common/FormLabel";

const DiglogAddCompary = ({ setActiveCompany, editCompany }) => {
  const dispatch = useDispatch();
  const [data, setData] = useState({});
  const [moduleList, setModuleList] = useState([]);
  const [listModule, setListModule] = useState();
  const [authorizedDealer, setAuthorizedDealer] = useState();
  const [active, setActive] = useState(true);
  useEffect(() => {
    dispatch(getModules())
      .unwrap()
      .then((res) => {
        setListModule(res?.modules);
      });
    // eslint-disable-next-line
  }, []);
  useEffect(() => {
    setModuleList(editCompany ? editCompany?.has_access.map((v) => v.id) : []);
    // eslint-disable-next-line
  }, []);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };
  const handleChangeCheckBox = async (e) => {
    // const arr =[ moduleList.push(2);]
    if (e.currentTarget.checked) {
      setModuleList([...moduleList, parseInt(e.target.name)]);
      setActive(!active);
    } else {
      const arr = await moduleList;

      arr?.splice(moduleList.indexOf(parseInt(e.target.name)), 1);
      await setModuleList(arr);
      setActive(!active);
    }
  };

  const handleCreate = () => {
    if (editCompany) {
      dispatch(
        putOrgapi({
          name: editCompany?.name,
          address: editCompany?.address,
          phone: editCompany?.phone,
          code: editCompany?.code,
          agency: editCompany?.agency,
          module: [...moduleList, 8, 12],
          ...data,
          id: editCompany?.id,
        })
      )
        .unwrap()
        .then((res) => {
          dispatch(
            showMessageSuccess(
              authorizedDealer
                ? "Sửa Đại Lý Thành Công"
                : "Sửa Công Ty Thành Công"
            )
          );
        })
        .catch((err) => {
          dispatch(
            showMessageError(
              authorizedDealer ? "Sửa Đại Lý Thất Bại" : "Sửa Công Ty Thất Bại"
            )
          );
        });
      setActiveCompany(false);
    } else {
      dispatch(
        postOrgApi({
          ...data,
          module: [
            ...moduleList,
            listModule.find((v) => v.code === "DASHBOARD").id,
            listModule.find((v) => v.code === "CONF").id,
          ],
          agency: authorizedDealer,
        })
      )
        .unwrap()
        .then((res) => {
          dispatch(
            showMessageSuccess(
              authorizedDealer
                ? "Thêm Đại Lý Thành Công"
                : "Thêm Công Ty Thành Công"
            )
          );
        })
        .catch((err) => {
          dispatch(
            showMessageError(
              authorizedDealer
                ? "Thêm Đại Lý Thất Bại"
                : "Thêm Công Ty Thất Bại"
            )
          );
        });
      setActiveCompany(false);
    }
  };

  const handleChangeDealer = (e) => {
    setAuthorizedDealer(e.currentTarget.checked);
  };
  // const handleActive = (e) => {
  //   setIs_active(e.target.checked);
  // };
  return (
    <div className="content">
      <Col className={6}>
        <FormInput
                type='text'

          placeholder="Tên công ty"
          label={"Tên công ty"}
          name="name"
          value={data?.name === undefined ? editCompany?.name : data?.name}
          onChange={handleChange}
        />
      </Col>
      <Col className={6}>
        <FormInput
                type='text'

          placeholder="Số điện thoại"
          label={"Số điện thoại"}
          name="phone"
          value={data?.phone === undefined ? editCompany?.phone : data?.phone}
          onChange={handleChange}
        />
      </Col>
      <Col className={12}>
        <TextArea
          rows={5}
          
          label={"Địa chỉ"}
          name="address"
          value={
            data?.address === undefined ? editCompany?.address : data?.address
          }
          onChange={handleChange}
        />
      </Col>

      <Col className={12}>
        <FormCheckBox
          pos="left"
          label={"Đại Lý"}
          name={`authorizedDealer`}
          onChange={handleChangeDealer}
          checked={data?.authorizedDealer || editCompany?.agency || authorizedDealer}
        />
      </Col>
      <Col className={12}>
          
          <FormLabel label={'Chọn Module Cho Công Ty này'} />
        
        {listModule?.map((v, i) => (
          <>
            {v?.code !== "DASHBOARD" && v?.code !== "CONF" && (
              <Col className={3} index={i} style={{padding: '0 !important'}}>
              <FormCheckBox
              pos="left"
              label={v?.name}
              checked={moduleList.includes(v?.id)}
                  style={{ border: "1px solid black" }}
                  onChange={handleChangeCheckBox}
                  name={v?.id}
            />
            </Col>

            )}
          </>
        ))}
          
       
      </Col>
      <Col className={12}>
        <div className="text-right">
          <Button className="bg-theme" onClick={handleCreate}>
            LƯU
          </Button>
        </div>
      </Col>
    </div>
  );
};

export default DiglogAddCompary;
