import moment from "moment";
import React, { useEffect, useState } from "react";
import {
  AiOutlineEdit,
  AiOutlineSlack,
  AiOutlineUsergroupAdd,
} from "react-icons/ai";
import { useDispatch, useSelector } from "react-redux";
import Button from "../../../../components/common/Button";
import DialogAddCompany from "./company/diglogAddCompary";
import DiglogAddAdmin from "./company/diglogAddAdmin";
import {
  showMessageError,
  showMessageSuccess,
} from "../../../../features/alert/alert-message";
import { selectProfile } from "../../../../features/user/userSlice";
import { getOrgApi, putOrgapi } from "../../../../features/org/orgSlice";
import Modal, { ModalContent } from "../../../../components/common/Modal";

const CompanyAgent = ({ module }) => {
  const dispatch = useDispatch();
  const [listOrg, setListOrg] = useState([]);

  const [activeCompany, setActiveCompany] = useState(false);
  const [activeAddAdmin, setActiveAddAdmin] = useState(false);
  const [idCompany, setIdCompany] = useState();
  const [addSuperAdmin, setAddSuperAdmin] = useState(false);
  const profile = useSelector(selectProfile);
  const [key, setKey] = useState("all");
  const [editCompany, setEditCompany] = useState();
  const [active, setActive] = useState(false);
  const [number, setNumber] = useState([]);
  const SettingAll = [
    { key: "all", name: "Danh sách công ty" },
    { key: "authorized", name: "Danh sách đại lý" },
  ];
  useEffect(() => {
    const arr = listOrg?.filter((k) => k?.agency === false)?.map((v, i) => 0);
    setNumber(arr);
  }, []);

  useEffect(() => {
    !activeCompany &&
      dispatch(getOrgApi({}))
        .unwrap()
        .then((res) => {
          setListOrg(res?.data?.list_org);
        });
  }, [activeCompany, active]);

  const addadmin = (id) => {
    setActiveAddAdmin(true);
    setIdCompany(id);
  };

  const handleActiveCompany = (value, i) => {
    const arr = number;

    arr[i] = value?.is_active ? number[i] + 360 : number[i] - 360;

    setNumber(arr);

    dispatch(
      putOrgapi({
        id: value?.id,
        is_active: !value?.is_active,
      })
    )
      .unwrap()
      .then((res) => {
        dispatch(
          showMessageSuccess(
            !value?.is_active ? " Đã kích hoạt" : "Đã ngưng hoạt động"
          )
        );
        setActive(!active);
      })
      .catch((err) => {
        dispatch(showMessageError(" thất bại"));
      });
  };

  return (
    <div style={{ paddingTop: "20px" }}>
      <Modal active={activeCompany}>
        <ModalContent
          className="pu-company"
          heading={!editCompany ? "Tạo công ty" : "Sửa công ty"}
          onClose={() => {
            setActiveCompany(false);
          }}
        >
          <DialogAddCompany
            setActiveCompany={setActiveCompany}
            editCompany={editCompany}
          />
        </ModalContent>
      </Modal>

      <Modal active={activeAddAdmin}>
        <ModalContent
          className="pu-company"
          heading={`Tạo ${
            addSuperAdmin ? "superAdmin cho công ty" : "admin cho công ty"
          }`}
          onClose={() => {
            setActiveAddAdmin(false);
          }}
        >
          <DiglogAddAdmin
            setActiveAddAdmin={setActiveAddAdmin}
            addSuperAdmin={addSuperAdmin}
            idCompany={idCompany}
          />
        </ModalContent>
      </Modal>

      <div style={{ paddingRight: "20px" }}>
        <div style={{ display: "flex", gap: "2px" }}>
          <Button
            style={{ marginBottom: 10 }}
            className="bg-theme"
            onClick={() => {
              setActiveCompany(true);
              setEditCompany();
            }}
          >
            + Tạo Công Ty
          </Button>
        </div>
      </div>

      <div className="box-tabs">
        <div className="tabs-list" style={{ margin: 0 }}>
          {SettingAll.map((v, i) => (
            <div
              key={i}
              className={`tab-item ${v.key === key ? "active" : ""}`}
              onClick={() => {
                setKey(v.key);
              }}
            >
              <p>{v?.name?.toUpperCase()}</p>
            </div>
          ))}
        </div>
      </div>

      <div className="box-form-general">
        {key === "all" && (
          <div className="table-wrapper">
            <table className="table mb-0">
              <thead className="bg-white">
                <tr>
                  <th style={{ paddingLeft: "10px", paddingTop: "10px" }}></th>
                  <th>Tên công ty</th>
                  <th style={{ width: "200px", paddingTop: "8px" }}>Địa chỉ</th>
                  <th>Số điện thoại</th>
                  <th>Ngày thành lập</th>
                  <th>Trạng thái</th>
                  <th>Hành động</th>
                </tr>
              </thead>
              <tbody>
                {listOrg
                  ?.filter((k) => k?.agency === false)
                  ?.map((v, i) => (
                    <tr
                      style={{
                        height: "30px",
                        backgroundColor: v?.is_active ? "white" : "#EEEEEE",
                        transition: "all 2s ease",
                      }}
                      key={i}
                    >
                      <td
                        style={{
                          display: "flex",
                          justifyContent: "center",
                          fontSize: "20px",
                          alignItems: "center",
                          height: "100%",
                        }}
                      ></td>
                      <td>{v?.name}</td>
                      <td>{v?.address}</td>
                      <td>{v?.phone}</td>
                      <td>{moment(v.created_on).format("DD-MM-YYYY")}</td>
                      <td>
                        <p
                          style={{
                            color: v?.is_active ? "#34c38f" : "#FF0000",
                            transition: "all 2s ease",
                            fontWeight: 600,
                          }}
                        >
                          {" "}
                          {v?.is_active ? "Đang" : "Ngưng"} hoạt động
                        </p>
                      </td>
                      <td
                        style={{
                          display: "flex",
                          gap: "2px",
                          alignItems: "center",
                          paddingTop: "10px",
                        }}
                      >
                        <AiOutlineEdit
                          data-tip={`Sửa Thông Tin Công Ty  ${v?.name}`}
                          style={{
                            color: "#FF9900",
                            fontSize: "20px",
                            cursor: "pointer",
                            opacity: v?.is_active || 0,
                            // display: v?.is_active || 'none',
                            transition: "all 2s ease",
                          }}
                          onClick={() => {
                            setActiveCompany(true);
                            setEditCompany(v);
                          }}
                        />

                        <AiOutlineUsergroupAdd
                          data-tip={`Thêm Tài Khoản ADMIN ${v?.name}`}
                          style={{
                            color: "#FF0000",
                            fontSize: "20px",
                            cursor: "pointer",
                            opacity: v?.is_active || 0,
                            // display: v?.is_active || 'none',
                            transition: "all 2s ease",
                          }}
                          onClick={() => {
                            addadmin(v?.id);
                            setAddSuperAdmin(false);
                          }}
                        />

                        <AiOutlineSlack
                          data-tip={` ${
                            v?.is_active ? "Ngưng" : "Kích"
                          }  hoạt công ty ${v?.name}`}
                          style={{
                            color: !v?.is_active ? "#FF0000" : "",
                            fontSize: "20px",
                            cursor: "pointer",
                            transition: "all 2s ease",
                            transform: `rotate(${number[i]}deg)`,
                          }}
                          onClick={() => {
                            handleActiveCompany(v, i);
                          }}
                        />
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        )}
        {key === "authorized" && (
          <div className="table-wrapper">
            <table className="table mb-0">
              <thead className="bg-white">
                <tr>
                  <th style={{ paddingLeft: "10px" }}></th>
                  <th>Tên đại lý</th>
                  <th style={{ width: "200px", paddingTop: "8px" }}>Địa chỉ</th>
                  <th>Số điện thoại</th>
                  <th>Ngày thành lập</th>
                  <th>Trạng thái</th>
                  <th>Hành động</th>
                </tr>
              </thead>
              <tbody>
                {listOrg
                  ?.filter((v) => v?.agency === true)
                  ?.map((v, i) => (
                    <tr
                      style={{
                        height: "30px",
                        backgroundColor: v?.is_active ? "white" : "#EEEEEE",
                      }}
                      key={i}
                    >
                      <td
                        style={{
                          display: "flex",
                          justifyContent: "center",
                          fontSize: "20px",
                          alignItems: "center",
                          height: "100%",
                        }}
                      ></td>
                      <td>{v?.name}</td>
                      <td>{v?.address}</td>
                      <td>{v?.phone}</td>
                      <td>{moment(v.created_on).format("DD-MM-YYYY")}</td>

                      <td>
                        {v?.is_active ? (
                          <p style={{ color: "#34c38f", fontWeight: 600 }}>
                            Đang hoạt động
                          </p>
                        ) : (
                          <p style={{ color: "#FF0000" }}>Ngừng hoạt Động</p>
                        )}
                      </td>

                      <td
                        style={{
                          display: "flex",
                          gap: "2px",
                          marginTop: "10px",
                        }}
                      >
                        {v?.is_active && (
                          <AiOutlineEdit
                            data-tip={`Sửa Thông Tin Đại Lý  ${v?.name}`}
                            style={{
                              color: "#FF9900",
                              fontSize: "20px",
                              cursor: "pointer",
                            }}
                            onClick={() => {
                              setActiveCompany(true);
                              setEditCompany(v);
                            }}
                          />
                        )}
                        {v?.is_active && (
                          <AiOutlineUsergroupAdd
                            data-tip={`Thêm Tài Khoản SUPERADMIN  ${v?.name}`}
                            style={{
                              color: "#FF0000",
                              fontSize: "20px",
                              cursor: "pointer",
                            }}
                            onClick={() => {
                              addadmin(v?.id);
                              setAddSuperAdmin(true);
                            }}
                          />
                        )}

                        <AiOutlineSlack
                          data-tip={` ${
                            v?.is_active ? "Ngưng" : "Kích"
                          }  hoạt công ty ${v?.name}`}
                          style={{
                            color: !v?.is_active ? "#FF0000" : "",
                            fontSize: "20px",
                            cursor: "pointer",
                            transition: "all 2s ease",
                            transform: `rotate(${number[i]}deg)`,
                          }}
                          onClick={() => {
                            handleActiveCompany(v, i);
                          }}
                        />
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        )}
      </div>
    </div>
  );
};

export default CompanyAgent;
