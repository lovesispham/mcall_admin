import Select from 'react-select';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAuthUserApi, getExtensionApi, putUserPhoneApi } from '../../../../../features/common/commonThunk';
import { selectProfile } from '../../../../../features/user/userSlice';
import { styles } from '../../../../../scss/cssCustoms';
import { showMessageError, showMessageSuccess } from '../../../../../features/alert/alert-message';

const Decentralization = () => {
  const dispatch = useDispatch();
  const profile = useSelector(selectProfile);
  const [listUser, setListUser] = useState([]);
  const [listPhone, setListPhone] = useState([]);
  const [labelPhone, setLablePhone] = useState([]);
  const [active, setActive] = useState(true);

  useEffect(() => {
    console.log('huu thin');
    dispatch(getExtensionApi())
      .unwrap()
      .then((res) => {
        setListPhone(
          [{ idPhone: null, label: 'Hủy máy lẻ' }]
            .concat(
              res?.Agent?.Agent?.map(
                (e) =>
                  e.webrtc_websocket && {
                    idPhone: e?.id,
                    label: e?.extension,
                  }
              )
            )
            .filter((v) => v)
        );
      })
      .catch((err) => {});
  }, []);

  useEffect(() => {
    dispatch(getAuthUserApi())
      .unwrap()
      .then((res) => {
        setListUser(res?.active_users?.active_users);
      })
      .catch((err) => {});
  }, []);

  const handleChange = (value) => {
    const arr = labelPhone;
    arr[value?.id] = value.label;
    setActive(!active);
    setLablePhone(arr);
    dispatch(putUserPhoneApi({ id: value.id, email: value?.email, agent: value.idPhone }))
      .unwrap()
      .then((res) => {
        dispatch(showMessageSuccess('Gắn Tài Khoản Thành Công'));
      })
      .catch((err) => {
        dispatch(showMessageError('Găn Tài Khoản Thất Bài'));
      });
  };

  // const lableName = listPhone?.find((h) => (h.idPhone = v?.agent?.id))?.label

  return (
    <div style={{ paddingRight: '20px' }}>
      <div className='table-wrapper'>
        <table className='table mb-0'>
          <thead className='bg-white'>
            <tr>
              <th scope='col' className='border-0'>
                Id
              </th>

              <th scope='col' className='border-0'>
                UserName
              </th>
              <th scope='col' className='border-0'>
                Quyền
              </th>
              <th scope='col' className='border-0'>
                Email
              </th>
              <th scope='col' className='border-0'>
                Máy lẻ
              </th>
            </tr>
          </thead>
          <tbody>
            {listUser?.map((v, i) => (
              <tr key={i}>
                <td>{v.id}</td>
                <td>
                  {v?.last_name} {v?.first_name}
                </td>
                <td>{v?.role}</td>
                <td>{v?.email}</td>

                <td style={{ paddingLeft: '10px' }}>
                  {' '}
                  <Select
                    styles={{ ...styles }}
                    menuPortalTarget={document.body}
                    menuPosition={'fixed'}
                    menuPlacement='bottom'
                    onChange={handleChange}
                    name='colors'
                    value={[
                      {
                        label: labelPhone[v?.id] || listPhone?.find((h) => h.idPhone === v?.agent?.id)?.label,
                        value: labelPhone[v?.id] || listPhone?.find((h) => h.idPhone === v?.agent?.id)?.label,
                      },
                    ]}
                    options={listPhone.map((k) => ({
                      ...k,
                      email: v.email,
                      id: v.id,
                    }))}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Decentralization;
