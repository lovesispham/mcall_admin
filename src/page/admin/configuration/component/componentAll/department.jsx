import Toast from '../../../../../components/common/Toast';

import moment from 'moment/moment';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import {
  getDetailCommonApi,
  getPositionsApi,
  postDepartmentsApi,
  postPositionsApi,
  PutDepartmentsApi,
} from '../../../../../features/common/commonThunk';
import { CssButton, cssInput } from '../../../../../scss/cssCustoms';
import InformationReceipt from '../../../Receipt/component/informationReceipt';
import Diglogposition from '../../../../../components/componentConfiguration/diglogposition';
import { AiOutlineEdit } from 'react-icons/ai';
import ReactTooltip from 'react-tooltip';
import { showMessageError, showMessageSuccess } from '../../../../../features/alert/alert-message';
import Button from '../../../../../components/common/Button';
import { FaCheck, FaPen, FaPlusCircle, FaTimes, FaTimesCircle } from 'react-icons/fa';
import FormInput from '../../../../../components/common/FormInput';

const Department = () => {
  const dispatch = useDispatch();
  const [active, setActive] = useState([]);
  const [listDepartment, setListDepartment] = useState([]);
  const [listPositions, setListPositions] = useState([]);
  const [number, setNumber] = useState(null);
  const [mes, setMes] = useState('');
  const [dataDepartments, setDataDepartments] = useState({});
  const [activeAddDepartment, setActiveAddDepartment] = useState(false);
  const [action, setAction] = useState();
  const [activePosition, setActivePosition] = useState(false);
  const [idPosition, setIdPosition] = useState();
  const [id, setId] = useState();
  const [edit, setEdit] = useState();
  const [editPosition, setEditPosition] = useState();

  const date = new Date();

  useEffect(() => {
    dispatch(getDetailCommonApi())
      .unwrap()
      .then((res) => {
        setListDepartment(res?.data);
      });
  }, []);

  useEffect(() => {
    !dataDepartments &&
      dispatch(getDetailCommonApi())
        .unwrap()
        .then((res) => {
          setListDepartment(res?.data);
        });
    if (mes === '') return;
    setMes('mes');
  }, [!dataDepartments]);

  useEffect(() => {
    const arr = listDepartment?.map((v) => false);

    setActive(arr);
  }, [listDepartment]);

  const addDepartment = () => {
    if (edit) {
      dispatch(PutDepartmentsApi({ id: edit?.id, name: edit?.name, ...dataDepartments }))
        .unwrap()
        .then((res) => {
          setActiveAddDepartment(false);
          setDataDepartments('');
          setEdit();
          dispatch(showMessageSuccess('Sửa phong ban thành công'));
        })
        .catch((err) => {
          dispatch(showMessageError('Sửa phòng ban thất bại '));
        });
    } else {
      dispatch(postDepartmentsApi(dataDepartments))
        .unwrap()
        .then((res) => {
          setEdit();
          setActiveAddDepartment(false);
          setDataDepartments('');
          dispatch(showMessageSuccess('Thêm phòng ban thành công'));
        })
        .catch((err) => {
          dispatch(showMessageError('Thêm Phòng ban thất bại '));
        });
    }
  };

  const submitPositions = (id) => {
    setId(id);
    dispatch(getPositionsApi({ id: id }))
      .unwrap()
      .then((res) => {
        setListPositions(res?.data);
      });
  };
  useEffect(() => {
    !activePosition &&
      id &&
      dispatch(getPositionsApi({ id: id }))
        .unwrap()
        .then((res) => {
          setListPositions(res?.data);
        });
  }, [activePosition]);

  const handleDepartments = (e) => {
    setDataDepartments({ ...dataDepartments, [e.target.name]: e.target.value });
  };

  return (
    <div style={{ paddingRight: '20px' }}>
      {activePosition && (
        <Diglogposition setActivePosition={setActivePosition} id={idPosition} editPosition={editPosition} />
      )}
      <div style={{ display: 'flex', gap: '2px' }}>
        <Button
          style={{ marginBottom: 10 }}
          className='bg-theme'
          onClick={() => {
            setActiveAddDepartment(true);
          }}
        >
          + Thêm Phòng
        </Button>
      </div>

      <div className='table-wrapper'>
        <table className='table mb-0'>
          <thead className='bg-white'>
            <tr>
              <th scope='col' className='border-0 text-center'></th>

              <th scope='col' className='border-0'>
                Tên Phòng Ban
              </th>

              <th scope='col' className='border-0'>
                Ngày Tạo
              </th>

              <th scope='col' className='border-0'>
                Hành động
              </th>
            </tr>
          </thead>
          <tbody>
            {/* thêm department */}
            {activeAddDepartment && (
              <tr>
                <td></td>
                <td style={{ paddingRight: '20px' }}>
                  <FormInput
                    name='name'
                    value={dataDepartments?.name === undefined ? edit?.name || '' : dataDepartments?.name}
                    onChange={handleDepartments}
                  />
                </td>
                {/* <td>{moment(v?.created_on).format('DD-MM-YYYY')}</td> */}
                <td>{/* {moment(date).format('DD-MM-YYYY')} */}</td>
                <td className='flex'>
                  <Button
                    className='bg-success'
                    onClick={addDepartment}
                    style={{
                      width: 60,
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <FaCheck style={{ marginRight: 5 }} />
                    {edit ? 'Sửa' : 'Thêm'}
                  </Button>
                  <Button
                    className='bg-error'
                    style={{
                      width: 60,
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    onClick={() => {
                      setEdit({});
                      setActiveAddDepartment(false);
                    }}
                  >
                    <FaTimes style={{ marginRight: 5 }} />
                    hủy
                  </Button>
                </td>
              </tr>
            )}
            {/* list department */}
            {listDepartment?.map((v, i) => (
              <>
                <tr style={{ height: '30px', backgroundColor: 'white', cursor: 'pointer' }}>
                  <td
                    data-tip={`Xem thông tin chức vụ phòng ${v?.name}`}
                    style={{
                      display: 'flex',
                      justifyContent: 'center',
                      fontSize: '20px',
                      alignItems: 'center',
                      height: '100%',
                      cursor: 'pointer',
                    }}
                    onClick={() => {
                      setActiveAddDepartment(false);
                      submitPositions(v.id);

                      setNumber(i);
                      setAction(active[i]);
                      const arr = [...Array(4)].map((v) => false);
                      if (number === i) {
                        arr[i] = action;
                      } else {
                        arr[i] = true;
                      }

                      setActive(arr);
                    }}
                  >
                    <FaPlusCircle />
                    <ReactTooltip />
                  </td>
                  <td>{v?.name}</td>
                  <td>{moment(v?.created_on).format('DD-MM-YYYY')}</td>
                  {/* <td>hh</td> */}
                  <td>
                    <Button
                      data-tip={`Sửa Phòng Ban ${v?.name}`}
                      className='btn-edit'
                      onClick={() => {
                        setActiveAddDepartment(true);
                        setEdit(v);
                        setDataDepartments({});
                      }}
                    >
                      <FaPen />
                    </Button>
                    <ReactTooltip />
                  </td>
                </tr>
                {/*  */}
                {active[i] && (
                  <tr>
                    {' '}
                    <td colSpan={4} style={{ paddingLeft: '130px' }}>
                      <table className='table mb-0' style={{ border: '1px solid #ebebeb' }}>
                        <thead className='text-xs text-gray-700 uppercase border-b' style={{ padding: '10px' }}>
                          <tr>
                            <th>Tên Chức Vụ</th>
                            <th>Ngày Tạo</th>
                            <th>
                              {' '}
                              <Button
                                data-tip={`Thêm một chức vụ mới cho phòng ${v?.name}`}
                                className='bg-theme'
                                onClick={() => {
                                  setActivePosition(true);
                                  setIdPosition(v?.id);
                                }}
                              >
                                {' '}
                                + Thêm <ReactTooltip />
                              </Button>{' '}
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {listPositions?.map((v, i) => (
                            <>
                              <tr style={{ backgroundColor: i % 2 === 0 ? 'white' : '#EEEEEE' }}>
                                <td>{v.name}</td>
                                <td>{moment(v?.created_on).format('DD-MM-YYYY')}</td>

                                <td>
                                  <Button
                                    data-tip={`Sửa Chức Vụ ${v.name}`}
                                    className='btn-edit'
                                    onClick={() => {
                                      setEditPosition(v);
                                      setActivePosition(true);
                                    }}
                                  >
                                    <FaPen />
                                  </Button>
                                  <ReactTooltip />
                                </td>
                              </tr>
                            </>
                          ))}
                        </tbody>
                      </table>
                    </td>
                  </tr>
                )}
                {/*  */}
              </>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Department;

export const Cssdepartment = {
  backgroundColor: '#FF9933',
  height: '20px',
  lineHeight: '20px',
  width: '80px',
  borderRadius: '5px',
  fontSize: '11px',
  color: 'white',
  marginTop: '5px',
  marginBottom: '5px',
};
