import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Button from '../../../../../components/common/Button';
import FormInput from '../../../../../components/common/FormInput';
import { Section } from '../../../../../components/common/Section';
import TextArea from '../../../../../components/common/TextArea';
import { showMessageError, showMessageSuccess } from '../../../../../features/alert/alert-message';
import { putOrgapi } from '../../../../../features/common/commonThunk';
import { selectProfile } from '../../../../../features/user/userSlice';

const InfomationAll = () => {
  const dispatch = useDispatch();

  const [data, setData] = useState({});

  const listuserName = useSelector(selectProfile);

  const handleChane = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const updateInfomation = () => {
    dispatch(putOrgapi({ ...data, id: listuserName?.org?.id }))
      .unwrap()
      .then((res) => {
        dispatch(showMessageSuccess('Cập nhật thông tin công ty thành công'));
      })
      .catch((err) => {
        dispatch(showMessageError('Cập nhật thông tin công ty thất bại '));
      });
  };
  return (
    <Section className='general-info' style={{ maxWidth: 492 }}>
      <FormInput
        label='Tên Công Ty :'
        name='name'
        onChange={handleChane}
        value={data?.name || listuserName?.org?.name}
      />
      <FormInput label='Email :' name='email' onChange={handleChane} value={data?.email || listuserName?.org?.email} />
      <FormInput
        label='Số Điện Thoại :'
        name='phone'
        onChange={handleChane}
        value={data?.phone || listuserName?.org?.phone}
      />

      <FormInput
        label='Địa Chỉ :'
        name='address'
        onChange={handleChane}
        value={data?.address || listuserName?.org?.address}
      />

      <TextArea name='describe' value={data?.describe} onChange={handleChane} label='Mô Tả :' />

      <div className='text-right'>
        <Button className='bg-theme' onClick={updateInfomation}>
          Cập Nhật
        </Button>
      </div>
    </Section>
  );
};

export default InfomationAll;

export const cssAll = {
  backgroundColor: 'white',
  marginBottom: '10px',
  width: '40%',
  marginLeft: '5px',
};
