import React, { useEffect, useState } from 'react';
import { FaPen } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import ReactTooltip from 'react-tooltip';
import Button from '../../../../../components/common/Button';
import Modal, { ModalContent } from '../../../../../components/common/Modal';
import DiglogAddStaff from '../../../../../components/componentConfiguration/diglogAddStaff';
import { getUserStaffApi } from '../../../../../features/common/commonThunk';
import { selectProfile } from '../../../../../features/user/userSlice';

import { CheckPermission } from '../../../../../util/help';

const Staff = ({ module }) => {
  const dispatch = useDispatch();
  const [listUser, setListuser] = useState();
  const [adddStaff, setAddStaff] = useState(false);
  const [idStaff, setIdStaff] = useState();
  const profile = useSelector(selectProfile);
  const roleAction = CheckPermission(module, 'POST');
  useEffect(() => {
    !adddStaff &&
      dispatch(getUserStaffApi({}))
        .unwrap()
        .then((res) => {
          setListuser(res?.active_users?.active_users);
        });
  }, [adddStaff]);

  return (
    <div style={{ paddingRight: '20px' }}>
      <Modal active={adddStaff}>
        <ModalContent
          heading={idStaff ? 'Sửa' : 'Tạo'}
          onClose={() => {
            setAddStaff(false);
            setIdStaff();
          }}
        >
          <DiglogAddStaff setAddStaff={setAddStaff} idStaff={idStaff} />
        </ModalContent>
      </Modal>
      {CheckPermission(module, 'POST') && profile.role !== 'USER' && (
        <div style={{ display: 'flex', gap: '2px' }}>
          <Button
            style={{ marginBottom: 10 }}
            className='bg-theme'
            onClick={() => {
              setAddStaff(true);
              setIdStaff();
            }}
          >
            + Thêm Nhân Viên
          </Button>
        </div>
      )}
      <div className='table-wrapper'>
        <table className='table mb-0'>
          <thead className='bg-white'>
            <tr>
              <th scope='col' className='border-0'>
                Tên Nhân Viên
              </th>

              <th scope='col' className='border-0'>
                Email
              </th>

              <th scope='col' className='border-0'>
                Số Điện Thoại
              </th>
              <th scope='col' className='border-0'>
                Ngày Tham Gia
              </th>
              <th scope='col' className='border-0'>
                Hành Động
              </th>
            </tr>
          </thead>
          <tbody>
            {listUser?.map((v, i) => (
              <tr style={{ height: '30px', backgroundColor: 'white' }} key={i}>
                <td>
                  {v?.first_name} {v?.last_name}
                </td>
                <td>{v?.email}</td>
                <td>{v?.phone}</td>
                <td>21/2/2022</td>

                <td>
                  {roleAction && profile.role !== 'USER' && (
                    <Button
                      className='btn-edit'
                      style={{ color: '#FF9900', fontSize: '20px', cursor: 'pointer' }}
                      onClick={() => {
                        setIdStaff(v);
                        setAddStaff(true);
                      }}
                    >
                      <FaPen data-tip={`Sửa Nhân Viên  ${v?.first_name} ${v?.last_name}`} />
                      <ReactTooltip />
                    </Button>
                  )}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Staff;
