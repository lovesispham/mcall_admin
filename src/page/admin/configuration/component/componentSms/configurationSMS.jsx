import React, { useEffect, useState } from 'react';
import { FaPen } from 'react-icons/fa';
import { useDispatch } from 'react-redux';
import ReactTooltip from 'react-tooltip';
import Button from '../../../../../components/common/Button';
import Empty from '../../../../../components/common/Empty';
import Modal, { ModalContent } from '../../../../../components/common/Modal';
import DigConfigurationSMS from '../../../../../components/componentConfiguration/digConfigurationSMS';
import { getconfigSMSApi } from '../../../../../features/common/commonThunk';

const ConfigurationSMS = () => {
  const dispatch = useDispatch();
  const [listSMS, setListSMS] = useState([]);

  const [addSMS, setAddSMS] = useState(false);
  const [edit, setEdit] = useState();
  const [activeConfiguration, setActiveConfiguration] = useState(false);

  useEffect(() => {
    !addSMS &&
      dispatch(getconfigSMSApi())
        .unwrap()
        .then((res) => {
          setListSMS(res?.sms_cfg);
        });
    //eslint-disable-next-line
  }, [addSMS]);
  return (
    <div style={{ paddingRight: '20px' }}>
      <Modal active={activeConfiguration}>
        <ModalContent
          heading={edit ? 'Sửa' : 'Tạo mẫu tin nhắn'}
          onClose={() => {
            setActiveConfiguration(false);
            setEdit();
          }}
        >
          <DigConfigurationSMS setActiveConfiguration={setActiveConfiguration} edit={edit} />
        </ModalContent>
      </Modal>

      <div style={{ display: 'flex', gap: '2px' }}>
        {listSMS?.length < 1 && (
          <Button
            className='bg-theme'
            style={{ marginBottom: '10px' }}
            onClick={() => {
              setActiveConfiguration(true);
              setEdit();
            }}
          >
            Tạo Cấu Hình Tin Nhắn
          </Button>
        )}
      </div>

      <div className='table-wrapper'>
        <table className='table mb-0'>
          <thead className='bg-white'>
            <tr>
              <th scope='col' className='border-0'>
                STT
              </th>

              <th scope='col' className='border-0'>
                Tên
              </th>
              <th scope='col' className='border-0'>
                BRAND NAME
              </th>
              <th scope='col' className='border-0'>
                URL
              </th>
              <th scope='col' className='border-0'>
                TÊN ĐĂNG NHẬP
              </th>
              <th scope='col' className='border-0'>
                TRẠNG THÁI
              </th>
              <th scope='col' className='border-0'>
                HÀNH ĐỘNG
              </th>
            </tr>
          </thead>
          <tbody>
            {listSMS?.map((v, i) => (
              <tr style={{ height: '30px', backgroundColor: 'white' }} key={i}>
                <td> {i + 1}</td>
                <td>{v?.name}</td>
                <td>{v?.url}</td>
                <td>{v?.brandName}</td>
                <td>{v?.name}</td>

                <td>{v?.activate === true ? 'Đang Hoạt Động' : 'Không Hoạt Động'}</td>
                <td>
                  <Button
                    className='btn-edit'
                    onClick={() => {
                      setActiveConfiguration(true);
                      setEdit(v);
                    }}
                  >
                    <FaPen data-tip={`Sửa Nhân Viên  ${v?.name}`} />
                  </Button>
                  <ReactTooltip />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        {listSMS?.length === 0 && <Empty description='Không có dữ liệu'></Empty>}
      </div>
    </div>
  );
};

export default ConfigurationSMS;
