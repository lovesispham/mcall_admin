import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import DiglogAddSMS from '../../../../../components/componentConfiguration/diglogAddSMS';
import { showMessageError, showMessageSuccess } from '../../../../../features/alert/alert-message';
import { getSMSApi } from '../../../../../features/common/commonThunk';
import { CssButton } from '../../../../../scss/cssCustoms';
import { AiOutlineEdit } from 'react-icons/ai';
import Button from '../../../../../components/common/Button';
import Empty from '../../../../../components/common/Empty';
import { FaPen } from 'react-icons/fa';
import Modal, { ModalContent } from '../../../../../components/common/Modal';

const NodeSMS = () => {
  const dispatch = useDispatch();
  const [listSMS, setListSMS] = useState([]);
  const [idSMS, setIdSms] = useState();
  const [addSMS, setAddSMS] = useState(false);
  const submitStaff = () => {};

  useEffect(() => {
    !addSMS &&
      dispatch(getSMSApi())
        .unwrap()
        .then((res) => {
          setListSMS(res?.temp_cfg);
        })
        .catch((err) => {});
  }, [addSMS]);

  return (
    <div style={{ paddingRight: '20px' }}>

      <Modal active={addSMS}>
        <ModalContent
          heading={idSMS ? 'Sửa' : 'Tạo mẫu tin nhắn'}
          onClose={() => {
            setAddSMS(false);
            setIdSms();
          }}
        >
           <DiglogAddSMS setAddSMS={setAddSMS} idSMS={idSMS} />
        </ModalContent>
      </Modal>



      <div style={{ display: 'flex', gap: '2px', alignItems: 'center' }}>
        <Button
          style={{ marginBottom: 10 }}
          className='bg-theme'
          onClick={() => {
            setAddSMS(true);
            setIdSms();
          }}
        >
          + Thêm mẫu tin nhắn mới
        </Button>
      </div>

      <div className='table-wrapper'>
        <table className='table mb-0'>
          <thead className='bg-white'>
            <tr>
              <th
                scope='col'
                className='border-0'
                style={{ paddingLeft: '10px', paddingTop: '10px', minWidth: '60px' }}
              >
                STT
              </th>
              <th scope='col' className='border-0'>
                Tên Mẫu
              </th>
              <th scope='col' className='border-0'>
                Nội Dung
              </th>
              <th scope='col' className='border-0'>
                Trạng Thái{' '}
              </th>
              <th scope='col' className='border-0'>
                Hành Động
              </th>
            </tr>
          </thead>
          <tbody>
            {listSMS?.map((v, i) => (
              <tr style={{ height: '30px', backgroundColor: i % 2 === 0 ? 'white' : '#EEEEEE' }}>
                <td style={{ paddingLeft: '10px' }}> {i + 1}</td>
                <td>{v?.name}</td>
                <td>{v?.msg_tem}</td>
                <td>{v?.activate === true ? 'Đang Hoạt Động' : 'Không Hoạt Động'}</td>

                <td>
                  <Button
                    className='btn-edit'
                    onClick={() => {
                      setIdSms(v);
                      setAddSMS(true);
                    }}
                  >
                    <FaPen />
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        {listSMS?.length === 0 && <Empty description='Không có dữ liệu'></Empty>}
      </div>
    </div>
  );
};

export default NodeSMS;
