import React, { useState, useEffect } from "react";
import Col from "../../../../components/common/Col";
import FormInput from "../../../../components/common/FormInput";
import { useFormik } from "formik";
import * as yup from "yup";
import logo from "../../../../assets/images/logo.png";
import Button from "../../../../components/common/Button";
import { HelpText } from "../../../../components/common/HelpText";
import CustomPhotoUpload from "../../../../components/common/CustomPhotoUpload";
import {
  getDepartment,
  getOrgDashboard,
  getPosition,
} from "../../../../features/org/orgSlice";
import { useDispatch } from "react-redux";
import {
  showMessageError,
  showMessageSuccess,
} from "../../../../features/alert/alert-message";
import CustomReactSelect from "../../../../components/common/CustomReactSelect";
import FormLabel from "../../../../components/common/FormLabel";
import { createUserDashboard } from "../../../../features/user/userSlice";
import { styles } from "../../../../scss/cssCustoms";
import { role } from "../../../../util/constan";
const DialogAddEdit = ({ setUDO, dataEdit, unique_id,data }) => {
  const [listPositions, setListPositions] = useState();
  const [department, setDepartment] = useState();
  const [position, setPosition] = useState();
  const [listDepartment, setListDepartment] = useState([]);
  const [selectedImage, setSelectedImage] = useState();

  const [orgList, setListOrg] = useState();

  const [agencyList, setAgencyList] = useState();

  const [orgName, setOrgName] = useState();
  const [roleName, setRole] = useState('Admin');

  const [selectOrgType, setSelectOrgType] = useState(1);
  const [activeEye, setActiveEye] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getOrgDashboard({
      parent: unique_id,
    }))
      .unwrap()
      .then((res) => {
        if (selectOrgType === 1) {
          setListOrg(
            res?.data?.results
              .map((v) => {
                return {
                  name: v?.id,
                  label: v?.name,
                  key: "org",
                };
              })
          );
        }
      });
  }, []);

  useEffect(() => {
    if(data){
     
        setAgencyList(
          [{
            name:data?.id,
            label: data?.name,
                key: "org",

          }]
        );
      
    }
  }, [data])
  
  const handlelistStatus = (value) => {
    if (value.key === "departments") {
      setDepartment(value?.label);
      dispatch(getPosition({ id: value?.name }))
        .unwrap()
        .then((res) => {
          setListPositions(
            res?.data?.data.map((v) => {
              return {
                name: v?.id,
                label: v?.name,
                key: "position",
              };
            })
          );
        });
    }

    if (value.key === "position") {
      setPosition(value.label);
      setFieldValue(value.key, value?.name);
    }
    if (value.key === "org") {
      
      setOrgName(value.label);
      setFieldValue(value.key, value?.name);
    }
    if (value.key === "role") {
      setRole(value.label);
      setFieldValue(value.key, value?.value);
    }
  };

  const validationSchema = yup.object({
    email: yup.string().required("Trường này không được bỏ trống"),
    password: yup.string().required("Trường này không được bỏ trống"),
    org: yup.string().required("Trường này không được bỏ trống"),
  });
  const convertToFormData = () => {
    const formData = new FormData();
    if (selectedImage) {
      formData.append("email", values.email);
      formData.append("username", values.email);
      formData.append("password", values.password);
      formData.append("first_name", values.first_name);
      formData.append("last_name", values.last_name);
      formData.append("position", values.position);
      formData.append("profile_pic", values.profile_pic);

      return formData;
    } else {
      const submitValues = {
        ...values,
        username: values.email,
      };
      delete submitValues["profile_pic"];
      return submitValues;
    }
  };
  const submitForm = async () => {
    
     dispatch(createUserDashboard(convertToFormData()))
     .unwrap()
       .then((res) => {
         dispatch(showMessageSuccess());
       })
       .catch((err) => {
         dispatch(showMessageError());
       });
     setUDO(false);
  };
  const {
    values,
    handleSubmit,
    handleChange,
    setFieldValue,
    handleBlur,
    touched,
    errors,
  } = useFormik({
    enableReinitialize: true,
    initialValues: {
      email: dataEdit?.email || "",
      username: dataEdit?.username || "",
      password: dataEdit?.password || "",
      first_name: dataEdit?.first_name || "",
      last_name: dataEdit?.last_name || "",
     
      profile_pic: dataEdit?.profile_pic || logo,
      role: 'ADMIN'
    },
    validationSchema: validationSchema,
    onSubmit: submitForm,
  });
  return (
    <form onSubmit={handleSubmit}>
      <div className="content">
        <Col className={12}>
          <CustomPhotoUpload
            label="Logo công ty"
            src={values?.logo ? values?.logo : logo}
            onError={({ currentTarget }) => {
              currentTarget.onerror = null; // prevents looping
              currentTarget.src = logo;
            }}
            name="profile_pic"
            onChange={(e) => {
              setFieldValue("profile_pic", e.currentTarget.files[0]);
              setSelectedImage(e.currentTarget.files[0]);
            }}
          />
        </Col>
        <Col className={3}>
          <FormInput
            value={values.email}
            type="text"
            placeholder="Nhập email"
            label={"Email"}
            name="email"
            onChange={handleChange}
            required
          />
          <HelpText status="error">
            {touched.code && errors.code ? errors.code : ""}
          </HelpText>
        </Col>
        <Col className={3}>
          <FormInput
            value={values.email}
            type="text"
            placeholder="Nhập tên đăng nhập"
            label={"Tên đăng nhập"}
            name="username"
            onChange={handleChange}
            disabled
          />
        </Col>
        <Col className={3}>
          <FormInput
            value={values.password}
            type="password"
            placeholder="Nhập mật khẩu"
            label={"Mật khẩu"}
            name="password"
            onChange={handleChange}
            active={activeEye}
                setActive={setActiveEye}
            required
          />
        </Col>
        <Col className={3}>
          <FormInput
            value={values.last_name}
            type="text"
            placeholder="Họ"
            label={"Họ"}
            name="last_name"
            onChange={handleChange}
          />
        </Col>
        <Col className={3}>
          <FormInput
            value={values.first_name}
            type="text"
            placeholder="Tên"
            label={"Tên"}
            name="first_name"
            onChange={handleChange}
          />
        </Col>
        <Col className={3}>
          <CustomReactSelect
            styleInput={styles}
            label="Quyền truy cập"
            onChange={handlelistStatus}
            name="role"
            options={role}
            value={[{ label: roleName }]}
          />
        </Col>
        <Col className={12}>
          <div className="form-input-group">
            <FormLabel style={{ display: "flex", alignItems: "center" }}>
              Chọn:
              {orgType?.map((v) => {
                return (
                  <Button
                    onClick={() => {
                      setSelectOrgType(v?.id);
                      setOrgName();
                    }}
                    className={`btn  ${
                      selectOrgType === v?.id ? "bg-theme" : "outline"
                    }`}
                    style={{ margin: "0 5px" }}
                  >
                    {v?.value}
                  </Button>
                );
              })}
            </FormLabel>
          </div>
        </Col>
        <Col className={3}>
          
              <CustomReactSelect
                styleInput={styles}
                label={selectOrgType === 1 ? "Công ty" : 'Đại lý'}
                isGroup={true}
                onChange={handlelistStatus}
                name="org"
                value={[{ label: orgName }]}
                options={selectOrgType === 1 ? orgList : agencyList}
                menuPlacement="top"
              />
              <HelpText status="error">
                {touched.org && errors.org ? errors.org : ""}
              </HelpText>
            
          
        </Col>
        {/* <Col className={3}>
          <CustomReactSelect
            styleInput={styles}
            style={styles}
            label={"Phòng ban"}
            isGroup={true}
            onChange={handlelistStatus}
            name="status"
            value={[
              { label: department || dataEdit?.position?.department?.name },
            ]}
            options={listDepartment}
            isDisabled={!listDepartment}
            menuPlacement="top"
          />
        </Col>

        <Col className={3}>
          <CustomReactSelect
            styleInput={styles}
            style={styles}
            label={"Chức vụ"}
            isGroup={true}
            onChange={handlelistStatus}
            name="position"
            value={[{ label: position || dataEdit?.position?.name }]}
            options={listPositions}
            isDisabled={!listPositions}
            menuPlacement="top"
          />
        </Col> */}
        {/* <Col className={3}>
        <CustomReactSelect
styleInput={styles}
              label={"Công ty"}
              isGroup={true}
              onChange={handlelistStatus}
              name="org"
              value={[{ label: orgName  }]}
              options={orgList}
              menuPlacement='top'
            />
            <HelpText status="error">
            {touched.org && errors.org ? errors.org : ""}
          </HelpText>
        </Col> */}

        <Col className={12}>
          <div className="text-right">
            <Button className="bg-theme" type="submit">
              Lưu
            </Button>
          </div>
        </Col>
      </div>
    </form>
  );
};

export default DialogAddEdit;

export const orgType = [
  { id: 1, value: "Công ty" },
  { id: 2, value: "Đại lý" },
];
