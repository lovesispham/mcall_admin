import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { editUser, editUserDashboardCode, getUserDashboardCode } from "../../../../features/user/userSlice";
import { useFormik } from "formik";
import * as yup from "yup";
import FormCheckBox from "../../../../components/common/FormCheckBox";
import Heading from "../../../../components/common/Heading";
import Col from "../../../../components/common/Col";
import FormInput from "../../../../components/common/FormInput";
import CustomReactSelect from "../../../../components/common/CustomReactSelect";
import { styles } from "../../../../scss/cssCustoms";
import TextArea from "../../../../components/common/TextArea";
import { editOrgDashboardCode, getDepartment, getOrgDashboard, getPosition } from "../../../../features/org/orgSlice";
import CustomPhotoUpload from "../../../../components/common/CustomPhotoUpload";
import logo from "../../../../assets/images/logo.png";
import Button from "../../../../components/common/Button";
import { showMessageSuccess,showMessageError } from "../../../../features/alert/alert-message";
import { gender,role } from "../../../../util/constan";
const UserDetail = () => {
  const { id } = useParams();
  const data = useSelector((state) => state.user.userDetail);
  const state = useSelector((state) => state.user);
  const [activeEye, setActiveEye] = useState(false);
  const [active, setActive] = useState({
    sale: false,
    marketing: false,
    org: false,
    is_active: false,
  });
  const [selectedImage, setSelectedImage] = useState();
  const [listDepartment, setListDepartment] = useState([]);
  const [listPositions, setListPositions] = useState();
  const [department, setDepartment] = useState();
  const [orgList,setListOrg] = useState()
  const [orgName, setOrgName] = useState()
  const [position, setPosition] = useState();
  const [genderName, setGender] = useState()
  const [roleName, setRole] = useState()
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getOrgDashboard())
    .unwrap()
    .then((res) => {
      setListOrg(
        res?.data?.results.map((v) => {
          return {
            name: v.id,
            label: v.name,
            key: "org",
          };
        })
      );
    });
  }, [])
  


  useEffect(() => {
    if(id){
      dispatch(
        getUserDashboardCode({
          code: id,
        })
      );
    }
  }, [id]);
  
  useEffect(() => {
    if(state.success === true){
        dispatch(
            getUserDashboardCode({
              code: id,
            })
          );
    }
  }, [state.success])
  


  useEffect(() => {
    if (data) {
        setActive({
            ...active,
            is_active: !!data?.is_active
        })
        setDepartment(data?.position?.department?.name);
        setPosition(data?.position?.name);
        setRole(data?.role);
        setOrgName(data?.org?.name);
    }
  }, [data]);


  // useEffect(() => {
  //   dispatch(getDepartment())
  //     .unwrap()
  //     .then((res) => {
  //       setListDepartment(
  //         res?.data?.data.map((v) => {
  //           return {
  //             name: v.id,
  //             label: v.name,
  //             key: "departments",
  //           };
  //         })
  //       );
  //     });
  // }, []);

  const handleChangeOptions = (value) => {
    if (value.key === "departments") {
      setDepartment(value?.label);
      // dispatch(getPosition({ id: value?.name }))
      //   .unwrap()
      //   .then((res) => {
      //     setListPositions(
      //       res?.data?.data.map((v) => {
      //         return {
      //           name: v.id,
      //           label: v?.name,
      //           key: "position",
      //         };
      //       })
      //     );
      //   });
    }

    if (value.key === "position") {
      setPosition(value.label);
      setFieldValue(value.key, value?.name);
    }
    if (value.key === "gender") {
        setGender(value.label);
        setFieldValue(value.key, value?.name);
      }
      if (value.key === "role") {
        setRole(value.label);
        setFieldValue(value.key, value?.value);
      }
      if (value.key === "org") {
        setOrgName(value.label);
        setFieldValue(value.key, value?.name);
      }
  };


  function clean(obj) {
    for (var propName in obj) {
      if (obj[propName] === null || obj[propName] === undefined  || obj[propName] === "") {
        delete obj[propName];
      }
    }
    return obj
  }

  const convertToFormData = () => {
    const formData = new FormData();

    if (selectedImage) {
        for (const [key, value] of Object.entries(values)) {
            formData.append(key, value);
        }
      
      return formData;
         } else {
          clean(values)
           return values;
    }
  };


  const validationSchema = yup.object({});
  const submitForm = () => {
    dispatch(editUserDashboardCode({
        code: data?.unique_id,
        body: convertToFormData()
    }))
    .then((res)=>{
        dispatch(showMessageSuccess())
      })
      .catch((err)=>{
        dispatch(showMessageError())
      })
  };
  const {
    values,
    handleSubmit,
    handleChange,
    setFieldValue,
    handleBlur,
    touched,
    errors,
  } = useFormik({
    enableReinitialize: true,
    initialValues: {
      org: data?.org?.id || "",
      first_name: data?.first_name || "",
      last_name: data?.last_name || "",
      role: data?.role || "",
      gender: data?.gender || "",
      email: data?.email || "",
      phone: data?.phone || "",

      description: data?.description || "",
      profile_pic: `${import.meta.env.VITE_REACT_APP_API_URL}${data?.profile_pic}`  || logo,
      // has_sales_access: !!data?.has_sales_access,
      // has_marketing_access: !!data?.has_marketing_access ,
      is_active: !!data?.is_active,
      // is_organization_admin:!!data?.is_organization_admin,
      position: data?.position?.id || "",
      new_password: data?.new_password || "",
      agent: data?.agent || "",
    },
    validationSchema: validationSchema,
    onSubmit: submitForm,
  });
  return (
    <>
      <Heading type={'h2'} style={{ fontSize: 25, margin:'20px 0' }}>
        Thông tin chi tiết
      </Heading>
      <form onSubmit={handleSubmit}>
      <div className="box-infor box-information">
        <div className="row">
        <Col className={12}>
        <CustomPhotoUpload
            
            label="Logo công ty"
            src={
                  (selectedImage && URL.createObjectURL(selectedImage)) || values.profile_pic
                }
            onError={({ currentTarget }) => {
              currentTarget.onerror = null; // prevents looping
              currentTarget.src = logo;
            }}
            name="profile_pic"
            onChange={(e) => {
              setFieldValue(
                "profile_pic",
                URL?.createObjectURL(e.currentTarget.files[0])
              );
              setSelectedImage(e.currentTarget.files[0]);
            }}
          />
        </Col>

          <Col className={6}>
            <FormInput
              className={"inline border"}
              label="Họ"
              type="text"
              name="last_name"
              value={values.last_name}
              onChange={handleChange}
            />
            <FormInput
              className={"inline border"}
              label="Tên"
              type="text"
              name="first_name"
              value={values.first_name}
              onChange={handleChange}
            />

            <FormInput
              className={"inline border"}
              label="Email"
              type="text"
              name="email"
              value={values.email}
              onChange={handleChange}
            />
                <FormInput
              className={"inline border"}
              label="Số điện thoại"
              type="number"
              name="phone"
              value={values.phone}
              maxLength={10}
              min={9}
              onChange={handleChange}
            />


<FormInput
              className={"inline border"}
              label="Mật khẩu"
              type="password"
              name="new_password"
              value={values.new_password}
              onChange={handleChange}
              active={activeEye}
                setActive={setActiveEye}
            />




            <CustomReactSelect
              styleInput={styles}
              className={"inline border"}
              label="Quyền truy cập"
              onChange={handleChangeOptions}
              name="role"
              options={role}
              value={
                [{ label: roleName }]
              }
              isDisabled
            />
          </Col>
          <Col className={6}>
          <CustomReactSelect
              styleInput={styles}
              className={"inline border"}
              label="Giới tính"
              onChange={handleChangeOptions}
              name="gender"
              options={gender}
              value={
                [{ label: genderName }]
              }
            />
            {/* <CustomReactSelect
                styleInput={styles}
              className={"inline border"}
              label={"Phòng ban"}
              isGroup={true}
              onChange={handleChangeOptions}
              name="status"
              value={[{ label: department }]}
              options={listDepartment}
              isDisabled={!listDepartment}
            /> */}
            <FormInput
              className={"inline border"}
              label={"Phòng ban"}
              type="text"
              value={data?.position?.department?.name}
              disabled
            />
                        <FormInput
              className={"inline border"}
              label={"Chức vụ"}
              type="text"
              value={data?.position?.name}
              disabled
            />
            {/* <CustomReactSelect
            styleInput={styles}
              className={"inline border"}
              label={"Chức vụ"}
              isGroup={true}
              onChange={handleChangeOptions}
              name="position"
              value={[{ label: position  }]}
              options={listPositions}
              isDisabled={!listPositions}
            /> */}
            <FormInput
              className={'inline border'}
              label={!data?.org?.agency  ? 'Công ty' : 'Đại lý'}
              type='text'
              value={data?.org?.name}
              disabled
            />


          </Col>
          <Col className={12}>
            
            {/* <FormCheckBox
              pos="left"
              label={"Có quyền truy cập sale"}
              name={`has_sales_access`}
              onChange={(e) => {
                setActive({ ...active, sale: e.target.checked });
                setFieldValue("has_sales_access", e.currentTarget.checked);
              }}
              checked={active?.sale}
            />
            <FormCheckBox
              pos="left"
              label={"Có quyền truy cập marketing"}
              name={`has_marketing_access`}
              onChange={(e) => {
                setActive({ ...active, marketing: e.target.checked });
                setFieldValue("has_marketing_access", e.currentTarget.checked);
              }}
              checked={active?.marketing}
            />
            <FormCheckBox
              pos="left"
              label={"Có quyền truy cập org"}
              name={`is_organization_admin`}
              onChange={(e) => {
                setActive({ ...active, org: e.target.checked });
                setFieldValue("is_organization_admin", e.currentTarget.checked);
              }}
              checked={active?.org}
            /> */}
            <FormCheckBox
              pos="left"
              label={"Đang hoạt động"}
              name={`is_active`}
              onChange={(e) => {
                setActive({ ...active, is_active: e.target.checked });
                setFieldValue("is_active", e.currentTarget.checked);
              }}
              checked={active?.is_active || values.is_active}
            />
          </Col>
          <Col className={12}>
            <TextArea
              className="inline border"
              label={"Mô tả"}
              rows={8}
              type="text"
              name="description"
              value={values.description}
              onChange={handleChange}
            />
          </Col>
          <Col className={12}>
            <div className="text-right">
                <Button className="bg-theme">Lưu</Button>
            </div>
          </Col>
        </div>
      </div>
      </form>
    </>
  );
};

export default UserDetail;


