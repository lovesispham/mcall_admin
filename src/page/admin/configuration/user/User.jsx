import React, { useEffect, useState } from "react";
import { FaEye, FaPen } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
// import Button from "../../../../components/common/Button";
// import Empty from "../../../../components/common/Empty";
import { Pagination } from "../../../../components/common/Pagination";
import { getOrgDashboard } from "../../../../features/org/orgSlice";
import { usePaginationState } from "../../../../hooks/use-pagination";
import moment from "moment";
import { AiOutlinePlusCircle } from "react-icons/ai";

import { getUserDashboard } from "../../../../features/user/userSlice";
import Button from "../../../../components/common/Button";
import Modal, { ModalContent } from "../../../../components/common/Modal";
import DialogAddEdit from "./dialogAddEdit";
import { useNavigate } from "react-router-dom";
import FormInput from "../../../../components/common/FormInput";
import Empty from "../../../../components/common/Empty";
import Filter, { FILTER_OPTION } from "../filter/Filter";
import ResultFilter from "../filter/Results";
import Heading from "../../../../components/common/Heading";

const User = (props) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [active, setActive] = useState(false);
  const [editCompany, setEditCompany] = useState();
  const list = useSelector((state) => state.user.userList);
  const profile = useSelector((state) => state.user.profile);
  const state = useSelector((state) => state.org);
  const pagination = usePaginationState();
  
  const [filter, setFilter] = useState({})
  const [search, setSearch] = useState({
    key: "",
    
  });


  const handleSearch = () => {
    dispatch(
      getUserDashboard({
        ...search,
        limit: pagination.perPage,
        offset: pagination.perPage * pagination.page - pagination.perPage,
        org_unique:profile?.org?.unique_id,
      })
    )
      .unwrap()
      .then((res) => {
       
      })
      .catch((err) => {});
  };


  const handleClearSearch = () => {
    setSearch({ ...search, key: "" });
  };
  useEffect(() => {
    if (!search?.key && profile?.org?.unique_id) {
      handleClearSearch();
      dispatch(
        getUserDashboard({
          org_unique:profile?.org?.unique_id,
          limit: pagination.perPage,
          offset: pagination.perPage * pagination.page - pagination.perPage,
          ...search
        })
      );
    }
   

  }, [!search?.key, pagination, profile?.org?.unique_id]);

  useEffect(() => {
    if (state.success === true) {
      dispatch(getUserDashboard());
    }
  }, [state.success]);


  const getRoleBg = (type) =>{
    if(type === 'ADMIN'){
      return 'bg-admin'
    }
    if(type === 'SUPERADMIN'){
      return 'bg-superadmin'
    }
    return 'bg-user'
  }
  return (
    <>
       <Modal active={active}>
        <ModalContent
          className="pu-company"
          heading={!editCompany ? "Thêm tài khoản" : "Sửa"}
          onClose={() => {
            setActive(false);
            setData('')
          }}
        >
          <DialogAddEdit 
            setUDO={setActive}
            unique_id={props.parent}
            data={props.data}
          />
        </ModalContent>
      </Modal>
      <Heading
        type={"h2"}
        style={{ fontSize: 25, margin: "0 0 20px 0" }}
      >
        Danh sách tài khoản
      </Heading>
      <div className="flex">
       {/* {
        
          !props.parent && <Button
          style={{ marginBottom: 10 }}
          className='outline'
          onClick={() => {
            setActive(true);
          }}
        >
          <AiOutlinePlusCircle style={{marginRight:5, fontSize:18}}/>Thêm tài khoản
        </Button> 
        
       } */}
        <FormInput
            isGroup={true}
            className="ml-auto search-wrapper"
            classInput="input-search"
            type="text"
            placeholder="Tìm kiếm tên"
            name="key"
            value={search?.name}
            onChange={(e) => {
              setSearch({ ...search, 'key': e.target.value });
            }}
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                dispatch(
                  getUserDashboard({
                    org_unique:profile?.org?.unique_id,
                    limit: pagination.perPage,
                    offset:
                      pagination.perPage * pagination.page - pagination.perPage,
                      ...search,
                      key: search?.key,
                      
                  })
                );
              }
            }}
            isclear={search?.key}
            handleclear={handleClearSearch}
          />
            <Filter
            className={'style2'}
          classFilterMore={'ml-auto'}
          type='user'
          
          onSearch={handleSearch}
          filter={search}
          setFilter={setSearch}
          optionsDataDrp={[
            FILTER_OPTION.ROLE,
            FILTER_OPTION.ISACTIVE,

          ]}
        />
          </div>
          <ResultFilter 
          totalRecord={list?.count}
            filter={search}
          setFilter={setSearch}
          onSearch={handleSearch}
          delKeySearch = 'key'
          />
          
      <div className="table-wrapper">
        <table className="table mb-0">
          <thead className="bg-white">
            <tr>
              <th scope="col" className="border-0" style={{ width: 110 }}>
                Tên tài khoản
              </th>

              <th scope="col" className="border-0" style={{ width: 200 }}>
                Phân loại
              </th>

              <th scope="col" className="border-0">
                Email
              </th>
              <th scope="col" className="border-0" style={{ width: 110 }}>
                Số điện thoại
              </th>
              <th scope="col" className="border-0">
                Tên công ty
              </th>
              
              <th scope="col" className="border-0">
                Hành động
              </th>
            </tr>
          </thead>

          <tbody>
             {list?.results?.map((item, idx) => {
              return (
                <tr key={idx}>
                  <td>{item?.username}</td>
                  <td>
                    <span className={`tag-role ${getRoleBg(item?.role)}`}>{item?.role}</span>
                  </td>
                  <td>{item?.email}</td>
                  <td>{item?.phone}</td>
                  <td>{item?.org?.name}</td>


                  <td className="flex">
                    <Button
                      className="btn-edit"
                      onClick={() => {
                        navigate(`${item.unique_id}`)
                      }}
                    >
                      <FaEye />
                    </Button>
                  </td>
                </tr>
              );
            })} 
          </tbody>
        </table>
         <Pagination
      currentPage={pagination.page}
      pageSize={pagination.perPage}
      lastPage={Math.min(
        Math.ceil((list?.results ? list?.count : 0) / pagination.perPage),
        Math.ceil((list?.results ? list?.count : 0) / pagination.perPage)
      )}
      onChangePage={pagination.setPage}
      onChangePageSize={pagination.setPerPage}
      onGoToLast={() =>
        pagination.setPage(Math.ceil((list?.results ? list?.count : 0) / pagination.perPage))
      }
    />
    {list?.results?.length === 0 && <Empty description='Không có dữ liệu'></Empty>} 
      </div>
      
    </>
  );
};

export default User;
