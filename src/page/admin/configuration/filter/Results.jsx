import React from "react";
import { activeUser, dataTrans, role } from "./Filter";
import { FaTimesCircle } from "react-icons/fa";
const ResultFilter = (props) => {
  const transFilter = (name) => {
    const text = dataTrans?.find((item) => item.type === name);
    return text && text.title;
  };

  const transText = (type, name) => {
    let filter;
    switch (type) {
      case "isActive":
        filter = activeUser.find((item) => item.value === name);
        return filter && filter.label;
      case "role":
        filter = role.find((item) => item.value === name);
        return filter && filter.label;
    }
  };

  const handleRemoveFilter = (name) => {
    delete props.filter[name];
    props.setFilter({ ...props.filter});
    props.onSearch(props.filter);
  };
  return (
    <div
      className="filter-tag"
      style={{
        height: Object.keys(props.filter).length > 3 ? "auto" : 40,
      }}
    >
      <span style={{ marginRight: 10, display: "inline-block", marginTop: 5 }}>
        Tổng số bản ghi : {props.totalRecord}
      </span>
      {props.filter &&
        Object.keys(props.filter).filter((key) => key !== props.delKeySearch).map((v, index) => (
          <div
            className="filter-tag-item"
            key={index}
            onClick={() => handleRemoveFilter(v)}
          >
            <span>{transFilter(v)}: </span>
            {transText(v, props.filter[v])}
            <span className="btn-remove">
              <FaTimesCircle />
            </span>
          </div>
        ))}
    </div>
  );
};

export default ResultFilter;
