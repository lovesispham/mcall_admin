import React, { useState } from "react";
import { BsFilter } from "react-icons/bs";

import Button from "../../../../components/common/Button";
import CustomSelect from "../../../../components/common/CustomSelect";
import { useOnOutsideClick } from "../../../../hooks/use-outside";
export const FILTER_OPTION = {
  ROLE: "role",
  ISACTIVE: "isActive",
};
export const role = [
  { value: "ADMIN", label: "ADMIN", key: "role" },
  { value: "USER", label: "USER", key: "role" },
  { value: "SUPERADMIN", label: "SUPERADMIN", key: "role" },
];
export const activeUser = [
  { value: true, label: "Đang hoạt động", key: "isActive" },
  { value: false, label: "Ngưng hoạt động", key: "isActive" },
];
export const dataTrans = [
  {
    title: "Quyền truy cập",
    type: "role",
  },
  {
    title: "Trạng thái",
    type: "isActive",
  },
];
const Filter = (props) => {
  const [active, setActive] = useState(false);


  const { innerBorderRef } = useOnOutsideClick(() => {
    setActive(false);
  });

  const handleSearch = () => {
    props.onSearch(props.filter);
    setActive(false);
  };

  const handleChange = (field, value) => {
    props.setFilter({ ...props.filter, [field]: value });
  };

  return (
    <>
      <div className={`filter-box ${props.className}`}>
        {" "}
        {props.optionsDataDrp && (
          <div className={`box-add-filter ${props.classFilterMore}`} ref={innerBorderRef}>
            <Button
              onClick={() => setActive(!active)}
              className="btn outline"
            >
              <BsFilter />
            </Button>

            <div className={`dropdown-menu ${active ? "active" : ""}`}>
              <h3 className="title">
                Lọc
                <span
                  onClick={() => {
                    setActive(false);
                    handleSearch();
                  }}
                  className="text"
                >
                  Lưu
                </span>
              </h3>

              <CustomSelect
                filter={props.filter}
                isGroup={true}
                className="form-control"
                handleChange={handleChange}
                label={"Hoạt động"}
                name="isActive"
                options={activeUser}
              />

              <CustomSelect
                filter={props.filter}
                isGroup={true}
                className="form-control"
                handleChange={handleChange}
                label={"Quyền truy cập"}
                name="role"
                options={role}
              />
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default Filter;
