import React,{useState} from 'react'
import Select from 'react-select'
import CustomReactSelect from '../../../../components/common/CustomReactSelect';
import { editUser } from '../../../../features/user/userSlice';
import { styles } from "../../../../scss/cssCustoms";
const ConfigExtension = (props) => {
  const [labelPhone, setLablePhone] = useState([]);
  const [active, setActive] = useState(true);

  const handleChange = (value) => {
    const arr = labelPhone;
    arr[value?.id] = value.label;
    setActive(!active);
    setLablePhone(arr);
     dispatch(editUser({ id: value.id, email: value?.email, agent: value.idPhone }))
       .unwrap()
       .then((res) => {
         dispatch(showMessageSuccess('Gắn Tài Khoản Thành Công'));
       })
       .catch((err) => {
         dispatch(showMessageError('Găn Tài Khoản Thất Bài'));
       });
  };

  return (
    <div className='table-wrapper'>
        <table className='table mb-0'>
          <thead className='bg-white'>
            <tr>
              <th scope='col' className='border-0'>
                Id
              </th>

              <th scope='col' className='border-0'>
                UserName
              </th>
              <th scope='col' className='border-0'>
                Quyền
              </th>
              <th scope='col' className='border-0'>
                Email
              </th>
              <th scope='col' className='border-0'>
                Máy lẻ
              </th>
            </tr>
          </thead>
          <tbody>
            {props.listUser?.map((v, i) => (
              <tr key={i}>
                <td>{v.id}</td>
                <td>
                  {v?.last_name} {v?.first_name}
                </td>
                <td>{v?.role}</td>
                <td>{v?.email}</td>

                <td style={{ paddingLeft: '10px' }}>
                  {' '}
                  <CustomReactSelect
                    isGroup={false}
                    styleInput={styles}
                    className={"form-control"}
                    menuPortalTarget={document.body}
                    menuPosition={'fixed'}
                    menuPlacement='bottom'
                    onChange={handleChange}
                    name='colors'
                    value={[
                      {
                        label: labelPhone[v?.id] || props.listPhone?.find((h) => h.idPhone === v?.agent?.id)?.label,
                        value: labelPhone[v?.id] || props.listPhone?.find((h) => h.idPhone === v?.agent?.id)?.label,
                      },
                    ]}
                    options={props.listPhone.map((k) => ({
                      ...k,
                      email: v.email,
                      id: v.id,
                    }))}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
  )
}

export default ConfigExtension