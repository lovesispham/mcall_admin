import React, { useEffect, useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import Button from "../../../../components/common/Button";
import Empty from "../../../../components/common/Empty";
import {
  editOrgDashboardCode,
  getOrgDashboard,
  orgAsync,
} from "../../../../features/org/orgSlice";
import { usePaginationState } from "../../../../hooks/use-pagination";
import { AiOutlinePlusCircle } from "react-icons/ai";
import Modal, { ModalContent } from "../../../../components/common/Modal";
import DialogAddEdit from "./dialogAddEdit";
import DiglogAddAdmin from "../component/company/diglogAddAdmin";
import DialogAsk from "./DialogAsk";
import {
  showMessageSuccess,
  showMessageError,
} from "../../../../features/alert/alert-message";
import FormCheckBox from "../../../../components/common/FormCheckBox";
import CustomSelect from "../../../../components/common/CustomSelect";
import DialogAsync from "./DialogAsync";
import FormInput from "../../../../components/common/FormInput";
import Col from "../../../../components/common/Col";
import Item from "./components/Item";
import InfiniteScroll from "../../../../hooks/InfiniteScroll";
import LoadingCard from "./components/LoadingCard";

const Agent = () => {
  
  const dispatch = useDispatch();
  const [search, setSearch] = useState({
    isClear: false,
    name: "",
  });

  const listType = [
    { value: "all", label: "Đồng bộ tất cả", name: "async:" },
    { value: "list", label: "Chọn theo danh sách", name: "async:" },
  ];

  const [active, setActive] = useState(false);
  const [modalAdmin, setModalAdmin] = useState(false);
  const [modalAsk, setModalAsk] = useState(false);
  const [editCompany, setEditCompany] = useState(null);
  // const list = useSelector((state) => state.org.orgAdminList);

  const state = useSelector((state) => state.org);
  const profile = useSelector((state) => state.user.profile);
  const pagination = usePaginationState();

  const [list, setList] = useState([]);
  const [page, setPage] = useState(0);
  const [total, setTotal] = useState(0);
  const [totalPage, setTotalPage] = useState(1);

  const fetchFeed = async () => {
    var arr = [...list];

    dispatch(
      getOrgDashboard({
        parent:profile?.org?.unique_id,
        agency: `True`,
        limit: 10,
        offset: (page - 1 + 1) * 10,
      })
    ).then((res) => {
      setTotalPage(Math.round(res?.payload?.data?.count / 10));
      setTotal(res?.payload?.data?.count);
      res?.payload?.data?.results?.map((item) => {
        return arr.push({
          ...item,
          page: page,
        });
      });
      const unique = [
        ...new Map(arr.map((item) => [item["unique_id"], item])).values(),
      ];

      if (unique) {
        setList(unique);
      }
    });
  };

  useEffect(() => {
    if (!search?.name && page <= totalPage && profile?.org?.unique_id) {
      handleClearSearch();
      fetchFeed();
    }
  }, [!search?.name, page, profile?.org?.unique_id]);

  useEffect(() => {
    if (state.success === true) {
      fetchFeed();
    }
  }, [state.success]);

  const handleStopOrg = () => {
    const values = {
      name: editCompany?.name,
      is_active: !editCompany.is_active,
    };
    dispatch(
      editOrgDashboardCode({
        code: editCompany?.unique_id,
        body: values,
      })
    )
      .unwrap()
      .then((res) => {
        dispatch(showMessageSuccess());
        setPage(editCompany?.page);
      })
      .catch((err) => {
        console.log(err);
        dispatch(showMessageError());
      });
    setModalAsk(false);
  };

  const handleClearSearch = () => {
    setSearch({ ...search, name: "" });
  };
  // checkbox

  const arrList = useRef();
  const [checkBoxAll, setCheckBoxAll] = useState(false);
  const [activeTest, setActiveTest] = useState(false);

  const handleCheckAll = async (e) => {
    if (e.target.checked) {
      setCheckBoxAll(true);

      const arr = await arrList.current;
      arr.push(...list?.map((v) => v?.unique_id));

      arrList.current = arr;
      setActiveTest(!activeTest);
    } else {
      const arr = arrList.current.filter(
        (v) => !list?.find((k) => k.unique_id === v)
      );

      setCheckBoxAll(false);
      setActiveTest(!activeTest);
      arrList.current = arr;
    }
  };

  const handlnCheckBox = async (e) => {
    if (e.target.checked) {
      const arr = await arrList.current;
      await arr.push(e.target.name);

      arrList.current = arr;

      setActiveTest(!activeTest);
    } else {
      const arr = await arrList.current;
      await arr?.splice(arrList.current.indexOf(e.target.name), 1);
      arrList.current = arr;
      setActiveTest(!activeTest);
      setCheckBoxAll(false);
    }
  };

  useEffect(() => {
    arrList.current = [];
  }, []);

  useEffect(() => {
    if (
      list?.filter((v) => arrList.current?.includes(v?.unique_id)).length >
      list?.length - 1
    ) {
      setCheckBoxAll(true);
    } else {
      setCheckBoxAll(false);
    }
  }, [activeTest, pagination, list]);

  // select box

  const [modalAsync, setModalAsync] = useState({
    open: false,
    type: "",
  });

  const handleChange = (field, value) => {
    setModalAsync({ ...modalAsync, type: value, open: true });
  };
  const synchronized = () => {
    const submitValue =
      modalAsync?.type === "all"
        ? {
            async: "all",
          }
        : {
            async: "list",
            orgs: arrList?.current,
          };
    dispatch(orgAsync(submitValue))
      .unwrap()
      .then((res) => {
        dispatch(showMessageSuccess("Đồng bộ thành công"));
        setModalAsync({ ...modalAsync, open: false });
      })
      .catch((err) => {
        dispatch(showMessageError("Đồng bộ thất bại"));
      });
  };

  return (
    <>
      <Modal active={active}>
        <ModalContent
          className="pu-company"
          heading={!editCompany ? "Thêm đại lý" : "Sửa đại lý"}
          onClose={() => {
            setActive(false);
            setEditCompany("");
          }}
        >
          <DialogAddEdit
            setUDO={setActive}
            dataEdit={editCompany}
            setData={setEditCompany}
            createAgent={true}
          />
        </ModalContent>
      </Modal>

      <Modal active={modalAdmin}>
        <ModalContent
          className="pu-company"
          heading={"Thêm SuperAdmin cho công ty"}
          onClose={() => {
            setModalAdmin(false);
          }}
        >
          <DiglogAddAdmin
            setActiveAddAdmin={setModalAdmin}
            dataEdit={editCompany}
          />
        </ModalContent>
      </Modal>

      <Modal active={modalAsk}>
        <ModalContent
          className="modal-warning"
          onClose={() => {
            setModalAsk(false);
          }}
        >
          <DialogAsk
            setUDO={setModalAsk}
            dataEdit={editCompany}
            text={
              editCompany?.is_active
                ? "Bạn có muốn dừng hoạt động công ty"
                : "Bạn có muốn kích hoạt công ty"
            }
            handleStop={handleStopOrg}
          />
        </ModalContent>
      </Modal>

      <Modal active={modalAsync?.open}>
        <ModalContent
          heading={"Đồng bộ tổng đài"}
          className="modal-warning"
          onClose={() => {
            setModalAsync({ ...modalAsync, open: false });
          }}
        >
          <DialogAsync
            setUDO={setModalAsync}
            text={
              modalAsync?.type === "all"
                ? "Đồng bộ tất cả"
                : `Tổng số công ty đã chọn:  ${arrList?.current?.length} `
            }
            handleAsync={synchronized}
            list={modalAsync?.type === "list" && arrList?.current?.length}
          />
        </ModalContent>
      </Modal>

      <div className="flex">
         <Button
          style={{ marginBottom: 10 }}
          className="outline"
          onClick={() => {
            setActive(true);
          }}
        >
          <AiOutlinePlusCircle style={{ marginRight: 5, fontSize: 18 }} />
          Thêm đại lý
        </Button> 

        {/* <CustomSelect
            isGroup={false}
            className="outline"
            handleChange={handleChange}
            label={"Đồng bộ tổng đài"}
            name="async"
            options={listType}
            style={{
              width: "inherit",
              padding: "0 10px",
              height: 36,
              marginRight: 10,
            }}
          /> */}
        <div style={{ marginLeft: "auto", display: "flex" }}>
          

          <FormInput
            isGroup={true}
            classInput="input-search"
            type="text"
            placeholder="Tìm kiếm tên"
            name="name"
            value={search?.name}
            onChange={(e) => {
              setSearch({ ...search, name: e.target.value });
            }}
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                dispatch(
                  getOrgDashboard({
                    parent:profile?.org?.unique_id,
                    agency: `True`,
                  })
                ).then((res) => {
                  setTotalPage(Math.round(res?.payload?.data?.count / 10));
                  setPage(0);
                  setTotal(res?.payload?.data?.count);
                  setList(res?.payload?.data?.results);
                });
              }
            }}
            isclear={search?.name}
            handleclear={handleClearSearch}
          />
        </div>
      </div>

      <InfiniteScroll
        loader={<LoadingCard />}
        className="row row-company"
        fetchMore={() => {
          setPage((prev) => prev + 1);
        }}
        hasMore={list?.length < total}
        endMessage={
          list?.length === 0 && <Empty description="Không có dữ liệu"></Empty>
        }
      >
        {list?.map((v) => {
          return (
            <Col className={4}>
              <Item
                item={v}
               
                
              />
            </Col>
          );
        })}
      </InfiniteScroll>
      {/* {list?.results?.length === 0 && (
        <Empty description="Không có dữ liệu"></Empty>
      )} */}

      {/* <div className="table-wrapper">
        <table className="table mb-0">
          <thead className="bg-white">
            <tr>
              <th scope="col" className="border-0" style={{ width: 40 }}>
                <FormCheckBox
                  pos={"left"}
                  onChange={handleCheckAll}
                  checked={checkBoxAll}
                />
              </th>
              <th scope="col" className="border-0" style={{ width: 110 }}>
                Mã công ty
              </th>

              <th scope="col" className="border-0" style={{ width: 200 }}>
                Tên công ty
              </th>

              <th scope="col" className="border-0" style={{ width: 200 }}>
                Địa chỉ
              </th>
              <th scope="col" className="border-0">
                Số điện thoại
              </th>
              <th scope="col" className="border-0" style={{ width: 160 }}>
                Số lượng nhân viên (tối đa)
              </th>

              <th scope="col" className="border-0">
                Ngày tạo
              </th>
              <th scope="col" className="border-0" style={{ width: 150 }}>
                Đang hoạt động
              </th>
              <th scope="col" className="border-0">
                Hành động
              </th>
            </tr>
          </thead>

          <tbody>
            {list?.results?.map((item, idx) => {
              return (
                <tr key={idx}>
                  <td>
                    <FormCheckBox
                      name={item?.unique_id}
                      pos={"left"}
                      onChange={handlnCheckBox}
                      checked={arrList?.current?.includes(item?.unique_id)}
                    />
                  </td>
                  <td>{item?.code}</td>
                  <td>{item?.name}</td>
                  <td>{item?.address}</td>
                  <td>{item?.phone}</td>
                  <td>{item?.user_limit}</td>

                  <td>{moment(item?.created_on).format("DD-MM-YYYY")}</td>
                  <td className="text-center">
                    <div
                      className={`status ${
                        item.is_active ? "is_success" : "is_error"
                      }`}
                    >
                      <Button
                        className="outline"
                        style={{
                          fontSize: 12,
                          fontWeight: "bold",
                          height: 26,
                          width: 130,
                          padding: "0 5px",
                        }}
                        onClick={() => {
                          setEditCompany(item);
                          setModalAsk(true);
                        }}
                      >
                        <FaStop
                          style={{
                            marginRight: 5,
                            color: item.is_active ? "#8ec640" : "#d64237",
                          }}
                        />{" "}
                        {item.is_active ? "Đang hoạt động" : "Dừng hoạt động"}
                      </Button>
                    </div>
                  </td>
                  <td className="flex">
                    <Button
                      className="btn-edit"
                      onClick={() => {
                        setEditCompany(item);
                        setActive(true);
                      }}
                    >
                      <FaPen />
                    </Button>
                    <Button
                      className="btn-edit bg-theme"
                      onClick={() => {
                        setModalAdmin(true);
                        setEditCompany(item);
                      }}
                    >
                      <FaUser />
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <Pagination
          currentPage={pagination.page}
          pageSize={pagination.perPage}
          lastPage={Math.min(
            Math.ceil((list?.results ? list?.count : 0) / pagination.perPage),
            Math.ceil((list?.results ? list?.count : 0) / pagination.perPage)
          )}
          onChangePage={pagination.setPage}
          onChangePageSize={pagination.setPerPage}
          onGoToLast={() =>
            pagination.setPage(
              Math.ceil((list?.results ? list?.count : 0) / pagination.perPage)
            )
          }
        />
        {list?.results?.length === 0 && (
          <Empty description="Không có dữ liệu"></Empty>
        )}
      </div> */}
    </>
  );
};

export default Agent;
