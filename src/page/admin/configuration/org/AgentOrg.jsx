import React from 'react'
import { useSelector } from 'react-redux'
import Col from '../../../../components/common/Col'
import Item from './components/Item'

const AgentOrg = () => {
    const profile = useSelector(state => state.user.profile)
  return (
    <div className='row row-company'>
        <Col className={4}>
              <Item
                item={profile?.org}
               
                
              />
            </Col>
    </div>
  )
}

export default AgentOrg