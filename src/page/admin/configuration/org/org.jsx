import React from "react";
import { Outlet, useLocation } from "react-router";
import { Link } from "react-router-dom";
import Col from "../../../../components/common/Col";
import Heading from "../../../../components/common/Heading";


const Org = () => {
  const { pathname } = useLocation();
  
  return (
    <>
      <div className="box-vertical">
        <Heading
          type={"h2"}
          style={{
            color: "#3b3f5c",
            fontSize: 25,
            marginBottom: 25,
          }}
        >
          Công ty
        </Heading>
        <div className="row">
          <Col className={4}>
            <div className="box-tabs">
              <div className="tabs-list">
              
              {companyTabs.map((item, index) => (
              <Link key={index} to={item.path} className={`tab-item ${pathname === `/company2/${item.path}` ? 'active' : ''}`}>
                <span>{item.name}</span>
              </Link>
            ))}
              </div>
            </div>
          </Col>
          <Col className={8}>
            <div className="box-form-general">
           
            <Outlet />
            </div>
            
          </Col>
        </div>
      </div>
    </>
  );
};

export default Org;

export const companyTabs = [
  {
    name: "DANH SÁCH CÔNG TY",
    key: 0,
    path: "company",
  },
  {
    name: "DANH SÁCH ĐẠI LÝ",
    key: 2,
    path: "agent",
  },
  {
    name: "DANH SÁCH TÀI KHOẢN",
    key: 2,
    path: "user",
  },
  

];
