import React, { useState, useEffect } from "react";
import { FaList, FaPen, FaStop, FaUser } from "react-icons/fa";
import { AiOutlinePlusCircle } from "react-icons/ai";

import { useDispatch, useSelector } from "react-redux";
import Button from "../../../../components/common/Button";
import Empty from "../../../../components/common/Empty";
import { Pagination } from "../../../../components/common/Pagination";
import { editOrgDashboardCode, getOrgDashboard } from "../../../../features/org/orgSlice";
import { usePaginationState } from "../../../../hooks/use-pagination";
import moment from "moment";
import FormInput from "../../../../components/common/FormInput";
import Modal, { ModalContent } from "../../../../components/common/Modal";
import DialogAddEdit from "./dialogAddEdit";
import DiglogAddAdmin from "../component/company/diglogAddAdmin";
import DialogAsk from "./DialogAsk";
import { useNavigate } from "react-router-dom";
const CompanyChild = (props) => {
  const navigator = useNavigate()
  const list = useSelector((state) => state.org.orgAdminList);
  const state = useSelector((state) => state.org);
  const dispatch = useDispatch();
  const pagination = usePaginationState();
  const [active, setActive] = useState(false);
  const [modalAdmin, setModalAdmin] = useState(false);
  const [modalAsk, setModalAsk] = useState(false);
  const [editCompany, setEditCompany] = useState(null);
  const [search, setSearch] = useState({
    isClear: false,
    name: "",
  });
  const handleClearSearch = () => {
    setSearch({ ...search, name: "" });
  };
  useEffect(() => {
    if (!search?.name) {
      handleClearSearch();
      dispatch(
        getOrgDashboard({
          parent: props.parent,
          limit: pagination.perPage,
          offset: pagination.perPage * pagination.page - pagination.perPage,
          name: search?.name,
        })
      );
    }
  }, [!search?.name, pagination]);


  useEffect(() => {
    if (state.success === true) {
      dispatch(getOrgDashboard({parent: props.parent}));
    }
  }, [state.success]);

  const handleStopOrg = () =>{
    const values = {
      id:editCompany?.id,
      is_active: !editCompany.is_active,
    }
    dispatch(editOrgDashboardCode({
      code:editCompany?.unique_id,
      body: values,
    })).unwrap().then(res=>{
      dispatch(showMessageSuccess())
    }).catch(err =>{
     
      dispatch(showMessageError())
    })
    setModalAsk(false)
  }
  return (
    <>
      <Modal active={active}>
        <ModalContent
          className="pu-company"
          heading={!editCompany ? "Thêm công ty" : "Sửa công ty"}
          onClose={() => {
            setActive(false);
            setEditCompany("");
          }}
        >
          <DialogAddEdit
            setUDO={setActive}
            dataEdit={editCompany}
            setData={setEditCompany}
          />
        </ModalContent>
      </Modal>


      <Modal active={modalAdmin}>
        <ModalContent
          className="pu-company"
          heading={'Thêm admin cho công ty'}
          onClose={() => {
            setModalAdmin(false);
          }}
        >
          <DiglogAddAdmin
            setActiveAddAdmin={setModalAdmin}
            dataEdit={editCompany}
          />
        </ModalContent>
      </Modal>


      <Modal active={modalAsk}>
        <ModalContent
          className="modal-warning"
          onClose={() => {
            setModalAsk(false);
            
          }}
        >
          <DialogAsk
            setUDO={setModalAsk}
            dataEdit={editCompany}
            text={editCompany?.is_active ? "Bạn có muốn dừng hoạt động công ty" : "Bạn có muốn kích hoạt công ty"}
            handleStop={handleStopOrg}
          />
        </ModalContent>
      </Modal>
      <div className="flex">
       <Button
          style={{ marginBottom: 10 }}
          className="outline"
          onClick={() => {
            setActive(true);
          }}
        >
          <AiOutlinePlusCircle style={{ marginRight: 5, fontSize: 18 }} />
          Thêm công ty
        </Button>  
        <FormInput
          isGroup={true}
          className="ml-auto search-wrapper"
          classInput="input-search"
          type="text"
          placeholder="Tìm kiếm tên"
          name="name"
          value={search?.name}
          onChange={(e) => {
            setSearch({ ...search, name: e.target.value });
          }}
          onKeyDown={(e) => {
            if (e.key === "Enter") {
              dispatch(
                getOrgDashboard({
                  parent: props.parent,
                  limit: pagination.perPage,
                  offset:
                    pagination.perPage * pagination.page - pagination.perPage,

                  name: search?.name,
                })
              );
            }
          }}
          isclear={search?.name}
          handleclear={handleClearSearch}
        />
      </div>
      <div className="table-wrapper">
        <table className="table mb-0">
          <thead className="bg-white">
            <tr>
              <th scope="col" className="border-0" style={{ width: 110 }}>
                Mã công ty
              </th>

              <th scope="col" className="border-0" style={{ width: 200 }}>
                Tên công ty
              </th>

              <th scope="col" className="border-0" style={{ width: 200 }}>
                Địa chỉ
              </th>
              <th scope="col" className="border-0">
                Số điện thoại
              </th>
              <th scope="col" className="border-0" style={{ width: 160 }}>
                Số lượng nhân viên (tối đa)
              </th>
              <th scope="col" className="border-0">
                Ngày tạo
              </th>
              <th scope="col" className="border-0" style={{ width: 120 }}>
                Đang hoạt động
              </th>
              <th scope="col" className="border-0">
                Hành động
              </th>
            </tr>
          </thead>

          <tbody>
            {list?.results?.map((item, idx) => {
              return (
                <tr key={idx}>
                  <td>{item?.code}</td>
                  <td>{item?.name}</td>
                  <td>{item?.address}</td>
                  <td>{item?.phone}</td>
                  <td>{item?.user_limit}</td>

                  <td>{moment(item?.created_on).format("DD-MM-YYYY")}</td>
                  <td className='text-center'>
                    <div className={`status ${item.is_active ? 'is_success' : 'is_error'}`}>
                      <Button className='outline'
                      style={{
                        fontSize:12,
                        fontWeight: 'bold',
                        height:26,
                        width:130,
                        padding:'0 5px'
                      }}
                       onClick={() => {
                        setEditCompany(item);
                        setModalAsk(true)
                        
                      }}>
                       <FaStop style={{
                        marginRight:5,
                        color: item.is_active ? '#8ec640' : '#d64237'
                       }} /> {item.is_active ? 'Đang hoạt động' : 'Dừng hoạt động'}
                      </Button>
                      
                    </div>
                  </td>
                  <td className="flex">
                    <Button
                      className="btn-edit"
                      onClick={() => {
                        setEditCompany(item);
                        setActive(true);
                      }}
                    >
                      <FaPen />
                    </Button>
                    <Button
                      className="btn-edit bg-theme"
                      onClick={() => {
                        setModalAdmin(true)
                        setEditCompany(item);
                      }}
                    >
                      <FaUser />
                    </Button>
                    <Button
                      className="btn-edit bg-theme"
                      onClick={() => {
                        navigator(`/company2/company/${item?.unique_id}`)
                      }}
                    >
                      <FaList />
                    </Button>

                    
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <Pagination
          currentPage={pagination.page}
          pageSize={pagination.perPage}
          lastPage={Math.min(
            Math.ceil((list?.results ? list?.count : 0) / pagination.perPage),
            Math.ceil((list?.results ? list?.count : 0) / pagination.perPage)
          )}
          onChangePage={pagination.setPage}
          onChangePageSize={pagination.setPerPage}
          onGoToLast={() =>
            pagination.setPage(
              Math.ceil((list?.results ? list?.count : 0) / pagination.perPage)
            )
          }
        />
        {list?.results?.length === 0 && (
          <Empty description="Không có dữ liệu"></Empty>
        )}
      </div>
    </>
  );
};

export default CompanyChild;
