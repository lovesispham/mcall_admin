import React from 'react'
import { IoIosWarning } from 'react-icons/io'
import Button from '../../../../components/common/Button'

const DialogAsk = (props) => {
  return (
    <div className='content text-center'>
       <IoIosWarning  />
       {props.text}
       
        <div className='flex' style={{justifyContent:'flex-end',marginTop:10, paddingTop:15}}>
        <Button className="outline" onClick={()=>props.setUDO(false)}>Thoát</Button>
        <Button className="btn bg-theme" onClick={props.handleStop}>Lưu</Button>
        </div>
      
    </div>
  )
}

export default DialogAsk