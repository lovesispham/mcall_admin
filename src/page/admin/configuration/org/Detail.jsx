import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import logo from "../../../../assets/images/logo.png";
import Col from "../../../../components/common/Col";
import CustomPhotoUpload from "../../../../components/common/CustomPhotoUpload";
import FormInput from "../../../../components/common/FormInput";
import Heading from "../../../../components/common/Heading";
import { HelpText } from "../../../../components/common/HelpText";
import {
  editOrgDashboardCode,
  getOrgDashboardCode,
  orgAsync,
} from "../../../../features/org/orgSlice";
import { useFormik } from "formik";
import * as yup from "yup";
import TextArea from "../../../../components/common/TextArea";
import FormCheckBox from "../../../../components/common/FormCheckBox";
import CustomReactSelect from "../../../../components/common/CustomReactSelect";
import { styles } from "../../../../scss/cssCustoms";
import { getModuleDashboard } from "../../../../features/module/moduleSlice";
import Button from "../../../../components/common/Button";
import {
  showMessageError,
  showMessageSuccess,
} from "../../../../features/alert/alert-message";
import CompanyChild from "./CompanyChild";
import { FaAlignLeft } from "react-icons/fa";
import Accordion from "../../../../components/common/Accordion";
import User from "../user/User";
import DetailOption from "./components/DetailOption";
import Modal, { ModalContent } from "../../../../components/common/Modal";
import DiglogAddAdmin from "../component/company/diglogAddAdmin";
import DialogAsk from "./DialogAsk";
import DialogAsync from "./DialogAsync";
const Detail = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const profile = useSelector((state) => state.user.profile);
  const data = useSelector((state) => state.org.orgDetail);
  // const modules = useSelector((state) => state.module.modulesDashboard);
  const state = useSelector((state) => state.org);

  const [selectedImage, setSelectedImage] = useState();
  const [active, setActive] = useState(false);
  const [moduleList, setModuleList] = useState([]);

  const [isActive, setIsActive] = useState(false)

  useEffect(() => {
    dispatch(getOrgDashboardCode({ code: id }));
    // dispatch(getModuleDashboard());
    setIsActive(id === profile ? false : true)
  }, [id]);
  useEffect(() => {
    if (state.success === true) {
      dispatch(getOrgDashboardCode({ code: id }));
    }
  }, [state.success]);

  // useEffect(() => {
  //   if (modules) {
  //     setModuleList(
  //       modules.map((v) => ({
  //         value: v.code,
  //         label: v.name,
  //         key: "modules",
  //       }))
  //     );
  //   }
  // }, [modules]);

  // const [selected, setSelected] = useState([]);

  // const handleChangeOption = (option, field) => {
  //   switch (field) {
  //     case "modules":
  //       setSelected([...option]);
  //       let arr = [...option];
  //       let results = arr.map((v) => v.value);
  //       setSelected(arr);
  //       setFieldValue(field, results);
  //       break;

  //     default:
  //       break;
  //   }
  // };

  // useEffect(() => {
  //   if (data && modules) {
  //     const arrEdit = data?.has_access;
  //     if (arrEdit) {
  //       const results = modules
  //         .filter((v) => arrEdit.includes(v?.id))
  //         .map((m) => ({
  //           value: m.code,
  //           label: m.name,
  //         }));
  //       setSelected(results);
  //     }
  //   }
   
  // }, [data, modules]);

  const validationSchema = yup.object(
    !data && {
      name: yup.string().required("Trường này không được bỏ trống"),
      code: yup.string().required("Trường này không được bỏ trống"),
    }
  );
  const convertToFormData = () => {
    const formData = new FormData();

    if (selectedImage) {
      formData.append("name", values.name);
      if (!data) {
        formData.append("code", values.code);
      }
      formData.append("address", values.address);
      formData.append("user_limit", values.user_limit);
      formData.append("phone", values.phone);
      formData.append("logo", selectedImage);

      return formData;
    } else {
      delete values["logo"];
      return values;
    }
  };
  const submitForm = async () => {
    dispatch(
      editOrgDashboardCode({
        code: data.unique_id,
        body: convertToFormData(),
      })
    )
      .unwrap()
      .then((res) => {
        dispatch(showMessageSuccess());
      })
      .catch((err) => {
        dispatch(showMessageError());
      });
    setUDO(false);
  };
  const {
    values,
    handleSubmit,
    handleChange,
    setFieldValue,
    handleBlur,
    touched,
    errors,
  } = useFormik({
    enableReinitialize: true,
    initialValues: {
      code: data?.code || "",
      address: data?.address || "",
      user_limit: data?.user_limit || 5,
      max_admin: data?.max_admin || 1,
      max_user: data?.max_user || 1,
      max_superadmin: data?.max_superadmin || 1,
      name: data?.name || "",
      phone: data?.phone || "",
      contact_name: data?.contact_name || "",
      contact_phone: data?.contact_phone || "",
      bank_branch: data?.bank_branch || "",
      bank_name: data?.bank_name || "",
      bank_number: data?.bank_number || "",
      agency: !!data?.agency,
      logo: `${import.meta.env.VITE_REACT_APP_API_URL}${data?.logo}` || logo,
    },
    validationSchema: validationSchema,
    onSubmit: submitForm,
  });


  // modal
  const [modalAsync, setModalAsync] = useState(false);
  const [modalAsk, setModalAsk] = useState(false);
  const [open, setOpen] = useState(false);
  const [activeAddAdmin, setActiveAddAdmin] = useState(false);

  const synchronized = () => {
    dispatch(
      orgAsync({
        async: "list",
        orgs: [data?.unique_id],
      })
    )
      .unwrap()
      .then((res) => {
        dispatch(showMessageSuccess("Đồng bộ thành công"));
        setModalAsync(false);
      })
      .catch((err) => {
        dispatch(showMessageError("Đồng bộ thất bại"));
      });
  };

  const handleStopOrg = () => {
    const values = {
      name: data?.name,
      is_active: !data.is_active,
    };
    dispatch(
      editOrgDashboardCode({
        code: data?.unique_id,
        body: values,
      })
    )
      .unwrap()
      .then((res) => {
        dispatch(showMessageSuccess());
      })
      .catch((err) => {
        console.log(err);
        dispatch(showMessageError());
      });
    setModalAsk(false);
  };
  return (
    <>
      <Modal active={activeAddAdmin}>
        <ModalContent
          className="pu-company"
          heading={"Thêm SuperAdmin cho công ty"}
          onClose={() => {
            setActiveAddAdmin(false);
          }}
        >
          <DiglogAddAdmin
            setActiveAddAdmin={setActiveAddAdmin}
            dataEdit={data}
          />
        </ModalContent>
      </Modal>

      <Modal active={modalAsk}>
        <ModalContent
          className="modal-warning"
          onClose={() => {
            setModalAsk(false);
          }}
        >
          <DialogAsk
            setUDO={setModalAsk}
            dataEdit={data}
            text={
              data?.is_active
                ? "Bạn có muốn dừng hoạt động công ty"
                : "Bạn có muốn kích hoạt công ty"
            }
            handleStop={handleStopOrg}
          />
        </ModalContent>
      </Modal>

      <Modal active={modalAsync}>
        <ModalContent
          heading={"Đồng bộ tổng đài"}
          className="modal-warning"
          onClose={() => {
            setModalAsync(false);
          }}
        >
          <DialogAsync
            setUDO={setModalAsync}
            text={`Đồng bộ ${data?.name}`}
            handleAsync={synchronized}
            list={data?.unique_id}
          />
        </ModalContent>
      </Modal>

      <DetailOption
        data={data}
        setActiveAddAdmin={setActiveAddAdmin}
        setModalAsk={setModalAsk}
        setModalAsync={setModalAsync}
        isOrg = {id === profile?.org?.unique_id ? true : false}
      />
      <Heading
        type={"h2"}
        style={{  fontSize: 25, margin: "0 0 20px 0" }}
      >
        Thông tin đại lý: {data?.name}
      </Heading>
      <form onSubmit={handleSubmit}>
        <div className="box-infor box-information">
          <div className="row">
            <Col className={12}>
              <CustomPhotoUpload
                label="Logo công ty"
                src={
                  (selectedImage && URL.createObjectURL(selectedImage)) || values.logo
                }
                onError={({ currentTarget }) => {
                  currentTarget.onerror = null; // prevents looping
                  currentTarget.src = logo;
                }}
                name="logo"
                onChange={(e) => {
                  setFieldValue("logo", e.currentTarget.files[0]);
                  setSelectedImage(e.currentTarget.files[0]);
                }}
              />
            </Col>
            <Col className={6}>
              <FormInput
                className={"inline border"}
                value={values.code}
                type="text"
                placeholder="Nhập mã code"
                label={"Code"}
                name="code"
                onChange={handleChange}
                required={!data}
              />
              <HelpText status="error">
                {touched.code && errors.code ? errors.code : ""}
              </HelpText>
            </Col>
            <Col className={6}>
              <FormInput
                className={"inline border"}
                value={values.name}
                type="text"
                placeholder="Nhập tên đại lý"
                label={"Tên đại lý"}
                name="name"
                onChange={handleChange}
                required={!data}
              />
              <HelpText status="error">
                {touched.name && errors.name ? errors.name : ""}
              </HelpText>
            </Col>

            <Col className={6}>
              <FormInput
                className={"inline border"}
                value={values.phone}
                type="number"
                placeholder="Nhập số điện thoại"
                label={"Số điện thoại"}
                name="phone"
                onChange={handleChange}
              />
            </Col>


            <Col className={6}>
              <FormInput
                className={"inline border"}
                value={values.contact_name}
                type="text"
                placeholder="Nhập tên pháp nhân"
                label={"Tên pháp nhân"}
                name="contact_name"
                onChange={handleChange}
              />
            </Col>

            <Col className={6}>
              <FormInput
                className={"inline border"}
                value={values.contact_phone}
                type="number"
                placeholder="Nhập số liên hệ"
                label={"Số liên hệ"}
                name="contact_phone"
                onChange={handleChange}
              />
            </Col>

            <Col className={6}>
              <FormInput
                className={"inline border"}
                value={values.bank_name}
                type="text"
                placeholder="Tên ngân hàng"
                label={"Tên ngân hàng"}
                name="bank_name"
                onChange={handleChange}
              />
            </Col>
            <Col className={6}>
              <FormInput
                className={"inline border"}
                value={values.bank_branch}
                type="text"
                placeholder="Tên chi nhánh"
                label={"Tên chi nhánh"}
                name="bank_branch"
                onChange={handleChange}
              />
            </Col>



            <Col className={6}>
              <FormInput
                className={"inline border"}
                value={values.bank_number}
                type="number"
                placeholder="Số tài khoản"
                label={"Số tài khoản"}
                name="bank_number"
                onChange={handleChange}
              />
            </Col>



            <Col className={6}>
              <FormInput
                className={"inline border"}
                value={values.user_limit}
                type="number"
                placeholder="Nhập số lượng nhân viên"
                label={"Số lượng nhân viên"}
                name="user_limit"
                onChange={handleChange}
              />
            </Col>
            <Col className={6}>
              <FormInput
                className={"inline border"}
                value={values.max_superadmin}
                type="number"
                placeholder="Nhập số lượng superadmin"
                label={"Số lượng superadmin"}
                name="max_superadmin"
                onChange={handleChange}
               
              />
            </Col>
            <Col className={6}>
              <FormInput
                className={"inline border"}
                value={values.max_admin}
                type="number"
                placeholder="Nhập số lượng admin"
                label={"Số lượng admin"}
                name="max_admin"
                onChange={handleChange}
              />
            </Col>

            {/* <Col className={6}>
              <FormInput
                className={"inline border"}
                value={values.max_user}
                type="number"
                placeholder="Nhập số lượng user"
                label={"Số lượng user"}
                name="max_user"
                onChange={handleChange}
              />
            </Col> */}
            <Col className={12}>
              <TextArea
                className={"inline border"}
                name="address"
                onChange={handleChange}
                label="Địa chỉ"
                rows={5}
                value={values.address}
                
              />
            </Col>
            <Col className={12}>
              <FormCheckBox
                pos="left"
                label={"Đại lý"}
                name={`agency`}
                onChange={(e) => {
                  setActive(e.currentTarget.checked);
                  setFieldValue("agency", e.currentTarget.checked);
                }}
                
                checked={values.agency}
              />
            </Col>

            {/* <Col className={12}>
              <CustomReactSelect
                styleInput={styles}
                className={"inline border"}
                classSelect={"multi-select"}
                label="Quyền truy cập"
                name="modules"
                options={moduleList}
                isMulti
                closeMenuOnSelect={false}
                menuPlacement="top"
                onChange={(e) => handleChangeOption(e, "modules")}
                placeholder={"Chọn"}
                value={selected}
                allowSelectAll={true}
                allOption={{ label: "Chọn tất cả", value: "*" }}
              />
            </Col> */}
            <Col className={12}>
              <div className="text-right">
                <Button className="bg-theme" type="submit">
                  Lưu
                </Button>
              </div>
            </Col>
          </div>
        </div>
      </form>
      
    </>
  );
};

export default Detail;
