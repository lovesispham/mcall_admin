import React, { useState, useEffect } from "react";
import Col from "../../../../components/common/Col";
import FormInput from "../../../../components/common/FormInput";
import { useFormik } from "formik";
import * as yup from "yup";
import logo from "../../../../assets/images/logo.png";
import TextArea from "../../../../components/common/TextArea";
import Button from "../../../../components/common/Button";
import { HelpText } from "../../../../components/common/HelpText";
import CustomPhotoUpload from "../../../../components/common/CustomPhotoUpload";
import {
  createOrgDashboard,
  editOrgDashboardCode,
  getOrgDashboardCode,
} from "../../../../features/org/orgSlice";
import { useDispatch, useSelector } from "react-redux";
import {
  showMessageError,
  showMessageSuccess,
} from "../../../../features/alert/alert-message";
import { getModuleDashboard } from "../../../../features/module/moduleSlice";
import CustomReactSelect from "../../../../components/common/CustomReactSelect";
import { styles } from "../../../../scss/cssCustoms";
import FormCheckBox from "../../../../components/common/FormCheckBox";
import { getUserDashboard } from "../../../../features/user/userSlice";

const DialogAddEdit = ({ setUDO, dataEdit = null, setData, createAgent = null }) => {
  const [selectedImage, setSelectedImage] = useState();
  const [user, setUser] = useState();
  const [active, setActive] = useState(false)
  const [selected, setSelected] = useState([]);
  
  const [dataAgent, setDataagent] = useState()


  const dispatch = useDispatch();
  const [moduleList, setModuleList] = useState([]);
  const modules = useSelector((state) => state.module.modulesDashboard);
  const profile = useSelector((state) => state.user.profile);


  useEffect(() => {
    if (profile?.org?.unique_id) {

      dispatch(
        getUserDashboard({
          org_unique:profile?.org?.unique_id,
          
        })
      ).then((res)=>{
        setUser(res?.payload?.data?.results?.map(v =>({
          value: v?.unique_id,
          label: v?.username,
          key: "created_by",
        })))
      })
    }
   

  }, [profile?.org?.unique_id]);

  useEffect(() => {
    dispatch(getModuleDashboard());
  }, []);


  

  useEffect(() => {
    if (modules) {
      setModuleList(
        modules.map((v) => ({
          value: v.id,
          label: v.name,
          key: "modules",
        }))
      );
    }
  }, [modules]);

  useEffect(() => {
    if (dataEdit && modules) {
      const arrEdit = dataEdit?.has_access;
      if (arrEdit) {
        const results = modules
          .filter((v) => arrEdit.includes(v?.id))
          .map((m) => ({
            value: m.id,
            label: m.name,
          }));
        setSelected(results);
      }
    }
    if(dataEdit){
      setActive(dataEdit?.is_active)
    }
  }, [dataEdit, modules]);


  
  const handleChangeOption = (option, field) => {
    switch (field) {
      case "modules":
        setSelected([...option]);
        let arr = [...option];
        let results = arr.map((v) => v.value);
        setSelected(arr);
        setFieldValue(field, results);
        break;

        case "created_by":
       
        setFieldValue(field, option.value);
        break;

      default:
        break;
    }
  };




  const validationSchema = yup.object(
    !dataEdit && {
      name: yup.string().required("Trường này không được bỏ trống"),
      code: yup.string().required("Trường này không được bỏ trống"),
    }
  );
  const convertToFormData = () => {
    const formData = new FormData();

    if (selectedImage) {
      formData.append("name", values.name);
      if (!dataEdit) {
        formData.append("code", values.code);
      }
      formData.append("address", values.address);
      formData.append("user_limit", values.user_limit);
      formData.append("phone", values.phone);
      formData.append("logo", selectedImage);

      return formData;
    } else {
      delete values["logo"];
      return values;
    }
  };
  const submitForm = async () => {
    if (dataEdit) {
      dispatch(
        editOrgDashboardCode({
          code: dataEdit.unique_id,
          body: convertToFormData(),
        })
      )
        .unwrap()
        .then((res) => {
          dispatch(showMessageSuccess());
        })
        .catch((err) => {
          dispatch(showMessageError());
        });
      setData("");
      setUDO(false);
    } else {
      dispatch(createOrgDashboard(convertToFormData()))
        .unwrap()
        .then((res) => {
          dispatch(showMessageSuccess());
        })
        .catch((error) => {

          dispatch(showMessageError(error?.data?.errors));
        });
      setUDO(false);
    }
  };
  const {
    values,
    handleSubmit,
    handleChange,
    setFieldValue,
    handleBlur,
    touched,
    errors,
  } = useFormik({
    enableReinitialize: true,
    initialValues: {
      code: dataEdit?.code || "",
      address: dataEdit?.address || "",
      user_limit: dataEdit?.user_limit || 5,
      max_admin: profile?.org?.adminCanCreate  ,
      max_user: profile?.org?.userCanCreate  ,
      max_superadmin: profile?.org?.superadminCanCreate ,
      name: dataEdit?.name || "",
      phone: dataEdit?.phone || "",
      
      contact_name: dataEdit?.contact_name || "",
      contact_phone: dataEdit?.contact_phone || "",
      bank_branch: dataEdit?.bank_branch || "",
      bank_name: dataEdit?.bank_name || "",
      bank_number: dataEdit?.bank_number || "",
      agency:!!createAgent
      // logo: dataEdit?.logo || logo,
    },
    validationSchema: validationSchema,
    onSubmit: submitForm,
  });


  return (
    <form onSubmit={handleSubmit}>
      <div className="content">
        <Col className={12}>
          <CustomPhotoUpload
            label="Logo công ty"
            src={(selectedImage && URL.createObjectURL(selectedImage)) || logo}
            onError={({ currentTarget }) => {
              currentTarget.onerror = null; // prevents looping
              currentTarget.src = logo;
            }}
            name="logo"
            onChange={(e) => {
              setFieldValue("logo", e.currentTarget.files[0]);
              setSelectedImage(e.currentTarget.files[0]);
            }}
          />
        </Col>
        <Col className={3}>
          <FormInput
            value={values.code}
            type="text"
            placeholder="Nhập mã code"
            label={"Code"}
            name="code"
            onChange={handleChange}
            required={!dataEdit}
          
          />
          <HelpText status="error">
            {touched.code && errors.code ? errors.code : ""}
          </HelpText>
        </Col>
        <Col className={3}>
          <FormInput
            value={values.name}
            type="text"
            placeholder={`Nhập tên ${!createAgent ? 'công ty' : 'đại lý'}`}
            label={!createAgent? 'Tên công ty' : 'Tên đại lý'}
            name="name"
            onChange={handleChange}
            required={!dataEdit}
          />
          <HelpText status="error">
            {touched.name && errors.name ? errors.name : ""}
          </HelpText>
        </Col>
        <Col className={3}>
          <FormInput
            value={values.phone}
            type="number"
            placeholder="Nhập số điện thoại"
            label={"Số điện thoại"}
            name="phone"
            onChange={handleChange}
            required={!dataEdit}
          />
        </Col>

        <Col className={3}>
          <FormInput
            value={values.user_limit}
            type="number"
            placeholder="Nhập số lượng nhân viên"
            label={"Số lượng nhân viên"}
            name="user_limit"
            onChange={handleChange}
          />
        </Col>

        {/* <Col className={3}>
          <FormInput
            value={values.max_superadmin}
            type="number"
            placeholder="Nhập số lượng superadmin"
            label={"Số lượng superadmin"}
            name="max_superadmin"
            onChange={handleChange}
            disabled
          />
        </Col>
        <Col className={3}>
          <FormInput
            value={values.max_admin}
            type="number"
            placeholder="Nhập số lượng admin"
            label={"Số lượng admin"}
            name="max_admin"
            onChange={handleChange}
            disabled
          />
        </Col> */}


            <Col className={3}>
              <FormInput
                
                value={values.bank_name}
                type="text"
                placeholder="Tên ngân hàng"
                label={"Tên ngân hàng"}
                name="bank_name"
                onChange={handleChange}
              />
            </Col>
            <Col className={3}>
              <FormInput
                
                value={values.bank_branch}
                type="text"
                placeholder="Tên chi nhánh"
                label={"Tên chi nhánh"}
                name="bank_branch"
                onChange={handleChange}
              />
            </Col>



            <Col className={3}>
              <FormInput
                
                value={values.bank_number}
                type="number"
                placeholder="Số tài khoản"
                label={"Số tài khoản"}
                name="bank_number"
                onChange={handleChange}
              />
            </Col>

            <Col className={3}>
              <FormInput
                
                value={values.contact_name}
                type="text"
                placeholder="Nhập tên pháp nhân"
                label={"Tên pháp nhân"}
                name="contact_name"
                onChange={handleChange}
              />
            </Col>

            <Col className={3}>
              <FormInput
                
                value={values.contact_phone}
                type="number"
                placeholder="Nhập số liên hệ"
                label={"Số liên hệ"}
                name="contact_phone"
                onChange={handleChange}
              />
            </Col>






        {/* <Col className={6}>
          <FormInput
            value={values.max_user}
            type="number"
            placeholder="Nhập số lượng user"
            label={"Số lượng user"}
            name="max_user"
            onChange={handleChange}
            disabled={true}
          />
        </Col> */}


        <Col className={12}>
          <TextArea
            name="address"
            onChange={handleChange}
            label="Địa chỉ"
            rows={5}
            value={values.address}
          />
        </Col>
        {/* <Col className={12}>
        <FormCheckBox
              pos="left"
              label={"Đại lý"}
              name={`agency`}
              onChange={(e) => {
                setActive(e.currentTarget.checked);
                setFieldValue("agency", e.currentTarget.checked);
              }}
              checked={active || values.is_active}
            />
        </Col> */}
        {!dataEdit && (
          <Col className={12}>
        <CustomReactSelect
            styleInput={styles}
           
            className={"inline border"}
            classSelect={'multi-select'}
            label="Chọn user"
            name="created_by"
            options={user}
            menuPlacement="top"
            onChange={(e) => handleChangeOption(e, "created_by")}
            placeholder={"Chọn"}
           
          />
        </Col>
        )}
        <Col className={12}>
          <CustomReactSelect
            styleInput={styles}
           
            className={"inline border"}
            classSelect={'multi-select'}
            label="Quyền truy cập"
            name="modules"
            options={moduleList}
            isMulti
            closeMenuOnSelect={false}
            menuPlacement="top"
            onChange={(e) => handleChangeOption(e, "modules")}
            placeholder={"Chọn"}
            value={selected}
            allowSelectAll={true}
            allOption={{ label: "Chọn tất cả", value: "*"}}
          />
        </Col>
        
        <Col className={12}>
          <div className="text-right max-[480px]:mt-2">
            <Button className="bg-theme" type="submit">
              Lưu
            </Button>
          </div>
        </Col>
      </div>
    </form>
  );
};

export default DialogAddEdit;
