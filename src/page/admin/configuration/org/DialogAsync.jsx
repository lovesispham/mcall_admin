import React from 'react'
import Button from '../../../../components/common/Button'

const DialogAsync = (props) => {
  return (
    <div className='content text-center'>
       
       {props.text}
       
        <div className='flex' style={{justifyContent:'flex-end',marginTop:10, paddingTop:15}}>
        <Button disabled={props.list === 0} className="btn bg-theme" onClick={props.handleAsync}>Lưu</Button>
        </div>
      
    </div>
  )
}

export default DialogAsync