import React from "react";
import { Link } from "react-router-dom";
import logo from "../../../../../assets/images/logo.png";
import MoreOption from "./MoreOption";

const Item = ({
  item,
  checkBoxItem,
  setEditCompany,
  setModalAsk,
  setModalAdmin,
}) => {

  return (
   
    <div className="item">
     <Link  to={`${item?.unique_id}`} >
      <div className="item-inside">
      </div>
      <div className="box-top">
        {checkBoxItem}
        
        <div className="photo">
          <img
            src={`${import.meta.env.VITE_REACT_APP_API_URL}${item?.logo}`}
            alt="avatar"
            onError={({ currentTarget }) => {
              currentTarget.onerror = null; // prevents looping
              currentTarget.src = logo;
            }}
          />
        </div>
        <p className="name">{item?.name}</p>
       
        {/* <MoreOption 
          setModalAdmin={setModalAdmin}
          setEditCompany={setEditCompany}
          setModalAsk={setModalAsk}
            to={`${item.unique_id}`}
            item={item}
        /> */}
      </div>
      <div className="content">
        <p>
          <span className="font-normal">Đại diện pháp luật: {''}</span>
          <span style={{ maxWidth: '100px', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis', display:'inline-block',verticalAlign:'top'  }}>{item?.contact_name}</span>
        </p>
        <p>
          <span className="font-normal">Mã công ty:</span> {item?.code}
        </p>
        <p>
          <span className="font-normal">Số điện thoại:</span> {item?.phone}
        </p>
        <p>
          <span className="font-normal">Số tài khoản: {''}
          </span>
           {item?.bank_number}
        </p>
        <p>
        <span className="font-normal">
        Tên ngân hàng : {''}
        
          
          
        </span>
        {item?.bank_name && item?.bank_name.toUpperCase()}
        </p>
        <p>
          <span className="font-normal">Chi nhánh:  {''}
          </span>
          {item?.bank_branch}
        </p>
       
      </div>
      <div
        className={`status ${item?.is_active ? "is_success" : "is_error"}`}
      ></div>
      </Link>
    </div>
   
  );
};

export default Item;
