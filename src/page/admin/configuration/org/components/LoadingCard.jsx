import React from 'react'
import Col from '../../../../../components/common/Col'

const LoadingCard = () => {
  return (
    <Col className={4}>
    <div className="item is-loading">
        <div className="box-top"></div>
        <div className="content">
            <p></p>
            <p></p>
            <p></p>
            <p></p>
        </div>
    </div>
    </Col>
  )
}

export default LoadingCard