import React, { useState } from "react";
import { BsThreeDots } from "react-icons/bs";
import { Link } from "react-router-dom";
import { useOnOutsideClick } from "../../../../../hooks/use-outside";

const MoreOption = (props) => {
  const [active, setActive] = useState(false);
  const { innerBorderRef } = useOnOutsideClick(() => {
    setActive(false);
  });

  return (
    <div className="more-drp" ref={innerBorderRef}>
      <span className="btn-more" onClick={() => setActive(!active)}>
        <BsThreeDots style={{ fontSize: 16 }} />
      </span>
      <div className={`dropdown-menu ${active ? "active" : ""}`}>
        <ul>
          {props.options?.map((item, indx) => (
            <li className="dropdown-item" key={indx}>
              {item?.to ? (
                <Link to={item?.to}>
                  <span>{item?.label}</span>
                </Link>
              ) : (
                <div
                  onClick={() => {
                    setActive(false);
                  }}
                >
                  <span onClick={item?.onClick}>{item?.label}</span>
                </div>
              )}
            </li>
          ))}
          {
            props.bottom && (
              <li className="dropdown-item last">
              <div
                  onClick={() => {
                    setActive(false);
                  }}
                >
                  <span className="text-red-500 hover:!text-red-500" onClick={props.bottom?.onClick}>{props.bottom?.label}</span>
                </div>
              </li>
            )
          }
        </ul>
      </div>
    </div>
  );
};

export default MoreOption;
