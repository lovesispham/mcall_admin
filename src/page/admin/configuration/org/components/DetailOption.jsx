import React from "react";
import Button from "../../../../../components/common/Button";

const DetailOption = (props) => {
  const options = props.isOrg
    ? [
        {
          value: 1,
          label: "Đồng bộ tổng đài",
          onClick: () => {
            props.setModalAsync(true);
          },
        },
        {
          value: 2,
          label: !props.data?.is_active ? "Kích hoạt" : "Dừng hoạt động",
          onClick: () => {
            props.setModalAsk(true);
          },
        },
      ]
    : [
        {
          value: 1,
          label: "Đồng bộ tổng đài",
          onClick: () => {
            props.setModalAsync(true);
          },
        },
        {
          value: 2,
          label: "Thêm SuperAdmin cho công ty",
          onClick: () => {
            props.setActiveAddAdmin(true);
          },
        },
        {
          value: 3,
          label: !props.data?.is_active ? "Kích hoạt" : "Dừng hoạt động",
          onClick: () => {
            props.setModalAsk(true);
          },
        },
      ];

  return (
    <div className="flex btn-group" style={{ marginBottom: 20 }}>
      {options.map((item, idx) => {
        return (
          <Button onClick={() => item?.onClick(idx)} className="outline">
            {item?.label}
          </Button>
        );
      })}
    </div>
  );
};

export default DetailOption;
