import React, { useEffect, useState, useRef } from "react";
import { FaList, FaPen, FaStop, FaUser } from "react-icons/fa";
import { AiOutlinePlusCircle } from "react-icons/ai";

import { useDispatch, useSelector } from "react-redux";
import Button from "../../../../components/common/Button";
import Empty from "../../../../components/common/Empty";
import { Pagination } from "../../../../components/common/Pagination";
import {
  editOrgDashboardCode,
  getOrgDashboard,
  orgAsync,
} from "../../../../features/org/orgSlice";
import { usePaginationState } from "../../../../hooks/use-pagination";
import moment from "moment";
import Modal, { ModalContent } from "../../../../components/common/Modal";
import DialogAddEdit from "./dialogAddEdit";
import DiglogAddAdmin from "../component/company/diglogAddAdmin";
import DialogAsk from "./DialogAsk";
import {
  showMessageError,
  showMessageSuccess,
} from "../../../../features/alert/alert-message";
import CustomSelect from "../../../../components/common/CustomSelect";
import DialogAsync from "./DialogAsync";
import FormCheckBox from "../../../../components/common/FormCheckBox";
import FormInput from "../../../../components/common/FormInput";
import { useNavigate } from "react-router-dom";
import Heading from "../../../../components/common/Heading";
import MoreOption from "./components/MoreOption";

const Company = () => {
  const navigator = useNavigate();
  const dispatch = useDispatch();
  const [search, setSearch] = useState({
    isClear: false,
    name: "",
  });
  const listType = [
    { value: "all", label: "Chọn tất cả", name: "async:" },
    { value: "list", label: "Chọn theo danh sách", name: "async:" },
  ];
  const [active, setActive] = useState(false);
  const [modalAdmin, setModalAdmin] = useState(false);
  const [modalAsk, setModalAsk] = useState(false);
  const [editCompany, setEditCompany] = useState(null);
  const list = useSelector((state) => state.org.orgAdminList);

  const state = useSelector((state) => state.org);
  const profile = useSelector((state) => state.user.profile);
  const pagination = usePaginationState();

  const handleClearSearch = () => {
    setSearch({ ...search, name: "" });
  };
  useEffect(() => {
    if (!search?.name && profile?.org?.unique_id) {
      handleClearSearch();
      dispatch(
        getOrgDashboard({
          limit: pagination.perPage,
          offset: pagination.perPage * pagination.page - pagination.perPage,
          name: search?.name,
          parent: profile?.org?.unique_id,
          agency: `False`,
        })
      );
    }
  }, [!search?.name, pagination, profile?.org?.unique_id]);

  useEffect(() => {
    if (state.success === true) {
      dispatch(
        getOrgDashboard({
          parent: profile?.org?.unique_id,
        })
      );
    }
  }, [state.success, profile?.org?.unique_id]);

  const handleStopOrg = () => {
    const values = {
      id: editCompany?.id,
      is_active: !editCompany.is_active,
      name:editCompany.name,
    };
    dispatch(
      editOrgDashboardCode({
        code: editCompany?.unique_id,
        body: values,
      })
    )
      .unwrap()
      .then((res) => {
        dispatch(showMessageSuccess());
      })
      .catch((err) => {
        dispatch(showMessageError());
      });
    setModalAsk(false);
  };
  // checkbox

  const arrList = useRef();
  const [checkBoxAll, setCheckBoxAll] = useState(false);
  const [activeTest, setActiveTest] = useState(false);

  const handleCheckAll = async (e) => {
    if (e.target.checked) {
      setCheckBoxAll(true);

      const arr = await arrList.current;
      arr.push(...list?.results?.map((v) => v?.unique_id));

      arrList.current = arr;
      setActiveTest(!activeTest);
    } else {
      const arr = arrList.current.filter(
        (v) => !list?.results?.find((k) => k.unique_id === v)
      );

      setCheckBoxAll(false);
      setActiveTest(!activeTest);
      arrList.current = arr;
    }
  };

  const handlnCheckBox = async (e) => {
    if (e.target.checked) {
      const arr = await arrList.current;
      await arr.push(e.target.name);

      arrList.current = arr;

      setActiveTest(!activeTest);
    } else {
      const arr = await arrList.current;
      await arr?.splice(arrList.current.indexOf(e.target.name), 1);
      arrList.current = arr;
      setActiveTest(!activeTest);
      setCheckBoxAll(false);
    }
  };

  useEffect(() => {
    arrList.current = [];
  }, []);

  useEffect(() => {
    if (
      list?.results?.filter((v) => arrList.current?.includes(v?.unique_id))
        .length >
      list?.results?.length - 1
    ) {
      setCheckBoxAll(true);
    } else {
      setCheckBoxAll(false);
    }
  }, [activeTest, pagination, list?.results]);
  // select box

  const [modalAsync, setModalAsync] = useState({
    open: false,
    type: "",
  });

  const handleChange = (field, value) => {
    setModalAsync({ ...modalAsync, type: value, open: true });
  };
  const synchronized = () => {
    const submitValue =
      modalAsync?.type === "all"
        ? {
            async: "all",
          }
        : {
            async: "list",
            orgs: arrList?.current,
          };
    dispatch(orgAsync(submitValue))
      .unwrap()
      .then((res) => {
        dispatch(showMessageSuccess("Đồng bộ thành công"));
        setModalAsync({ ...modalAsync, open: false });
      })
      .catch((err) => {
        dispatch(showMessageError("Đồng bộ thất bại"));
      });
  };

  return (
    <>
      <Modal active={active}>
        <ModalContent
          className="pu-company"
          heading={!editCompany ? "Thêm công ty" : "Sửa công ty"}
          onClose={() => {
            setActive(false);
            setEditCompany("");
          }}
        >
          <DialogAddEdit
            setUDO={setActive}
            dataEdit={editCompany}
            setData={setEditCompany}
          />
        </ModalContent>
      </Modal>

      <Modal active={modalAdmin}>
        <ModalContent
          className="pu-company"
          heading={"Thêm admin cho công ty"}
          onClose={() => {
            setModalAdmin(false);
          }}
        >
          <DiglogAddAdmin
            setActiveAddAdmin={setModalAdmin}
            dataEdit={editCompany}
          />
        </ModalContent>
      </Modal>

      <Modal active={modalAsk}>
        <ModalContent
          className="modal-warning"
          onClose={() => {
            setModalAsk(false);
          }}
        >
          <DialogAsk
            setUDO={setModalAsk}
            dataEdit={editCompany}
            text={
              editCompany?.is_active
                ? "Bạn có muốn dừng hoạt động công ty"
                : "Bạn có muốn kích hoạt công ty"
            }
            handleStop={handleStopOrg}
          />
        </ModalContent>
      </Modal>
      <Modal active={modalAsync?.open}>
        <ModalContent
          heading={"Đồng bộ tổng đài"}
          className="modal-warning"
          onClose={() => {
            setModalAsync({ ...modalAsync, open: false });
          }}
        >
          <DialogAsync
            setUDO={setModalAsync}
            text={
              modalAsync?.type === "all"
                ? "Đồng bộ tất cả"
                : `Tổng số công ty đã chọn:  ${arrList?.current?.length} `
            }
            handleAsync={synchronized}
            list={modalAsync?.type === "list" && arrList?.current?.length}
          />
        </ModalContent>
      </Modal>
      <Heading type={"h2"} style={{ fontSize: 25, margin: "0 0 20px 0" }}>
        Danh sách công ty
      </Heading>
      <div className="flex btn-table-group">
        <Button
          style={{ marginBottom: 10 }}
          className="outline"
          onClick={() => {
            setActive(true);
          }}
        >
          <AiOutlinePlusCircle style={{ marginRight: 5, fontSize: 18 }} />
          Thêm công ty
        </Button>
        <CustomSelect
          isGroup={false}
          className="outline"
          handleChange={handleChange}
          label={"Đồng bộ tổng đài"}
          name="async"
          options={listType}
          style={{
            width: "inherit",
            padding: "0 10px",
            height: 36,
          }}
        />
        <FormInput
          isGroup={true}
          className="md:ml-auto max-[480px]:mt-2 search-wrapper"
          classInput="input-search"
          type="text"
          placeholder="Tìm kiếm tên"
          name="name"
          value={search?.name}
          onChange={(e) => {
            setSearch({ ...search, name: e.target.value });
          }}
          onKeyDown={(e) => {
            if (e.key === "Enter") {
              dispatch(
                getOrgDashboard({
                  limit: pagination.perPage,
                  offset:
                    pagination.perPage * pagination.page - pagination.perPage,
                  parent: profile?.org?.unique_id,
                  name: search?.name,
                  agency: `False`,
                })
              );
            }
          }}
          isclear={search?.name}
          handleclear={handleClearSearch}
        />
      </div>

      <div className="table-wrapper">
        <table className="table mb-0">
          <thead className="bg-white">
            <tr>
              <th scope="col" className="border-0" style={{ width: 40 }}>
                <FormCheckBox
                  pos={"left"}
                  onChange={handleCheckAll}
                  checked={checkBoxAll}
                />
              </th>
              <th scope="col" className="border-0" style={{ width: 110 }}>
                Mã công ty
              </th>

              <th scope="col" className="border-0" style={{ width: 200 }}>
                Tên công ty
              </th>
              <th scope="col" className="border-0" style={{ width: 200 }}>
                Địa chỉ
              </th>
              <th scope="col" className="border-0">
                Số điện thoại
              </th>
              <th scope="col" className="border-0 text-right" style={{ width: 160 }}>
                Số lượng nhân viên (tối đa)
              </th>

              <th scope="col" className="border-0" style={{ width: 160 }}>
                Ngày tạo
              </th>
              <th scope="col" className="border-0" style={{ width: 100 }}>
                Đang hoạt động
              </th>
              <th
                scope="col"
                className="border-0 text-center"
                style={{ width: 140 }}
              >
                Hành động
              </th>
            </tr>
          </thead>

          <tbody>
            {list?.results?.map((item, idx) => {
              return (
                <tr key={idx}>
                  <td>
                    <FormCheckBox
                      name={item?.unique_id}
                      pos={"left"}
                      onChange={handlnCheckBox}
                      checked={arrList?.current?.includes(item?.unique_id)}
                    />
                  </td>
                  <td>{item?.code}</td>
                  <td>{item?.name}</td>
                  <td>{item?.address}</td>
                  <td>{item?.phone}</td>
                  <td className="text-right">{item?.user_limit}</td>

                  <td>{moment(item?.created_on).format("DD-MM-YYYY")}</td>
                  <td className="text-center">
                    <div
                      className={`badge ${
                        item.is_active ? "bg-active" : "bg-inactive"
                      }`}
                    >
                     {item.is_active ? "Hoạt động" : "Đã khoá"}
                    </div>
                  </td>
                  <td className="text-center">
                    <MoreOption
                      bottom={{
                        value: 1,
                        label: item?.is_active ? "Dừng hoạt động" : "Kích hoạt",
                        onClick: () => {
                          setEditCompany(item);
                          setModalAsk(true);
                        },
                      }}
                      options={[
                        {
                          value: 1,
                          label: "Sửa thông tin công ty",
                          onClick: () => {
                            setEditCompany(item);
                            setActive(true);
                          },
                        },
                        {
                          value: 2,
                          label: "Thêm admin cho công ty",
                          onClick: () => {
                            setModalAdmin(true);
                            setEditCompany(item);
                          },
                        },
                        {
                          value: 3,
                          label: `Xem danh sách tài khoản công ty`,
                          to: `/company2/company/${item?.unique_id}`,
                        },
                      ]}
                      item={item}
                    />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <Pagination
          currentPage={pagination.page}
          pageSize={pagination.perPage}
          lastPage={Math.min(
            Math.ceil((list?.results ? list?.count : 0) / pagination.perPage),
            Math.ceil((list?.results ? list?.count : 0) / pagination.perPage)
          )}
          onChangePage={pagination.setPage}
          onChangePageSize={pagination.setPerPage}
          onGoToLast={() =>
            pagination.setPage(
              Math.ceil((list?.results ? list?.count : 0) / pagination.perPage)
            )
          }
        />
        {list?.results?.length === 0 && (
          <Empty description="Không có dữ liệu"></Empty>
        )}
      </div>
    </>
  );
};

export default Company;
