import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import logo from "../../../../assets/images/logo.png";
import Col from "../../../../components/common/Col";
import CustomPhotoUpload from "../../../../components/common/CustomPhotoUpload";
import FormInput from "../../../../components/common/FormInput";
import Heading from "../../../../components/common/Heading";
import { HelpText } from "../../../../components/common/HelpText";
import {
  editOrgDashboardCode,
  getOrgDashboardCode,
  orgAsync,
} from "../../../../features/org/orgSlice";
import { useFormik } from "formik";
import * as yup from "yup";
import TextArea from "../../../../components/common/TextArea";
import FormCheckBox from "../../../../components/common/FormCheckBox";
import CustomReactSelect from "../../../../components/common/CustomReactSelect";
import { styles } from "../../../../scss/cssCustoms";
import { getModuleDashboard } from "../../../../features/module/moduleSlice";
import Button from "../../../../components/common/Button";
import {
  showMessageError,
  showMessageSuccess,
} from "../../../../features/alert/alert-message";
import CompanyChild from "./CompanyChild";
import { FaAlignLeft } from "react-icons/fa";
import Accordion from "../../../../components/common/Accordion";
import User from "../user/User";
import DetailOption from "./components/DetailOption";
import Modal, { ModalContent } from "../../../../components/common/Modal";
import DiglogAddAdmin from "../component/company/diglogAddAdmin";
import DialogAsk from "./DialogAsk";
import DialogAsync from "./DialogAsync";
import UserCompany from "../user/UserCompany";
const DetailCompany = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const [data, setData] = useState()
  const state = useSelector((state) => state.org);

  useEffect(() => {
    dispatch(getOrgDashboardCode({ code: id })).then((res )=>{
      setData(res?.payload?.data)
    })
  }, [id]);

  useEffect(() => {
    if (state.success === true) {
      dispatch(getOrgDashboardCode({ code: id }));
    }
  }, [state.success]);


  return (
    data && (
      
      <div className="box-company-accordin">
      <Heading
        type={"h2"}
        style={{ fontSize: 25, margin: "0 0 20px 0" }}
      >
       Danh sách tài khoản công ty {data?.name}
      </Heading>
      <UserCompany parent={data?.unique_id} data={data}  />

      </div>
    )
  );
};

export default DetailCompany;
