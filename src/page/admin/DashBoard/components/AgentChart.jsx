import React, { useState, useEffect } from "react";
import Chart from "react-apexcharts";
import { useDispatch } from "react-redux";
import Col from "../../../../components/common/Col";
import Heading from "../../../../components/common/Heading";
import { getOrgAndAgent } from "../../../../features/dashboard/dashboardThunk";
import { groupBy } from "../../../../util/help";

import Column from "./column";
import DashBoardHeader from "./DashBoardHeader";
import Empty from "../../../../components/common/Empty";
import ColumnDetail from "./ColumnDetail";
const AgentChart = ({ activeKeyTable, setActiveKeyTable }) => {
  const [activeTab, setActiveTab] = useState(0);
  const [activeDetailTab, setActiveDetailTab] = useState(0);
  const [listOrgAgent, setListOrgAgent] = useState();

  const [data, setData] = useState([]);

  const dispatch = useDispatch();

  //month and last month

  const date = new Date();
  let currentMonth = date.getMonth()+1;
  let prevMonth = date.getMonth();
  let currentYear = date.getFullYear()

  useEffect(() => {
    dispatch(getOrgAndAgent())
      .unwrap()
      .then((res) => {
        let agencyData = [];
        let orgData = [];
        const arr = [{ ...res?.lastMonth }, { ...res?.month }];
        if (arr) {
          agencyData = groupBy(
            arr.map((v) => v?.agency),
            (v) => v?.agency || "agency"
          );

          orgData = groupBy(
            arr.map((v) => v?.org),
            (v) => v?.org || "org"
          );
        }
        setListOrgAgent({
          ...res,

          compare: {
            data: {
              ...orgData,
              ...agencyData,
            },
            category: [`${prevMonth}/${currentYear}`,`${currentMonth}/${currentYear}`],
          },
          newMonthReport: {
            category: res?.orgMonthReport
              ?.filter((m) => m?.created !== 0)
              .map((v) => v?.date || "N/A"),
            data: res?.orgMonthReport
              ?.filter((m) => m?.created !== 0)
              .map((v) => `${v?.created}` || 0),
          },
          newCustomerMonthReport: {
            category: res?.customerMonthReport
              ?.filter((m) => m?.created !== 0)
              .map((v) => v?.date || "N/A"),
            data: res?.customerMonthReport
              ?.filter((m) => m?.created !== 0)
              .map((v) => `${v?.created}` || 0),
          },
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const dataTab = [
    <Column
      category={listOrgAgent?.compare?.category}
      data={listOrgAgent?.compare?.data?.agency}
      total={listOrgAgent?.agencies}
    />,
    <Column
      category={listOrgAgent?.compare?.category}
      data={listOrgAgent?.compare?.data?.org}
      total={listOrgAgent?.orgs}
    />,
  ];


  const dataTab2 = [
    <ColumnDetail
      category={listOrgAgent?.newMonthReport?.category}
      data={listOrgAgent?.newMonthReport?.data}
      name={'Số lượng đại lý/ công ty'}
    />,
    <ColumnDetail
      category={listOrgAgent?.newCustomerMonthReport?.category}
      data={listOrgAgent?.newCustomerMonthReport?.data}
      name={'Số lượng tài khoản'}
    />,
  ];

  return (
    listOrgAgent && (
      <div>
        <DashBoardHeader listOrgAgent={listOrgAgent} />

        <div className="chart">
          <Col className={6}>
            <div className="card-separator">
              <div className="lg:flex justify-center">
                <Heading type="h5" style={{ fontSize: "1.25rem" }}>
                  Số lượng {activeTab === 0 ? "đại lý mới" : "công ty mới"}
                </Heading>

                <div className="box-tabs ml-auto mr-10 max-lg:mt-2">
                  <div className="tabs-list" style={{ margin: 0 }}>
                    {dashboardTabs.map(
                      (v, i) =>
                        activeKeyTable?.includes(v?.index?.toString()) && (
                          <div
                            key={i}
                            className={`tab-item ${
                              activeTab === v.key ? "active" : ""
                            }`}
                            onClick={() => {
                              setActiveTab(v.key);
                            }}
                          >
                            <p className="font-[500]">{v.name}</p>
                          </div>
                        )
                    )}
                  </div>
                </div>
              </div>

              {dataTab[activeTab]}
            </div>
          </Col>
          <Col className={6}>
            <div className="card-separator">
              <div div className="lg:flex">
                <Heading type="h5" className="max-w-[250px]" style={{ fontSize: "1.25rem" }}>
                  Chi tiết số lượng tạo trong tháng {`${currentMonth}/${currentYear}`}
                </Heading>
                <div className="box-tabs ml-auto max-lg:mt-2">
                  <div className="tabs-list" style={{ margin: 0 }}>
                    {dashboardDetailTabs.map(
                      (v, i) =>
                        activeKeyTable?.includes(v?.index?.toString()) && (
                          <div
                            key={i}
                            className={`tab-item ${
                              activeDetailTab === v.key ? "active" : ""
                            }`}
                            onClick={() => {
                              setActiveDetailTab(v.key);
                            }}
                          >
                            <p className="font-[500]">{v.name}</p>
                          </div>
                        )
                    )}
                  </div>
                </div>
              </div>
              {dataTab2[activeDetailTab]}
              

            </div>
          </Col>
        </div>
      </div>
    )
  );
};

export default AgentChart;
export const dashboardTabs = [
  {
    name: "Đại lý",
    key: 0,
    index: 0,
  },
  {
    name: "Công ty",
    key: 1,
    index: 1,
  },
];

export const dashboardDetailTabs = [
  {
    name: "Đại lý/ Công ty",
    key: 0,
    index: 0,
  },
  {
    name: "Tài khoản",
    key: 1,
    index: 1,
  },
];
