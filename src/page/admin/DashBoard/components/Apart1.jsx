import React, { useEffect, useState } from "react";
import Chart1 from "./Chart1";
import Chart2 from "./Chart2";
import { useDispatch, useSelector } from "react-redux";
import { getOrgAndAgent } from "../../../../features/dashboard/dashboardThunk";

function Apart1({ activeKeyTable, setActiveKeyTable }) {
  const dispatch = useDispatch();
  const [listOrgAgent, setListOrgAgent] = useState();
  const [listMoth, setListMoth] = useState([]);
  const [listLastMoth, setLastListMoth] = useState([]);
  useEffect(() => {
    dispatch(getOrgAndAgent())
      .unwrap()
      .then((res) => {
        setListOrgAgent(res);
        setListMoth([
          { ...res.lastMonth, title: "02/2023" },
          { ...res.month, title: "03/2023" },
        ]);
        setLastListMoth(res.monthReport);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const dataCity = {
    labels: listMoth?.map((v) => v?.title || "N/A"),
    datasets: [
      {
        label: "Thống kê theo đại lý",
        data: listMoth?.map((v) => `${v?.org}` || 0),
        fill: true,

        // backgroundColor: '#0066FF',
        backgroundColor: ["#165BAA"],
        // borderColor: '#0066FF',
        barPercentage: 0.08,
        borderRadius: 5,
        borderWidth: 1,
      },
    ],
  };

  const dataCity2 = {
    labels: listLastMoth?.map((v) => v?.date || "N/A"),
    datasets: [
      {
        label: "Thống kê theo công ty",
        data: listLastMoth?.map((v) => v?.created || ""),
        fill: true,
        backgroundColor: ["#A155B9"],
        // borderColor: '#0066FF',
        barPercentage: 0.3,
      },
    ],
  };
  const dataStatus = {
    labels: listMoth?.map((v) => v?.title || "N/A"),
    datasets: [
      {
        label: "Thống kê sản phẩm",
        data: listMoth?.map((v) => `${v?.org}` || 0),
        fill: true,
        backgroundColor: [
          "#A155B9",
          "#165BAA",
          "#F765A3",
          "#16BFD6",
          "#16BFD6",
          "#F4B740",
        ],
        borderColor: ["white"],
        borderWidth: "0",
        width: "10px",
        cutout: "50%",
        borderRadius: 5,
      },
    ],
  };
  return (
    <>
      
        
          <Chart1
            title={"Số lượng tài khoản"}
            dataCity={dataCity}
            dataCity2={dataCity2}
            setActiveKeyTable={setActiveKeyTable}
            activeKeyTable={activeKeyTable}
          />
       
        {/* <div style={{ flex: 1 }}>
           <Chart2
            title={"Thống kê doanh số"}
            dataStatus={dataStatus}
            setActiveKeyTable={setActiveKeyTable}
            activeKeyTable={activeKeyTable}
            options={options}
            dataSource={[]}
            DataPie={dataStatus}
          /> 
        </div> */}
      
    </>
  );
}

export default Apart1;

const options = {
  plugins: {
    legend: {
      position: "top",
      labels: {
        boxWidth: "15",
        usePointStyle: true,
        color: "#222",
        // pointStyle: 'triangle',
      },
    },
  },
};
