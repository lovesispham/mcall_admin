import React from "react";
import Empty from "../../../../components/common/Empty";
import Chart from "react-apexcharts";
const ColumnDetail = (props) => {
  return (
    <>
      {props.category?.length === 0 ? (
        <Empty description="Không có dữ liệu"></Empty>
      ) : (
        <Chart
          height={"295px"}
          type="bar"
          options={{
            chart: {
              id: "agent-chart-2",
              stacked: true,
              toolbar: {
                show: true,
                tools: {
                  download: false, // <== line to add
                },
              },
            },
            plotOptions: {
              bar: {
                horizontal: false,
                borderRadius: 5,
                columnWidth: "30%",

                borderRadiusApplication: "around",
              },
            },
            xaxis: {
              categories: props.category,
              axisTicks: {
                show: false,
              },
              axisBorder: {
                show: false,
              },
              labels: {
                show: true,
              },
            },

            yaxis: {
              axisTicks: {
                show: false,
              },
              axisBorder: {
                show: false,
              },
              labels: {
                show: false,
              },
            },
            //xoa duong line
            grid: {
              show: false,
              padding: {
                top: 0,
                bottom: 0,
              },
              xaxis: {
                lines: {
                  show: false,
                },
              },
              yaxis: {
                lines: {
                  show: false,
                },
              },
            },
          }}
          series={[
            {
              name: props.name,
              data: props.data,
            },
          ]}
        />
      )}
    </>
  );
};

export default ColumnDetail;
