import React, { useState , useEffect } from "react";
import { Bar } from "react-chartjs-2";
import "chart.js/auto";

const PieChart = ({ dataCity2, dataCity }) => {
  const options = {
    plugins: {
      legend: {
        position: "bottom",
        labels: {
          boxWidth: "15",
          usePointStyle: true,
          color: "#222",
        },
      },
    },
  };
  return <Bar data={dataCity2 || dataCity} options={options}></Bar>;
};

function Chart1({
  dataCity2,
  dataCity,
  activeKeyTable,
  setCallApiindex,
  title,
}) {
  const [activeTab, setActiveTab] = useState(0);

  const dataTab = [
    <PieChart dataCity={dataCity} />,
    <PieChart dataCity2={dataCity2} />,
  ];
  useEffect(() => {
    if (!activeKeyTable?.includes('2')) {
      setActiveTab(1);
    } else if (!activeKeyTable?.includes('3') && !activeKeyTable?.includes('2')) {
      setActiveTab(2);
    } else {
      setActiveTab(0);
    }
  }, [activeKeyTable.length]);
  return (
    <div className="chart-wrapper" style={{padding:0}}>
      <div className="md:text-[20px] md:font-[600] md:py-1 text-[12px] font-[600] py-[10px] px-[20px] bg-slate-100">
        {title}
      </div>
      <div className="box-tabs px-[20px]" >
        <div className="tabs-list" style={{padding:'0 20px'}}>
          {dashboardTabs.map(
            (v, i) =>
              activeKeyTable?.includes(v?.index?.toString()) && (
                <div
                  key={i}
                  className={`tab-item ${activeTab === v.key ? "active" : ""}`}
                  onClick={() => {
                    setActiveTab(v.key);
                    v.key === 0 && setCallApiindex("all");
                    v.key === 1 && setCallApiindex("purchased");    
                  }}
                >
                  <p className="md:text-[16px] md:font-[600] md:py-1 text-[10px] font-[600] py-[10px]">
                    {v.name}
                  </p>
                </div>
              )
          )}
        </div>
      </div>

      <div className="box-form-general px-[20px] pb-[20px]">{dataTab[activeTab]}</div>
    </div>
  );
}

export default Chart1;
export const dashboardTabs = [
  {
    name: "Đại lý",
    key: 0,
    index: 0,
  },
  {
    name: "Công ty",
    key: 1,
    index: 1,
  },
];
