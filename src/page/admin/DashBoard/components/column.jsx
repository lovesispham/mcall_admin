import React from "react";
import { BsArrowDownShort, BsArrowUpShort } from "react-icons/bs";
import { AiOutlineUsergroupAdd } from "react-icons/ai";
import Chart from "react-apexcharts";
import { isPositiveInteger } from "../../../../util/help";
const Column = ({ currentMonth, category, data, total }) => {
  const percentChange = (v1, v2) => {
    
    if(v1 && v2){
      return Math.round(((v2 - v1) / v1) * 100)
    } else{
      return 0
    }
  };

  const percentTotal = (v1, v2) => Math.round((v1 / v2) * 100);

  return (
    <div className="flex justify-between">
      <div className=" mt-10">
        <h2 className="flex text-[2rem] align-center">
          <AiOutlineUsergroupAdd style={{ color: "#4d7ffd" }} />
          <span className="xl:text-[1.5rem]">
            {data && percentTotal(data[1], total)}%
          </span>
        </h2>
        <div className="font-medium">
          <span
            className={`${
              data && isPositiveInteger(percentChange(data[0], data[1]))
                ? "text-green-600"
                : "text-red-600"
            }`}
          >
            {data && isPositiveInteger(percentChange(data[0], data[1])) ? (
              <BsArrowUpShort style={{ display: "inline", fontSize: 15 }} />
            ) : (
              <BsArrowDownShort style={{ display: "inline", fontSize: 15 }} />
            )}
            {data && percentChange(data[0], data[1])}%
          </span>
        </div>
      </div>
      <Chart
        height={"335px"}
        type="bar"
        options={{
          chart: {
            id: "agent-chart",

            toolbar: {
              show: true,
              tools: {
                download: false, // <== line to add
              },
            },
          },
          plotOptions: {
            bar: {
              horizontal: false,
              borderRadius: 5,
              columnWidth: "20%",
              barHeight: "100%",
              distributed: true,
              borderRadiusApplication: "around",
            },
          },

          xaxis: {
            categories: category,
            axisTicks: {
              show: false,
            },
            axisBorder: {
              show: false,
            },
            labels: {
              show: true,
            },
          },

          yaxis: {
            axisTicks: {
              show: false,
            },
            axisBorder: {
              show: false,
            },
            labels: {
              show: false,
            },
          },
          //xoa duong line
          grid: {
            show: false,
            padding: {
              top: 0,
              bottom: 0,
            },
            xaxis: {
              lines: {
                show: false,
              },
            },
            yaxis: {
              lines: {
                show: false,
              },
            },
          },
          //xoa label cot doc
        }}
        series={[
          {
            name: "Số lượng",
            data: data,
          },
        ]}
      />
    </div>
  );
};

export default Column;
