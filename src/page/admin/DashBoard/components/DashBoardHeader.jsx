import React from "react";
import DatePicker from "react-date-picker";
import { FaRegCalendar } from "react-icons/fa";
import {
  FcSalesPerformance,
  FcConferenceCall,
  FcDepartment,
  FcAssistant,
  FcComboChart,
} from "react-icons/fc";
import Col from "../../../../components/common/Col";
import Row from "../../../../components/common/Row";

function DashBoardHeader({ setStartDate, startDate, listOrgAgent }) {
  const total = (a, b) => {
    return a + b;
  };

  const list = [
    {
      id: 1,
      title: "Số lượng đại lý",
      count: listOrgAgent?.agencies,
      icon: <FcConferenceCall />,
    },
    {
      id: 2,
      title: "Số lượng công ty",
      count: listOrgAgent?.orgs,
      icon: <FcDepartment />,
    },
    {
      id: 3,
      title: "Số lượng tài khoản",
      count: listOrgAgent?.customers,
      icon: <FcAssistant />,
    },
  ];

  return (
    <>
      <div
        className="flex items-center mb-[10px] py-[10px]"
        style={{ gap: "16px" }}
      >
        <div className="flex-1">
          <h1 className="text-[26px] ">
            Xin chào <b>Admin</b>
          </h1>
        </div>
        {/* <div className="flex-1 flex justify-end">
          <div className="flex items-center ">
            <div className="mb-[10px] w-[50%]">
              <DatePicker
                className="custom-datepicker-new"
                name={"fromdate"}
                value={startDate}
                onChange={(value) => {
                  setStartDate(value);
                }}
                format="d/MM/y"
                clearIcon={null}
                calendarIcon={<FaRegCalendar />}
              />
            </div>
          </div>
        </div> */}
        <div className="flex-1"></div>
      </div>
      <div className="mb-[16px] flex items-center px-[40px]">
        <FcSalesPerformance className="text-[20px]" />
        <p className="break-words mb-0">
          <b>Chương trình trợ giá. </b>- giảm giá 10% và các ưu đái khác. Xem
          chi tiết...
        </p>
      </div>
      <div>
        <p className="flex items-center text-[16px]">
          <FcComboChart style={{ fontSize: 30 }} />{" "}
          <b className="mr-[20px]">Tổng quan</b>{" "}
          <a className="text-[#0ea5e9] font-[500]" href="">
            xem chi tiet
          </a>
        </p>
      </div>
      <Row className="mb-[20px] md:mr-[-20px] ml-[-20px]">
        {list.map((item, index) => {
          return (
            <Col className={3} key={index}>
              <div className="max-[767px]:mb-5 p-[20px] items-center  justify-between rounded-[0.5rem]  bg-[white] custom-shawdow">
                <div className="text-[42px] mb-2">{item.icon}</div>
                <div>
                  <div className="text-[16px]">{item.title}</div>
                  <div className="text-[24px] font-medium text-end">
                    {item.count}
                  </div>
                </div>
              </div>
            </Col>
          );
        })}
      </Row>
    </>
  );
}

export default DashBoardHeader;
