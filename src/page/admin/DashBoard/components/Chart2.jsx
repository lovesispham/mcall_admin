import React, { useState, useRef } from "react";
import { Pie } from "react-chartjs-2";
import "chart.js/auto";
import { FaAngleRight, FaAngleLeft } from "react-icons/fa";
import { useEffect } from "react";
const Chart2 = ({
  title,
  dataStatus,
  dataSource,
  DataPie,
  options,
  activeKeyTable = [],
  setCallApiindex,
}) => {
  const [activeTab, setActiveTab] = useState(0);
  const dataTab = [
    <PieChart dataStatus={dataStatus} options={options} />,
    // <PieChart dataStatus={dataStatus} options={options} />,
    // <PieChart DataPie={DataPie} options={options} />,
  ];
  const scrollElement = useRef(null);
  const [scrollX, setscrollX] = useState(0);
  const [scrolEnd, setscrolEnd] = useState(false);
  //Slide click
  const slide = (shift) => {
    // scrollElement.current.scrollLeft += shift;
    // setscrollX(scrollX + shift);
    // if (
    //   Math.floor(scrollElement.current.scrollWidth - scrollElement.current.scrollLeft) <=
    //   scrollElement.current.offsetWidth
    // ) {
    //   setscrolEnd(true);
    // } else {
    //   setscrolEnd(false);
    // }
  };

  const scrollCheck = () => {
    // setscrollX(scrollElement.current.scrollLeft);
    // if (
    //   Math.floor(scrollElement.current.scrollWidth - scrollElement.current.scrollLeft) <=
    //   scrollElement.current.offsetWidth
    // ) {
    //   setscrolEnd(true);
    // } else {
    //   setscrolEnd(false);
    // }
  };
  useEffect(() => {
    if (!activeKeyTable?.includes("2")) {
      setActiveTab(1);
    } else if (
      !activeKeyTable?.includes("3") &&
      !activeKeyTable?.includes("2")
    ) {
      setActiveTab(2);
    } else {
      setActiveTab(0);
    }
  }, [activeKeyTable.length]);

  return (
    <div className="chart-wrapper " style={{padding:0}}>
      <div className="md:text-[20px] md:font-[600] md:py-1 text-[12px] font-[600] py-[10px] px-[20px] bg-slate-100">
        {title}
      </div>
      <div className="box-tabs px-[20px]">
        {scrollX !== 0 && (
          <div className="btn-right" onClick={() => slide(-200)}>
            <FaAngleLeft />
          </div>
        )}
        <div className="tabs-list" ref={scrollElement} onScroll={scrollCheck}>
          {dashboardTabs.map(
            (v, i) =>
              activeKeyTable?.includes(v?.index?.toString()) && (
                <div
                  key={i}
                  className={`tab-item ${activeTab === v.key ? "active" : ""}`}
                  onClick={() => {
                    setActiveTab(v.key);
                    v.key === 1 && setCallApiindex("source");
                    v.key === 2 && setCallApiindex("employe");
                  }}
                >
                  <p>{v.name}</p>
                </div>
              )
          )}
        </div>
        {!scrolEnd && (
          <div onClick={() => slide(+200)} className="btn-right">
            <FaAngleRight />
          </div>
        )}
      </div>

      <div className="pb-[20px]">{dataTab[activeTab]}</div>
    </div>
  );
};

export default Chart2;
export const dashboardTabs = [
  {
    name: "Tình trạng đơn hàng",
    key: 0,
    index: 2,
  },
  {
    name: "Nguồn khách hàng",
    key: 1,
    index: 3,
  },
  {
    name: "Đơn hàng theo nhân viên",
    key: 2,
    index: 4,
  },
];
const PieChart = ({ dataStatus, DataPie, options }) => {
  return (
    <div style={{ width: "73%", margin: "0 auto" }}>
      <Pie data={dataStatus || DataPie} options={options}></Pie>
    </div>
  );
};
