import { useEffect, useState, useRef, useCallback } from "react";
import Button from "../../../components/common/Button";
import FormLabel from "../../../components/common/FormLabel";
import DatePicker from "react-date-picker";
import { FaRegCalendar } from "react-icons/fa";
import { AiOutlineSetting } from "react-icons/ai";
import Apart3 from "./components/Apart3";
import Apart2 from "./components/Apart2";
import Apart1 from "./components/Apart1";
import DashBoardHeader from "./components/DashBoardHeader";
import AgentChart from "./components/AgentChart";

const Dashboard = () => {
  const [activeKeyTable, setActiveKeyTable] = useState([
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "11",
  ]);
  const listRef = useRef([]);
  const date = new Date();
  const [startDate, setStartDate] = useState(
    new Date(date.getFullYear(), date.getMonth(), 1)
  );
  const [endDate, setEndDate] = useState(
    new Date(date.getFullYear(), date.getMonth() + 1, 0)
  );
  const handleSearch = async () => {
    setDataTime({
      ...dataTime,
      fromdate: moment(startDate).format("YYYY-MM-DD"),
      todate: moment(endDate).format("YYYY-MM-DD"),
    });
  };
  const custom = null;
  const list = [
    {
      id: 1,
      components: (
        <AgentChart
          activeKeyTable={activeKeyTable}
          setActiveKeyTable={setActiveKeyTable}
        />
      ),
    },
    // {
    //   id: 1,
    //   components: <Apart2 />,
    // },
    // {
    //   id: 1,
    //   components: <Apart3 />,
    // },
  ];
  return (
    <>
      <div>
        
        {/* <div className="md:flex md:justify-between sm:block py-[20px]">
          <div className="md:flex md:items-center sm:block md:gap-[1rem]">
            <div className="flex items-center">
              <label className="mb-[10px] text-[16px] font-[600]" htmlFor="">
                Từ ngày
              </label>
              <div style={{ marginBottom: "10px" }}>
                <DatePicker
                  className="custom-datepicker-new"
                  name={"fromdate"}
                  value={startDate}
                  onChange={(value) => {
                    setStartDate(value);
                  }}
                  format="d/MM/y"
                  clearIcon={null}
                  calendarIcon={<FaRegCalendar />}
                />
              </div>
            </div>

            <div className="flex items-center">
              <label className="mb-[10px] text-[16px] font-[600]" htmlFor="">
                Đến ngày
              </label>
              <div style={{ marginBottom: "10px" }}>
                <DatePicker
                className="custom-datepicker-new"
                  name={"todate"}
                  value={endDate}
                  onChange={(value) => {
                    setEndDate(value);
                  }}
                  format="d/MM/y"
                  clearIcon={null}
                  calendarIcon={<FaRegCalendar />}
                />
              </div>
            </div>
          </div>

          <div
            style={{
              display: "flex",
              alignItems: "center",
              marginBottom: "10px",
            }}
          >
            <Button
              onClick={handleSearch}
              className="bg-theme"
              style={{ marginRight: "10px" }}
            >
              Tìm kiếm
            </Button>
            <Button
              className="bg-theme"
              onClick={() => {
                setCustom(!custom);
              }}
            >
              <AiOutlineSetting data-tip={"Cài đặt hiển thị"} />
            </Button>
          </div>
        </div> */}

        {list.map((x, idx) => (
          <div key={idx + 1} ref={(el) => (listRef.current[x.id] = el)}>
            {x.components}
          </div>
        ))}
      </div>
    </>
  );
};

export default Dashboard;
