import React, { useState, useEffect } from 'react';
import { Section } from '../../../components/common/Section';
import FormInput from '../../../components/common/FormInput';
import { selectProfile, editPassword, getProfile, editUser, selectMessage } from '../../../features/user/userSlice';
import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';
import * as yup from 'yup';

import Grid from '../../../components/common/Grid';
import Button from '../../../components/common/Button';

import blank from '../../../assets/images/logo.png';
import Heading from '../../../components/common/Heading';
import { showMessageError, showMessageSuccess } from '../../../features/alert/alert-message';
import { FaPencilAlt } from 'react-icons/fa';
const Profile = () => {
  const dispatch = useDispatch();
  const profile = useSelector(selectProfile);
  const status = useSelector(selectMessage);
  const [activeOld, setActiveOld] = useState(false);
  const [activeNew, setActiveNew] = useState(false);
  const [activeAgain, setActiveAgain] = useState(false);
  const [selectedImage, setSelectedImage] = useState();

  const convertToFormData = () => {
    const editProfile = {
      last_name: values.last_name,
      first_name: values.first_name,
      id: profile?.id,
      email: profile?.email,
    };
    const formData = new FormData();
    if (selectedImage) {
      formData.append('last_name', values.last_name);
      formData.append('first_name', values.first_name);
      formData.append('email', profile?.email);
      formData.append('profile_pic', selectedImage);
      return formData;
    } else {
      return editProfile;
    }
  };

  const submitForm = async (values, { resetForm }) => {
    const valuesPassword = {
      old_password: values.old_password,
      new_password: values.new_password,
      retype_password: values.retype_password,
    };
    if (values.new_password && values.old_password && values.retype_password) {
      await dispatch(editPassword(valuesPassword))
        .unwrap()
        .then(() => {
          dispatch(showMessageSuccess());
        })
        .catch((error) => {
          console.log(error);
          dispatch(showMessageError());
        });
      await dispatch(editUser({ id: profile?.id, body: convertToFormData() }))
        .unwrap()
        .then(() => {
          dispatch(showMessageSuccess());
        })
        .catch((error) => {
          dispatch(showMessageError());
        });
    } else {
    }
    if (values.new_password !== values.retype_password) {
      dispatch(showMessageError('Mật khẩu không trùng khớp'));
      return;
    } else {
      await dispatch(editUser({ id: profile?.id, body: convertToFormData() }))
        .unwrap()
        .then(() => {
          dispatch(showMessageSuccess());
        })
        .catch((error) => {
          dispatch(showMessageError());
        });
    }

    resetForm({
      new_password: '',
      retype_password: '',
    });
  };

  useEffect(() => {
    if (status === true) {
      dispatch(getProfile());
    }
    //eslint-disable-next-line
  }, [status]);

  const validationSchema = yup.object({
    // new_password: yup
    //   .string()
    //   .required('Nhập mật khẩu')
    //   .matches(
    //     // eslint-disable-next-line
    //     /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
    //     'Mật khẩu bao gồm 8 ký tự, 1 ký tự in hoa, 1 ký tự thường, 1 số và 1 ký tự đặc biệt'
    //   ),
  });

  const { values, handleSubmit, handleChange, errors, touched, setFieldValue } = useFormik({
    initialValues: {
      old_password: '',
      new_password: '',
      retype_password: '',
      last_name: profile?.last_name || '',
      first_name: profile?.first_name || '',
      profile_pic: profile?.profile_pic || blank,
    },
    enableReinitialize: true,
    validationSchema: validationSchema,
    onSubmit: submitForm,
  });

  return (
    <>
      <Heading type='h2' style={{ marginBottom: 15 }}>
        Thông tin cá nhân
      </Heading>
      <Section className='general-info'>
        <div className='info'>
          <div className='photo'>
            <img
              src={values?.profile_pic ? values?.profile_pic : blank}
              alt='avatar'
              onError={({ currentTarget }) => {
                currentTarget.onerror = null; // prevents looping
                currentTarget.src = blank;
              }}
            />
            <span className='edit'>
              <FaPencilAlt />
            </span>
            <input
              accept='image/png, image/jpeg'
              type='file'
              onChange={(e) => {
                setFieldValue('profile_pic', URL?.createObjectURL(e.currentTarget.files[0]));
                setSelectedImage(e.currentTarget.files[0]);
              }}
            />
          </div>
          <div className='user-info'>
            <Grid col={2} mdCol={1} smCol={1} gap={10}>
              <FormInput onChange={handleChange} value={values.last_name} name='last_name' type='text' label='Họ' />
              <FormInput onChange={handleChange} value={values.first_name} name='first_name' type='text' label='Tên' />
              <FormInput value={profile?.username} name='username' type='text' label='Tên đăng nhập' disabled />
              <FormInput value={profile?.email} name='email' type='email' label='Email' disabled />
            </Grid>

            <Grid col={3} mdCol={1} smCol={1} gap={10}>
              <FormInput
                onChange={handleChange}
                value={values.old_password}
                name='old_password'
                type='password'
                label='Mật khẩu cũ'
                active={activeOld}
                setActive={setActiveOld}
              />
              <FormInput
                onChange={handleChange}
                value={values.new_password}
                name='new_password'
                type='password'
                label='Mật khẩu mới'
                active={activeNew}
                setActive={setActiveNew}
              />
              <FormInput
                value={values.retype_password}
                onChange={handleChange}
                name='retype_password'
                type='password'
                label='Nhập lại mật khẩu'
                active={activeAgain}
                setActive={setActiveAgain}
              />
            </Grid>

            {errors.retype_password && touched.retype_password ? (
              <div style={{ color: 'red' }}>{errors.retype_password}</div>
            ) : null}
            <div className='text-right'>
              <Button className='bg-theme' style={{ width: 120 }} type='submit' onClick={handleSubmit}>
                Cập nhập
              </Button>
            </div>
          </div>
        </div>
      </Section>
    </>
  );
};

export default Profile;
