import React from 'react';

const Privacy = () => {
  return (
    <>
      <div className='main-content'>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          VNtel đã xây dựng giải pháp CRM dưới dạng phần mềm thương mại. DỊCH VỤ này được cung cấp bởi Vntel và được sử
          dụng như hiện tại. Trang này được sử dụng để thông báo cho khách truy cập trang web về các chính sách của
          chúng tôi với việc thu thập, sử dụng và tiết lộ dữ liệu nếu bất kỳ ai quyết định sử dụng dịch vụ của chúng
          tôi. Nếu bạn chọn sử dụng dịch vụ của chúng tôi, nghĩa là bạn đồng ý với việc thu thập và sử dụng thông tin
          liên quan đến chính sách này. Dữ liệu mà chúng tôi thu thập được sử dụng để cung cấp và cải thiện dịch vụ.
          Chúng tôi sẽ không sử dụng hoặc chia sẻ dữ liệu của bạn với bất kỳ ai ngoại trừ được mô tả trong Chính sách
          Bảo mật này. Các điều khoản được sử dụng trong Chính sách Bảo mật này có cùng ý nghĩa như trong Điều khoản và
          Điều kiện của chúng tôi, có thể truy cập được tại Vntel trừ khi được định nghĩa khác trong Chính sách Bảo mật
          này.
        </p>
        <h3 style={{ fontWeight: 600, fontSize: 22 }}>1. Mục đích và phạm vi thu thập</h3>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          CRM yêu cầu khách hàng cung cấp các thông tin cơ bản bao gồm: email, điện thoại, tên đăng nhập, mật khẩu đăng
          nhập, địa chỉ khi khách hàng đăng ký sử dụng dịch vụ của CRM và một số thông tin không bắt buộc khác khi khách
          hàng muốn tương tác với một số nội dung trên website. CRM sử dụng các thông tin này nhằm liên hệ xác nhận với
          khách hàng và đảm bảo quyền lợi cho Khách hàng khi cần thiết.
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          Việc bạn truy cập, đăng ký, sử dụng CRM có nghĩa rằng bạn đồng ý và chấp nhận ràng buộc bởi các quy định trong
          chính sách bảo mật của chúng tôi.
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          Khách hàng sẽ tự chịu trách nhiệm về bảo mật và lưu giữ mọi hoạt động sử dụng dịch vụ dưới tên đăng ký, mật
          khẩu và hộp thư điện tử và/hoặc số điện thoại của mình. CRM không chịu trách nhiệm về các thất thoát dữ liệu,
          bí mật thông tin của khách hàng do khách hàng vô tình hoặc cố ý gây ra. Ngoài ra, Khách hàng có trách nhiệm
          thông báo kịp thời cho CRM về những hành vi sử dụng trái phép, lạm dụng, vi phạm bảo mật, lưu giữ tên đăng ký
          và mật khẩu của bên thứ ba để có biện pháp giải quyết phù hợp.
        </p>
        <h3 style={{ fontWeight: 600, fontSize: 22 }}>2. Dịch vụ, ứng dụng liên kết với CRM</h3>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          Để đảm bảo quyền lợi và trải nghiệm tốt nhất cho Khách hàng, CRM áp dụng một số điều khoản riêng khi Khách
          hàng sử dụng các dịch vụ do CRM cung cấp hoặc có liên kết với CRM:
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          A. Đối với tài khoản Facebook của Khách hàng khi liên kết CRM, Phần mềm sẽ yêu cầu quyền truy cập các thông
          tin sau:
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          1) Quyền truy cập vào địa chỉ email của Trang (Fanpage) Facebook sử dụng để tích hợp với CRM;
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          2) Quyền truy cập vào tập hợp các mục công khai trên Trang Facebook đã tích hợp với CRM;
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          3) Cho phép truy cập vào tập hợp các mục công khai trên Tài khoản cá nhân của người dùng có tương tác với
          Trang Facebook tích hợp với CRM.
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          4) Cho phép truy cập hộp thư trên Trang Facebook tích hợp với CRM thông qua CRM;
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          5) Cho phép gửi và nhận tin nhắn, bình luận trên Trang Facebook tích hợp với CRM thông qua CRM.
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          B. Đối với tài khoản Zalo của Khách hàng khi liên kết với CRM, CRM sẽ yêu cầu quyền truy cập các thông tin
          sau:
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          1) Quyền truy cập vào ảnh đại diện, tên, ảnh bìa và mô tả của tài khoản Zalo Official Account sử dụng để tích
          hợp với CRM;
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          2) Quyền truy cập vào thông tin họ tên, địa chỉ, số điện thoại của người dùng theo dõi (follow) tài khoản Zalo
          Official account sử dụng tích hợp với CRM
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          3) Cho phép gửi và nhận tin nhắn từ tài khoản Zalo Official Account tích hợp với Vntel thông qua CRM.
        </p>
        <h3 style={{ fontWeight: 600, fontSize: 22 }}>3. Phạm vi sử dụng thông tin</h3>
        <p style={{ fontSize: 16, fontWeight: 500 }}>Vntel sử dụng thông tin Khách hàng cung cấp để:</p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>– Cung cấp các dịch vụ đến Khách hàng.</p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          – Gửi các thông báo về các hoạt động trao đổi thông tin giữa Khách hàng và đơn vị Hỗ trợ kĩ thuật của CRM
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          – Ngăn ngừa các hoạt động phá hủy tài khoản người dùng của Khách hàng hoặc các hoạt động giả mạo Khách hàng.
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          – Liên lạc và giải quyết với khách hàng trong những trường hợp đặc biệt.
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          – Vntel có trách nhiệm hợp tác cung cấp thông tin cá nhân Khách hàng khi có yêu cầu từ cơ quan nhà nước có
          thẩm quyền.
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          – Chia sẻ thông tin cần thiết cho bên đối tác nếu nhận được sự đồng ý của Khách hàng.
        </p>
        <h3 style={{ fontWeight: 600, fontSize: 22 }}>4. Thời gian lưu trữ thông tin</h3>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          Trong mọi trường hợp thông tin cá nhân Khách hàng sẽ được bảo mật hoàn toàn trên máy chủ của CRM. Khách hàng
          có quyền cập nhật, sửa đổi và xóa thông tin của các Dữ liệu cá nhân này. Tuy nhiên, trong một số trường hợp,
          CRM vẫn có thể khôi phục những thông tin đó từ cơ sở dữ liệu của chúng tôi để giải quyết các tranh chấp, thi
          hành điều khoản sử dụng, hay vì các yêu cầu kỹ thuật, pháp lý liên quan đến sự an toàn và những hoạt động của
          CRM.
        </p>
        <h3 style={{ fontWeight: 600, fontSize: 22 }}>
          5. Địa chỉ của đơn vị thu thập, quản lý thông tin và hỗ trợ Khách hàng
        </h3>
        <p style={{ fontSize: 16, fontWeight: 500 }}>Công ty Cổ phần Phần Viễn thông tin học Việt Nam</p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          Trụ sở chính: Nhà B1, ĐN14, số 118/6 Nguyễn Khánh Toàn, Q. Cầu Giấy, TP. Hà Nội.
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>Hotline: 0901.826.555 – 0901.899.555</p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>Email: cskh@vntel.vn</p>
        <h3 style={{ fontWeight: 600, fontSize: 22 }}>
          6. Phương tiện và công cụ để Khách hàng tiếp cận và chỉnh sửa dữ liệu của mình
        </h3>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          Khách hàng có quyền tự kiểm tra, cập nhật, điều chỉnh thông tin cá nhân của mình bằng cách đăng nhập vào tài
          khoản và chỉnh sửa thông tin cá nhân hoặc yêu cầu CRM thực hiện việc này.
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          Khách hàng có quyền gửi khiếu nại về việc lộ thông tin các nhân cho bên thứ ba đến Ban quản trị của CRM. Khi
          tiếp nhận những phản hồi này, CRM sẽ xác nhận lại thông tin, phải có trách nhiệm trả lời lý do và hướng dẫn
          Khách hàng khôi phục và bảo mật lại thông tin.
        </p>
        <h3 style={{ fontWeight: 600, fontSize: 22 }}>7. Cam kết bảo mật thông tin cá nhân Khách hàng</h3>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          Thông tin của Khách hàng trên CRM được CRM cam kết bảo mật tuyệt đối theo chính sách bảo vệ thông tin cá nhân
          của CRM. Việc thu thập và sử dụng thông tin của mỗi Khách hàng chỉ được thực hiện khi có sự đồng ý của Khách
          hàng đó, trừ những trường hợp pháp luật có quy định khác. CRM cam kết:
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          – Không sử dụng, không chuyển giao, cung cấp hay tiết lộ cho bên thứ ba nào về thông tin cá nhân của Khách
          hàng khi không có sự cho phép hoặc đồng ý từ Khách hàng, trừ những trường hợp pháp luật có quy định khác.
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>
          – Bảo mật tuyệt đối mọi thông tin giao dịch trực tuyến của Khách hàng bao gồm thông tin hóa đơn, chứng từ kế
          toán số hóa tại khu vực dữ liệu trung tâm an toàn của CRM.
        </p>
        <p style={{ fontSize: 16, fontWeight: 500 }}>Lưu ý: Chính sách bảo mật này có hiệu lực từ ngày 31/05/2020.</p>
      </div>
    </>
  );
};

export default Privacy;
