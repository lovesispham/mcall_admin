//react hook
import React, { useRef,useEffect, useCallback } from "react";
import { useFormik } from "formik";
import * as yup from "yup";
import { useDispatch } from "react-redux";

import { useNavigate } from "react-router-dom";
import FormInput from "../../components/common/FormInput";
import "./auth.css";
import {
  showMessageError,
  showMessageSuccess,
} from "../../features/alert/alert-message";
import { useState } from "react";
import { handleErr } from "../../util/convertErr";
import img from "../../assets/images/robo.png";
import FormCheckBox from "../../components/common/FormCheckBox";
import { HelpText } from "../../components/common/HelpText";
import { login, LoginApi } from "../../features/auth/authSlice";
import { getProfile } from "../../features/user/userSlice";
import ReCAPTCHA from "react-google-recaptcha";

const Auth = () => {
  const captchaRef = useRef(null);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [active, setActive] = useState(false);
  const [disableSubmit,setDisableSubmit] = useState(true);
  const validationSchema = yup.object({
    email: yup
      .string()
      .required("Bạn chưa nhập email")
      .email("Email không hợp lệ"),
    password: yup.string().required("Bạn chưa nhập mật khẩu"),
  });

  const submitForm = (values) => {
    const token = captchaRef.current.getValue()
    dispatch(
      LoginApi({
        email: values.email,
        password: values.password,
        recaptcha: token,
      })
    )
      .unwrap()
      .then((res) => {
        dispatch(getProfile()).then((res) => {
          
          localStorage.setItem(
            "is_superuser",
            res?.payload?.data?.user_obj?.is_superuser
          );
        });
        dispatch(login());
        localStorage.setItem("role", res?.role);
         if (res?.role === 'SUPERADMIN') {
           navigate('/dashboard');
         }
         if (res?.role === 'USER' || 'ADMIN' ) {
          navigate('/denied');
         }
        dispatch(showMessageSuccess("Đăng nhập thành công"));
      })
      .catch((err) => {
        captchaRef.current.reset();
        console.log(err)
        dispatch(showMessageError(handleErr(err?.data?.errorType)));
      });
  };

  const { values, errors, handleChange, handleBlur, handleSubmit, touched } =
    useFormik({
      validationSchema: validationSchema,
      initialValues: {
        email: "",
        password: "",
      },
      onSubmit: submitForm,
    });
  return (
    <>
      <div className="blockLogin">
        <form onSubmit={handleSubmit}>
          <div className="formLogin">
            <div className="image">
              <img src={img} alt="" className="imageLogin" />
            </div>
            <div className>
              <div className="textLogin">
                <h2>
                  <b>MCALL PORTAL</b>
                </h2>
              </div>

              <FormInput
                name="email"
                type="email"
                bottom="20px"
                styleInput={{
                  borderRadius: "8px",
                  backgroundColor: "white",
                  marginBottom: 5,
                  height: 40,
                  borderColor: "#222",
                }}
                placeholder="Nhập email"
                required
                onChange={handleChange}
              />
              <HelpText className="text-left block mb-4" status="error">
                {touched.email && errors.email ? errors.email : ""}
              </HelpText>
              <FormInput
                name="password"
                type="password"
                bottom="30px"
                styleInput={{
                  borderRadius: "8px",
                  marginBottom: 5,
                  backgroundColor: "white",
                  height: 40,
                  borderColor: "#222",
                }}
                active={active}
                setActive={setActive}
                required
                placeholder="Nhập mật khẩu"
                onChange={handleChange}
              />
              <HelpText className="text-left block mb-4" status="error">
                {touched.password && errors.password ? errors.password : ""}
              </HelpText>
              <ReCAPTCHA
                sitekey={import.meta.env.VITE_REACT_APP_API_CAPTCHA}
                ref={captchaRef}
                onChange={useCallback(() => setDisableSubmit(false))}
              />

              <div>
                <button
                  className="btn btn-login"
                  style={{ backgroundColor: "#2b7fff" }}
                  type="submit"
                  disabled={disableSubmit}
                >
                  Đăng nhập
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </>
  );
};

export default Auth;
