import { combineReducers, configureStore } from '@reduxjs/toolkit';
import alertReducer from '../features/alert/alert-message';
import authReducer from '../features/auth/authSlice';
import extensionReducer from '../features/extension/extensionSlice';
import moduleReducer from '../features/module/moduleSlice';
import orgReducer from '../features/org/orgSlice';
import userReducer from '../features/user/userSlice';
import dashboardReducer from '../features/dashboard/dashboardSlice';
import sidebarReducer from '../features/sidebar/sidebarSlice';
const rootReducer = combineReducers({
  dashboard: dashboardReducer,
  alert: alertReducer,
  auth:authReducer,
  user:userReducer,
  org:orgReducer,
  module:moduleReducer,
  extension:extensionReducer,
  sidebar:sidebarReducer,
});

const configStore = () => {
  const store = configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({
        serializableCheck: false,
      }),
  });

  return store;
};

const store = configStore();
export default store;
