import React from "react";
import { Navigate, Outlet } from "react-router-dom";

const useAuth = () => {
  //get item from localstorage
  const token = localStorage.getItem("access_token");
  const local_role = localStorage.getItem("role");
  const is_superuser = localStorage.getItem("is_superuser");
  if (token) {
    return {
      auth: true,
      role: local_role,
      is_superuser: is_superuser,
    };
  } else {
    return {
      auth: false,
      role: null,
      is_superuser: null,
    };
  }
};

const ProtectedRoutes = (props) => {
  const { auth, role, is_superuser } = useAuth();
  //if the role required is there or not
  if (props.roleRequired) {
    return auth ? (
      props.roleRequired.includes(role) ? (
        <Outlet />
      ) : (
        <Navigate to="/denied" />
      )
    ) : (
      <Navigate to="/login" />
    );
  } else {
    return auth ? <Outlet /> : <Navigate to="/login" />;
  }
};

export default ProtectedRoutes;
