import React, {useEffect} from 'react'
import { RxLockClosed } from 'react-icons/rx'
import { Link, useNavigate } from 'react-router-dom'
import Heading from '../components/common/Heading'

const PermissionDenied = (props) => {
const navigate = useNavigate();
const local_role = localStorage.getItem("role");
useEffect(() => {
  if(!props.roleRequired.includes(local_role)){
    navigate('/dashboard');
  }
}, [props.role])



  const logout = async () => {
    await localStorage.clear();
  };
  return (
    <div className='page-permision'
    >
      <RxLockClosed />
      <Heading type='h2' >
            KHÔNG CÓ QUYỀN TRUY CẬP
          </Heading>
          <p>
            Bạn không có quyền truy cập vào đây
          </p>
          <Link to='/'  className='btn bg-theme' style={{lineHeight:'34px'}}
          onClick={()=>logout()}
          >
            Đăng xuất
          </Link>
    </div>
  )
}

export default PermissionDenied