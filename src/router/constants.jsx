import Dashboard from "../page/admin/DashBoard/Dashboard";
import Term from "../page/other/Term";
import Privacy from "../page/other/Privacy";
import Configuration from "../page/admin/configuration/configuration";
import CompanyAgent from "../page/admin/configuration/component/CompanyAgent";
import CompanyLayout from "../page/admin/configuration/component/companyLayout";
import Profile from "../page/admin/User/Profile";
import Org from "../page/admin/configuration/org/org";
import UserDetail from "../page/admin/configuration/user/UserDetail";
import User from "../page/admin/configuration/user/User";
import Company2 from "../page/admin/configuration/org/Company";
import DetailCompany from "../page/admin/configuration/org/DetailCompany";
import Agent from "../page/admin/configuration/org/Agent";
import Detail from "../page/admin/configuration/org/Detail";

export const router = [
  {
    path: 'dashboard',
    component: <Dashboard module={'DASHBOARD'} /> ,
    module: 'DASHBOARD',
    role: ['ADMIN', 'SUPERADMIN'],
  },

  {
    path: 'profile',
    component: <Profile />,
    module: 'CONF',
    role: ['ADMIN', 'SUPERADMIN'],
  },
];

export const user = [
  {
    path: 'terms-of-use',
    component: <Term />,
  },
  {
    path: 'privacy-policy',
    component: <Privacy />,
  },
];


